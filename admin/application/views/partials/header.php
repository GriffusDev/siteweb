<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title><?php echo $title_for_layout; ?></title>
        <?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0):
            foreach($includes_for_layout['css'] as $css): ?>
                                <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
            <?php endforeach;
        endif;
        if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): 
            foreach($includes_for_layout['js'] as $js): 
                if($js['options'] === NULL OR $js['options'] == 'footer'): ?>
                    <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
                <?php endif; 
            endforeach; 
        endif; ?>
        <!--[if IE]> <link href="{function:base_url}assets/css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/charts/jquery.sparkline.min.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.inputlimiter.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.autosize.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.ibutton.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.dualListBox.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.validate.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.select2.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/forms/jquery.cleditor.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/uploader/jquery.plupload.queue.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/wizard/jquery.form.wizard.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/wizard/jquery.form.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.collapsible.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.jgrowl.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.pie.chart.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.fullcalendar.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.elfinder.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/ui/jquery.fancybox.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/tables/jquery.dataTables.min.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/bootstrap/bootstrap-bootbox.min.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/bootstrap/bootstrap-progressbar.js"></script>
        <script type="text/javascript" src="{function:base_url}assets/js/plugins/bootstrap/bootstrap-colorpicker.js"></script>

        <script type="text/javascript" src="{function:base_url}assets/js/functions/custom.js"></script>

    </head>

<body>
    <!-- Top nav -->
    <div id="top">
        <div class="top-wrapper">
            <a href="{function:base_url}Home" title="" class="logo"><?php echo $this->config->item('Server_Name'); ?></a>
            <ul class="topnav">
                <li class="topuser">
                    <a title="" data-toggle="dropdown"><img src="{function:base_url}assets/images/user.png" alt="" /><span><?php echo $this->session->userdata('account_name'); ?></span><i class="caret"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" title=""><span class="user-profile"></span>Mon profile</a></li>
                        <li><a href="#" title=""><span class="user-stats"></span>Message <strong>2</strong></a></li>
                        <li><a href="#myModal" data-toggle="modal"><span class="user-logout"></span>Logout</a></li>
                    </ul>
                </li>
                <li><a href="#" title=""><b class="settings"></b></a></li>
                <li><a href="#" title=""><b class="mail"></b></a></li>
                <li class="search">
                    <a title=""><b class="search"></b></a>
                    <form class="top-search" action="#">
                        <input type="text" placeholder="Search..." />
                        <input type="submit" value="" />
                    </form>
                </li>
                <li class="sidebar-button"><a href="#" title=""><b class="responsive-nav"></b></a></li>
                <li><a href="#" title=""><b class="logout"></b></a></li>
            </ul>
        </div>
    </div>
    <!-- /top nav -->
    <div style="display: none;" id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h5 id="myModalLabel">Deconnexion</h5>
                            </div>
                            <div class="modal-body">
                            <div class="row-fluid">
                                Êtes vous sur de vouloir vous deconnecter ?
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal">Non</button>
                                <a href="{function:base_url}Auth/Logout"><button class="btn btn-primary">Oui</button></a>
                            </div>
                        </div>