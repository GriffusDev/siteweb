<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><?php echo $title_for_layout; ?></title>
	<?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0): ?>
		<?php foreach($includes_for_layout['css'] as $css): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
		<?php endforeach; ?>
	<?php endif; ?>
        <link href='http://fonts.googleapis.com/css?family=Andika&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Oldenburg&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pirata+One&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    </head>
    <body>