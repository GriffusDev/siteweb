<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Main content -->
    <div class="content">
        <!-- Info notice -->
        <div class="notice outer">
            <div class="note">
                <button type="button" class="close">×</button>
                <strong>Bienvenue !</strong> Bienvenue sur la version ALPHA de l'administration du serveur shindorei Serveur.
            </div>
            <br />
            <ul class="breadcrumb" style="margin-bottom: -1px;">
                <li><a href="{function:base_url}Home"><i class="font-home"></i>{lang:Title_Page_Home}</a> <span class="divider">/</span></li>
                <li class="active">Forum</li>
            </ul>
        </div>
        <!-- /info notice -->
    	<div class="outer">
            <div class="inner">
                <div class="page-header"><!-- Page header -->
                    <h5><i class="font-home"></i>{lang:Title_Page_Forum}</h5>
                </div><!-- /page header -->
                <div class="body">
                    <!-- Content container -->
                    <div class="container">
                        <div class="block well">
                            <div class="navbar">
                                <div class="navbar-inner">
                                    <h5>Statistiques</h5>
                                </div>
                            </div>
                            <div class="table-overflow">
                                <table class="table table-striped table-bordered table-block align-center">
                                    <thead>
                                        <tr>
                                            <th># & Type</th>
                                            <th>Status</th>
                                            <th>Nom</th>
                                            <th>Description</th>
                                            <th>Statistique</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="#">#1 / Catégorie</a>
                                            </td>
                                            <td>
                                                <span class="btn btn-block btn-success">Ouvert</span>
                                            </td>
                                            <td>
                                                Communauté
                                            </td>
                                            <td>
                                                NA
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>NA messages</li>
                                                    <li>NA sujets</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="table-controls">
                                                    <li><a data-original-title="Add entry" href="#" class="btn hovertip" title=""><b class="font-plus"></b></a> </li>
                                                    <li><a data-original-title="Edit entry" href="#" class="btn hovertip" title=""><b class="font-edit"></b></a> </li>
                                                    <li><a data-original-title="Export" href="#" class="btn hovertip" title=""><b class="font-user"></b></a> </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">#1 / Forum</a>
                                            </td>
                                            <td>
                                                <span class="btn btn-block btn-danger">Fermer</span>
                                            </td>
                                            <td>
                                                Annonce
                                            </td>
                                            <td>
                                                Annonce du serveur
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>94563 messages</li>
                                                    <li>79864513 sujets</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="table-controls">
                                                    <li><a data-original-title="Add entry" href="#" class="btn hovertip" title=""><b class="font-plus"></b></a> </li>
                                                    <li><a data-original-title="Edit entry" href="#" class="btn hovertip" title=""><b class="font-edit"></b></a> </li>
                                                    <li><a data-original-title="Export" href="#" class="btn hovertip" title=""><b class="font-user"></b></a> </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">#1 / Forum</a>
                                            </td>
                                            <td>
                                                <span class="btn btn-block btn-success">Ouvert</span>
                                            </td>
                                            <td>
                                                Présentation
                                            </td>
                                            <td>
                                                Venez vous présentez
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>94563 messages</li>
                                                    <li>79864513 sujets</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="table-controls">
                                                    <li><a data-original-title="Add entry" href="#" class="btn hovertip" title=""><b class="font-plus"></b></a> </li>
                                                    <li><a data-original-title="Edit entry" href="#" class="btn hovertip" title=""><b class="font-edit"></b></a> </li>
                                                    <li><a data-original-title="Export" href="#" class="btn hovertip" title=""><b class="font-user"></b></a> </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br />
                        <dl style="text-align: center;">
                            <dt>Configurations</dt>
                            <dd><a href="{function:base_url}Forum/Config"class="btn btn-info">Général</a></dd>
                            <dt>Administration des catégories & sous-catégories</dt>
                            <dd><a href="{function:base_url}Forum/Config"class="btn btn-info">Créer une catégorie & sous-catégories</a><a href="{function:base_url}Forum/Config"class="btn btn-info">Modifier une catégorie & sous-catégories</a><a href="{function:base_url}Forum/Config"class="btn btn-info">Modifier les permissions</a></dd>
                            <dt>Administration des forums</dt>
                            <dd><a href="{function:base_url}Forum/Config"class="btn btn-info">Créer un forum</a><a href="{function:base_url}Forum/Config"class="btn btn-info">Modifier un forum</a><a href="{function:base_url}Forum/Config"class="btn btn-info">Modifier les permissions</a></dd>
                        </dl>
                    </div>
                    <!-- /content container -->
                </div>
            </div>
        </div>
    </div>
    <!-- /main content -->