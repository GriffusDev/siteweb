<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Main content -->
    <div class="content">
        <!-- Info notice -->
        <div class="notice outer">
            <div class="note">
                <button type="button" class="close">×</button>
                <strong>Warning!</strong> Best check yo self, you're not looking too good.
            </div>
        </div>
        <!-- /info notice -->
    	<div class="outer">
        	<div class="inner">
                <div class="page-header"><!-- Page header -->
                    <h5><i class="font-home"></i>{lang:Title_Page_Home}</h5>
                    <ul class="icons">
                        <li><a href="#" class="hovertip" title="My tasks"><i class="font-tasks"></i></a></li>
                        <li><a href="#" class="hovertip" title="Reload data"><i class="font-refresh"></i></a></li>
                        <li><a href="#" class="hovertip" title="Settings"><i class="font-cog"></i></a></li>
                    </ul>
                </div><!-- /page header -->
                <div class="body">
                    <!-- Middle navigation standard -->
                    <ul class="midnav">
                        <li><a href="#" title="Add an article"><img src="{function:base_url}assets/images/icons/color/order-149.png" alt="" /><span>Tâches</span></a></li>
                        <li><a href="{function:base_url}Forum" title="Upload files"><img src="{function:base_url}assets/images/icons/color/issue.png" alt="" /><span>Forum</span></a></li>
                        <li><a href="#" title="Add something"><img src="{function:base_url}assets/images/icons/color/hire-me.png" alt="" /><span>Utilisateurs</span></a><strong>17</strong></li>
                        <li><a href="#" title="Check statistics"><img src="{function:base_url}assets/images/icons/color/config.png" alt="" /><span>Parameters</span></a></li>
                    </ul>
                    <!-- /middle navigation standard -->
                    <!-- Content container -->
                    <div class="container"></div>
                    <!-- /content container -->
                </div>
            </div>
        </div>
    </div>
    <!-- /main content -->