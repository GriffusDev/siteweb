<?php $this->load->view('partials/header'); ?>
<!-- Main wrapper -->
<div class="wrapper three-columns">
    <!-- Left sidebar -->
    <div class="sidebar" id="left-sidebar">
        <ul class="navigation standard">
            <li class="active"><a href="{function:base_url}Home" title=""><img src="images/icons/mainnav/dashboard.png" alt="" />{lang:Title_Page_Home}</a></li>
            <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/form-elements.png" alt="" />{lang:Title_Page_Forum}<strong>3</strong></a>
                <ul>
                    <li><a href="{function:base_url}Forum/Config" title="">Configuration</a></li>
                    <li><!--class="current"--><a href="{function:base_url}Forum/Categorie" title="">Gestion des categories</a></li>
                    <li><a href="{function:base_url}Forum/Forums" title="">Gestion des forums</a></li>
                </ul>
            </li>
            <li><a href="#" title="" class="expand"><img src="images/icons/mainnav/form-elements.png" alt="" />{lang:Title_Page_Account}<strong>3</strong></a>
                <ul>
                    <li><a href="{function:base_url}#" title="">Gestion</a></li>
                    <li><!--class="current"--><a href="{function:base_url}#" title="">Droits</a></li>
                    <li><a href="{function:base_url}#" title="">Bann</a></li>
                </ul>
            </li>
            <li><a href="{function:base_url}#" title=""><img src="images/icons/mainnav/buttons-icons.png" alt="" />Icons &amp; buttons</a></li>
            <li><a href="{function:base_url}#" title=""><img src="images/icons/mainnav/charts.png" alt="" />Charts &amp; graphs</a></li>
            <li><a href="{function:base_url}#" title=""><img src="images/icons/mainnav/messages.png" alt="" />Messages</a></li>
            <li><a href="{function:base_url}#" title=""><img src="images/icons/mainnav/tables.png" alt="" />Tables</a></li>
        </ul><!-- /standard nav -->
    </div>
    <!-- /left sidebar -->
    <?php echo $content_for_layout; ?>
    <!-- Right sidebar -->
    <div class="sidebar" id="right-sidebar">
        <div class="appendable">
            <!-- Sidebar stats -->
            <!-- Stats progress -->
            <ul class="outer block buttons" style="background: url({function:base_url}assets/images/backgrounds/default-2.jpg);">
                <li><center><strong>Êtat des serveurss</strong></center></li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 3306, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">Mysql</span>
                </li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 80, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">Apache</span>
                </li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 21, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">FTP</span>
                </li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 3724, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">Auth</span>
                </li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 8086, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">Royaume</span>
                </li>
                <li>
                    <span class="action-button <?php if (@fsockopen('37.187.26.230', 41144, $errno, $errstr, 1) >= 1) {echo ('green');}else{echo ('red');} ?>" style="margin-top: 6px;">TS3</span>
                </li>
            </ul>
            <ul class="progress-statistics block">
                <li><center><strong>Joueurs en ligne :</strong></center></li>
                <li>
                    <strong>Horde</strong><strong class="pull-right">88%</strong>
                    <div class="progress progress-danger"><div style="width: 88%;" class="bar filled-text" data-percentage="88">88%</div></div>
                </li>
                <li>
                    <strong>Alliance</strong><strong class="pull-right">70%</strong>
                    <div class="progress progress-info"><div style="width: 70%;" class="bar filled-text" data-percentage="70">70%</div></div>
                </li>
                <li>
                    <strong>Général</strong><strong class="pull-right">50%</strong>
                    <div class="progress progress-success"><div style="width: 50%;" class="bar filled-text" data-percentage="50">50%</div></div>
                </li>
            </ul>
            <ul class="user-list block">
                <li><center><strong>Staff en ligne :</strong></center></li>
                <li>
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face.png" alt="">
                        <span class="contact-name">
                            <strong>Eugene Kopyov <span>(5)</span></strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_away"></span>
                    </a>
                </li>
                <li class="active">
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face2.png" alt="">
                        <span class="contact-name">
                            <strong>Lucy Wilkinson <span>(12)</span></strong>
                            <i>Team leader</i>
                        </span>
                        <span class="status_off"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face3.png" alt="">
                        <span class="contact-name">
                            <strong>John Dow</strong>
                            <i>PHP developer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face4.png" alt="">
                        <span class="contact-name">
                            <strong>The Incredible</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face5.png" alt="">
                        <span class="contact-name">
                            <strong>The wazzup guy</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
                <li>
                    <a href="#" title="">
                        <img src="{function:base_url}assets/images/live/face6.png" alt="">
                        <span class="contact-name">
                            <strong>Viktor Fedorovich</strong>
                            <i>web &amp; ui designer</i>
                        </span>
                        <span class="status_available"></span>
                    </a>
                </li>
            </ul>
            <!-- /stats progress -->
        </div>
    </div>
    <!-- /right sidebar -->
</div>
<!-- /main wrapper -->
<?php $this->load->view('partials/footer'); ?>