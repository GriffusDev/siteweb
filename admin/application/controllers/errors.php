<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->layout->set_title($this->config->item('Server_Name') . $this->layout->title_separator . lang('Title_Page_Error_404'));
        
        //Bootstrap
        $this->layout->add_includes('css', 'assets/css/main.css');
        //Style
        $this->layout->add_includes('css', 'assets/css/error.css');
        //font-awesome.css
        $this->layout->add_includes('css', 'assets/css/font-awesome.css');
              
        //Jquery
        $this->layout->add_includes('js', 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js', NULL, FALSE);
        $this->layout->add_includes('js', 'assets/js/jquerui.ui.js', NULL, TRUE);
        //Bootstrap
        $this->layout->add_includes('js', 'assets/js/bootstrap.js');
        $this->layout->add_includes('js', 'assets/js/transition.js');
        //Theme scripts
        $this->layout->add_includes('js', 'assets/js/error.js');
        
        //Language
        $this->lang->load('404');
        
    }
    
    public function index() {
        redirect('Errors/error_404');
    }
	
    public function error_404() {
        
        $data = array();
        
        $this->lang->load('404');
        
        $this->layout->view('errors/html/error_404', $data, 'auth');
        
    }
}