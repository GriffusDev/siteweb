<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->layout->set_title($this->config->item('Server_Name') . $this->layout->title_separator . lang('Title_Page_Forum'));
        
        //Bootstrap
        $this->layout->add_includes('css', 'assets/css/main.css');
                                
        //Jquery
        $this->layout->add_includes('js', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', NULL, FALSE);
        //Bootstrap
        $this->layout->add_includes('js', 'assets/js/jquery_ui_custom.js');
        
        if (!$this->session->userdata('logged_in')) {
            
            redirect('Auth', 'refresh');
            
        }
        
    }
	
    public function index() {
                
        $data = array();
        
        $this->layout->view('pages/forum/home', $data);
        
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */