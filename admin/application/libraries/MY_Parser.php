<?php

//Condition d'utilisation :
//Aficher une variable "{var:lavariable}
//Afficher une langue "{lang:laphrase}"
//Afficher une condition :
//"{if var:test == ""}
//condition oui
//{else}
//condition non
//{endif}
//Fonction base_url()
//{function:base_url}
//La suite : http://ellislab.com/codeigniter%20/user-guide/libraries/parser.html

class MY_Parser extends CI_Parser {

    public $l_delim = '{var:';
    public $l_delimIF = '{';
    private $ci;
    
    const LANG_REPLACE_REGEXP = '!\{lang:\s*(?<key>[^\}]+)\}!';
    
    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
    }

    public function parse($template, $data, $return = FALSE) {
        
        $template = $this->ci->load->view($template, $data, TRUE);
        $template = $this->replace_lang_keys($template);
        $template = $this->_parse_function_statements($template, $data);
        $template = $this->parse_function($template);

        return $this->_parse($template, $data, $return);
    }

    protected function replace_lang_keys($template) {
        return preg_replace_callback(self::LANG_REPLACE_REGEXP, array($this, 'replace_lang_key'), $template);
    }

    protected function replace_lang_key($key) {
        return $this->ci->lang->line($key[1]);
    }
    
    protected function parse_function($template) {
        return preg_replace('#{function:base_url}#isU', base_url(), $template);
    }
    
    protected function _parse_function_statements($template, $data) {
        
        if ( ! preg_match_all("|".$this->l_delimIF . "if (.+?)" . $this->r_delim."(.+?)".$this->l_delimIF . "endif" . $this->r_delim."|s", $template, $match) )
        {
            
            return $template;
            
        }
        for ($offset = 0; $offset < sizeof($match[0]); $offset++)
        {
            
            $return = array();
            $return['original'] = trim($match[0][$offset]);
            $return['keyword'] = trim($match[1][$offset]);
            $return['if_data'] = trim($match[2][$offset]);

            if ( preg_match_all("|(.*?)".$this->l_delimIF . "else" . $this->r_delim . "(.*)|s", $match[2][$offset], $else_match) ) {
                $return['else_left'] = trim($else_match[1][0]);
                $return['else_right'] = trim($else_match[2][0]);
            } else {
                $return['else_left'] =  $return['if_data'];
                $return['else_right'] = '';
            }

            $statement = $return['keyword'];
            foreach ( $data as $key => $var ) {
                if (strpos($statement, 'var:'.$key) !== FALSE) {
                    $statement = str_replace('var:'.$key, '$data[\''.$key.'\']', $statement);
                }
            }
            
            $result = '';
            $eval = "\$result = (" . $statement . ") ? 'true' : 'false';";
            eval($eval);

            if ( $result == 'true' ) {
                if ( isset($else_match) && isset($return['else_left']) ) $template = str_replace($return['original'], $return['else_left'], $template);
            } else {
                if ( isset($else_match) && isset($return['else_right'])  ) $template = str_replace($return['original'], $return['else_right'], $template);
                else $template = str_replace($return['original'], $return['if_data'], $template);
            }
            
        }

        return $template;
    }  
    
}