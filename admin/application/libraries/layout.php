<?php

/**
 * -----------------------------------------------
 *
 * This file is part of deathart.
 *
 * Diamond is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Diamond is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Diamond. If not, see <http://www.gnu.org/licenses/>
 *
 * @author Louis BRABANT <louis.brabant@laposte.net>
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * -----------------------------------------------
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Layout {
        
        private $ci;
        
        // the title for the layout
        private $title_for_layout;
        
        // title separator
        // you can change this in the construct
        public $title_separator;
        
        // holds the css and js files
        private $includes;
        
        public function __construct() {
                
                $this->ci = &get_instance();
                
                $this->title_for_layout = NULL;
                $this->title_separator = ' | ';
                
                $this->includes = array();
                
        }
        
        public function set_title($title = NULL) {
                
                $this->title_for_layout = $title;
                
        }
        
        public function add_includes($type, $file, $options = NULL, $prepend_base_url = TRUE) {
                
                if($prepend_base_url) {
                        
                        $this->ci->load->helper('url');
                        $file = base_url() . $file;
                        
                }
                
                $this->includes[$type][] = array(
                        
                        'file' => $file,
                        'options' => $options
                        
                        
                );
                
                // allows chaining
                return $this;
                
        }
        
        public function view($name, $data = array(), $layout = 'default') {
                
                // get the contents of the view and store it
                $obj =& get_instance();
                $obj->load->library('MY_Parser');
                $view = $obj->parser->parse($name, $data, TRUE);
                
                // set the title
                $title = '';
                
                if($this->title_for_layout !== NULL) {
                        
                        $title = $this->title_for_layout;
                        
                }
                
                $obj->parser->parse('layouts/' . $layout, array(
                        
                        'title_for_layout' => $title,
                        'includes_for_layout' => $this->includes,
                        'content_for_layout' => $view
                        
                ));
                
        }
        
}

/* End of file layout.php */
/* Location: ./application/libraries/layout.php */