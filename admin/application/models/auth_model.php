<?php

/**
 * -----------------------------------------------
 *
 * This file is part of deathart.
 *
 * Diamond is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Diamond is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Diamond. If not, see <http://www.gnu.org/licenses/>
 *
 * @author Louis BRABANT <louis.brabant@laposte.net>
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * -----------------------------------------------
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class auth_model extends CI_Model {
    
    public function __construct(){
        
        parent::__construct();
        
    }
    
    public function VerifPseudo ($pseudo) {
        
        $DB3 = $this->load->database('auth', TRUE);
        $pseudoexiste = $DB3->query('SELECT * FROM account WHERE username="'.strtoupper($pseudo).'"');
        
        if($pseudoexiste->num_rows() > 0) {
            return 1;
        }
        else {
            return 0;
        }
        
    }
    
    public function VerifPassword ($pseudo, $password) {
        
        $PasswordCrypt = strtoupper(sha1(strtoupper("".$pseudo.":".$password."")));
        $DB3 = $this->load->database('auth', TRUE);
        $recup = $DB3->query('SELECT id, sha_pass_hash FROM account WHERE username="'.strtoupper($pseudo).'"')->row();

        if ($PasswordCrypt == $recup->sha_pass_hash) {
            if ($this->RecupRank($recup->id) >= 3) {
                if ($this->RecupGMlevel($recup->id) >= 2) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
        
    }
    
    public function RecupID ($pseudo) {
        
        $DB3 = $this->load->database('auth', TRUE);
        $recup = $DB3->query('SELECT id FROM account WHERE username="'.strtoupper($pseudo).'"')->row();
        return $recup->id;
        
    }
    
    public function RecupRank ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        
        $recup = $DB3->query("SELECT membre_rank FROM membres WHERE id='".$id."'")->row();
        
        return $recup->membre_rank;
        
    }
    
    public function RecupGMlevel ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        
        $recup = $DB3->query("SELECT membre_gmlevel FROM membres WHERE id='".$id."'")->row();
        
        return $recup->membre_gmlevel;
        
    }
    
    public function avatar ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        
        $recup_avatar = $DB1->query("SELECT membre_avatar FROM membres WHERE id='".$id."'")->row();
        
        return $recup_avatar->membre_avatar;
        
    }
    
    
    
}