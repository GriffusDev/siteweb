$(document).ready(function(){
    
    var loginbox = $('#loginform');

    $('#loginform').submit(function(e){
        
        $('#loginform').prepend('<br /><div class="alert alert-warning">Connexion en cours...</div>');
        
        var userinput = $('#username');
        var passinput = $('#password');
        var hrefs = $('#hrefs').val();
        
        if(userinput.val() === '' || passinput.val() === '') {
            highlight_error(userinput);
            highlight_error(passinput);
            loginbox.effect('shake');
            return false;
        } else {
           
            $.ajax({
                type: "POST",
                url: hrefs + "Auth/Pseudo",
                async: false,
                data: "pseudo="+userinput.val(),
                dataType: "html",
                success: function(html) {
                    if(html == 1) 
                    {
                        $.ajax({
                            type: "POST",
                            url: hrefs + "Auth/Login",
                            async: false,
                            data: "pseudo="+userinput.val()+"&mdp="+passinput.val(),
                            dataType: "html",
                            success: function(msg){
                                if(msg == 1) 
                                {
                                    loginbox.animate({'top':'+=100px','opacity':'0'},250,function(){});
                                    $('#loginbox').html('<br /><br /><br /><br /><br /><div class="alert alert-success">Connecté, redirection en cours...</div>');
                                    window.setTimeout(function(){
                                        window.location.reload(true);
                                    },3000);
                                    return false;
                                }
                                else {
                                    highlight_error(userinput);
                                    highlight_error(passinput)
                                    passinput.addClass('has-error');
                                    loginbox.effect('shake');
                                    $('#loginform').prepend('<br /><div class="alert alert-danger">Mots de pass incorrect</div>');
                                    return false;
                                }
                            }
                        });
                        return false;
                    }
                    else {
                        highlight_error(userinput);
                        highlight_error(passinput)
                        userinput.addClass('has-error');
                        loginbox.effect('shake');
                        $('#loginform').prepend('<br /><div class="alert alert-danger">Pseudo incorrect</div>');
                        return false;
                    }
                }
            
            });
            
        }
        
    });

    $('#username, #password').on('keyup',function(){
        highlight_error2($(this));
    }).focus(function(){
        highlight_error2($(this));
    }).blur(function(){
        highlight_error2($(this));
    });
    
});

function highlight_error(el) {
    if(el.val() == '') {
        el.parent().addClass('has-error');
    } else {
        el.parent().removeClass('has-error');
    }
}

function highlight_error2(el) {
    if(el.val() == '') {
        el.parent().removeClass('has-error');
    } else {
        el.parent().removeClass('has-error');
    }
}