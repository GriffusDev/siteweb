<script>
////////////////////////////////////////////////////////
// Password edit Action
///////////////////////////////////////////////////////// 
$('#formchangepass').submit(function() 
{
	var pass = $('#pass').val();
	var newpass = $('#NewPass').val();
	var newpassverif = $('#NewPassVerif').val();
	$.ajax({
		type: 'POST',
		url: 'pages/update_mdp.php',
		data: 'pass='+pass+'&newpass='+newpass+'&newpassverif='+newpassverif,
		success: function(html) 
		{
			switch(parseInt(html))
			{
				case 0:
				{
					$('#center').fadeIn(500, function(){
						$('.nt_error').remove();
						$('#center').prepend('<div class="nt_error"><p>Vous devez être connecté pour effectuer cette action</p></div>');
						$('.nt_error').fadeOut(2000);
					});
					break;
				}
				case 1:
				{
					$('#center').fadeIn(500, function(){
					$('.nt_error').remove();
					$('#center').prepend('<div class="nt_error"><p>Mauvais mot de passe</p></div>');
					$('#pass').val('');
					$('#NewPass').val('');
					$('#NewPassVerif').val('');
					$('.nt_error').fadeOut(4500);
					});
					break;
				}
				case 2:
				{
					$('#center').fadeIn(500, function(){
					$('.nt_error').remove();
					$('#center').prepend('<div class="nt_error"><p>Le nouveau mot de passe doit contenir au minimum 6 caractères</p></div>');
					$('#NewPass').val('');
					$('#NewPassVerif').val('');
					$('.nt_error').fadeOut(4500);
					});
					break;
				}
				case 3:
				{
					$('#center').fadeIn(500, function(){
					$('.nt_error').remove();
					$('#center').prepend('<div class="nt_error"><p>Les deux mots de passe doivent être identiques</p></div>');
					$('#NewPass').val('');
					$('#NewPassVerif').val('');
					$('.nt_error').fadeOut(4500);
					});
					break;
				}
				case 4:
				{
					$('#center').fadeIn(500, function(){
					$('.nt_error').remove();
					$('#center').prepend('<div class="nt_error"><p>Le nouveau mot de passe est identique à celui actuel</p></div>');
					$('#NewPass').val('');
					$('#NewPassVerif').val('');
					$('.nt_error').fadeOut(4500);
					});
					break;
				}
				case 5:
				{
					$('#center').fadeIn(500, function(){
					$('.success').remove();
					$('#center').prepend('<div class="success"><p>Votre mot de passe a été changé, redirection en cours</p></div>');

					$('.success').fadeOut(2000, function(){
							window.location.href = 'Account'
					});
					});
					break;
				}
			}
		}
	});
	return false;
});
</script>