////////////////////////////////////////////////////////
// Notification add
/////////////////////////////////////////////////////////
function NotificationDiv()
{
        this.Add = function(page, type, texte)
        {
                switch(page)
                {
                    case 'logout':
                    {
                        switch(type) 
                        {
                            case 'success': 
                            {
                                $('#center').fadeIn(500, function(){
                                    $('.nt_success').remove();
                                    $('.nt_warning').remove();
                                    $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                    $('.nt_success').fadeOut(3500, function(){
                                        window.location.href = 'http://' + window.location.host + '/';
                                    });
                                });
                                return false;
                                break;
                            }
                        }
                        break;
                    }
                    case 'login':
                    {
                            switch(type)
                            {
                                    case 'error':
                                    {
                                            $('#center').fadeIn(10000, function()
                                            {
                                                    $('.nt_error').remove();
                                                    $('.nt_warning').remove();
                                                    $('#center').prepend('<div class="nt_error"><div class="nt_title_error">Une erreur est survenue</div><div class="nt_text_error">' + texte + '</div></div>');
                                                    $('.nt_error').fadeOut(3500, function(){
                                                            $('form[id=formlogin] input[type=submit]').removeAttr('disabled');
                                                            $('form[id=formlogin] input[type=text]').css('border','');
                                                            $('form[id=formlogin] input[type=password]').css('border','');
                                                    });
                                            });
                                            break;
                                    }
                                    case 'success':
                                    {
                                            $('#formlogin').remove();
                                            $('#login').html('<center><img src="http://i.stack.imgur.com/qq8AE.gif" alt="" width="60px" height="60px" style="margin-top: 70px;"/></center>');
                                            $('#center').fadeIn(500, function(){
                                                $('.nt_success').remove();
                                                $('.nt_warning').remove();
                                                $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                                $('.nt_success').fadeOut(3500, function(){
                                                    location.reload();
                                                });
                                            });
                                            break;
                                    }
                            }
                            break;
                    }
                    case 'register':
                    {
                            switch(type)
                            {
                                    case 'error':
                                    {
                                            $('#registerbg').fadeIn(500, function()
                                            {
                                                    $('.nt_error').remove();
                                                    $('.nt_warning').remove();
                                                    $('#registerbg').prepend('<div class="nt_error">'+ texte + '</div>');
                                                    $('form[id=formregister] input[id=logina]').css('border','solid 1px #FF0000');
                                                    $('form[id=formregister] input[type=submit]').attr('disabled','disabled');
                                                    $('.nt_error').fadeOut(3500, function(){
                                                                    $('form[id=formregister] input[type=submit]').removeAttr('disabled');
                                                    })
                                            });
                                            break;
                                    }
                            }
                            break;
                    }
                    
                    case 'forum':
                    {
                            switch(type)
                            {
                                    case 'errorrep':
                                    {
                                        $('form[class=reponserapide] input[type=submit]').attr('disabled','disabled');
                                        $('form[class=reponserapide] input[type=text]').css('border','solid 1px #FF0000');
                                        $('#center').fadeIn(500, function(){
                                            $('.nt_warning').remove();
                                            $('.nt_error').remove();
                                            $('#center').prepend('<div class="nt_error"><div class="nt_title_error">Une erreur est survenue</div><div class="nt_text_error">' + texte + '</div></div>');
                                            $('.nt_error').fadeOut(2000, function(){
                                                $('form[class=reponserapide] input[type=submit]').attr('disabled','');
                                                $('form[class=reponserapide] input[type=text]').css('border','');
                                                return false;
                                            });
                                        });
                                        break;
                                    }
                                    case 'successrep':
                                    {
                                        $('form[class=reponserapide] input[type=submit]').attr('disabled','');
                                        $('#center').fadeIn(500, function(){
                                            $('.nt_warning').remove();
                                            $('.nt_success').remove();
                                            $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                            $('.nt_success').fadeOut(3500, function(){
                                                location.reload();
                                            });
                                        });
                                        break;
                                    }
                            }
                            break;
                    }
                    
        case 'general':
        {
            switch(type)
            {
                case 'warning':
                {
                    $('#center').fadeIn(500, function(){
                        $('.nt_warning').remove();
                        $('#center').prepend('<div class="nt_warning"><div class="nt_title_warning">Chargement...</div><div class="nt_text_warning">' + texte + '</div></div>');
                    });
                    return false;
                    break;
                }
            }
            break;
        }

        }

        }
}

////////////////////////////////////////////////////////
// Var Init
///////////////////////////////////////////////////////// 
var Notification = new NotificationDiv();