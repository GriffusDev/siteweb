<script>
////////////////////////////////////////////////////////
// Armory Home
/////////////////////////////////////////////////////////
$("#armoryportal").submit( function() 
{ 
    var recherche = $('#search').val();
    var hrefs = $('#href').val();
    
    if (recherche === "") 
	{
        $('form[id=armoryportal] input[type=submit]').attr('disabled','disabled');
        $("#search").css('border','solid 1px #FF0000');
        
        $('#center').fadeIn(500, function(){
            $('.nt_error').remove();
            $('#center').prepend('<div class="nt_error">Merci de remplire le champs</div>');
            $('.nt_error').fadeOut(5000, function(){
                $('form[id=armoryportal] input[type=submit]').removeAttr('disabled');
		$('#search').css('border','solid 1px green');
            });
        });
        return false;
    }
    else 
	{
        $('#armoryportal').remove();
        $('#center').fadeIn(500, function()
		{
            $('.success').remove();
            $('#center').prepend('<div class="success"><img src="http://shindorei-serveur.com/img/loading.gif" width="16px" height="16px"/>Recherche en cours, merci de patienter...</div>');
            $('.success').fadeOut(5000, function(){
                document.location.href=hrefs+"Armory/Search/"+recherche;
            });
        });
        return false;
    }
    return false;
});
</script>