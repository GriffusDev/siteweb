<script>
////////////////////////////////////////////////////////
// AddVote
/////////////////////////////////////////////////////////
﻿$(document).ready(function()
{    
    $('#gowonda').click(function()
	{ 
        var account = $(this).attr('class');
        
        $.ajax({
            type: 'GET',
            url: 'scripts/Ajax/AddVote.php',
            data: 'vote=1',
            success: function(html)
			{
				switch(parseInt(html))
				{
					case 1:
					{
						$('#center').fadeIn(500, function()
						{
							$('.success').remove();
							$('.vote').prepend('<div class="success">Votre vote à bien été pris en compte, merci beaucoup !</div>');
							$('.success').fadeOut(4000, function(){
								location.reload();
							});
						});
						return false;
						break;
					}
					case 0: 
					{
						$('.nt_error').remove();
						$('#center').fadeIn(500, function(){
							$('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
							$('.nt_error').fadeOut(7500);
						});
						return false;
						break;
					}
				}
            }
        });
        
        return false;
        
    });
    
    $('#rpg').click(function(){
        
        var account = $(this).attr('class');
        
        $.ajax({
            type: 'GET',
            url: 'scripts/Ajax/AddVote.php',
            data: 'vote=2',
            success: function(html)
			{
				switch(parseInt(html))
				{
					case 1:
					{
						$('#center').fadeIn(500, function(){
							$('.success').remove();
							$('.vote').prepend('<div class="success">Votre vote à bien été pris en compte, merci beaucoup !</div>');
							$('.success').fadeOut(4000, function(){
								location.reload();
							});
						});
						return false;
						break;
					}
					case 0:
					{
						$('.nt_error').remove();
						$('#center').fadeIn(500, function(){
							$('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
							$('.nt_error').fadeOut(7500);
						});
						return false;
						break;
					}
				}
            }
        });       
        return false;        
    });    
});
</script>