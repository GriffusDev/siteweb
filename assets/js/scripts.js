////////////////////////////////////////////////////////
// BBCode
/////////////////////////////////////////////////////////
function bbcode(bbdebut, bbfin) 
{
    var input = window.document.formulaire.message;
    input.focus();
    if(typeof document.selection != 'undefined')
    {
        var range = document.selection.createRange();
        var insText = range.text;
        range.text = bbdebut + insText + bbfin;
        range = document.selection.createRange();
        if (insText.length == 0)
            range.move('character', -bbfin.length);
        else
            range.moveStart('character', bbdebut.length + insText.length + bbfin.length);
        range.select();
    }
    else if(typeof input.selectionStart != 'undefined')
    {
        var start = input.selectionStart;
        var end = input.selectionEnd;
        var insText = input.value.substring(start, end);
        input.value = input.value.substr(0, start) + bbdebut + insText + bbfin + input.value.substr(end);
        var pos;
        if (insText.length == 0)
            pos = start + bbdebut.length;
        else
            pos = start + bbdebut.length + insText.length + bbfin.length;

        input.selectionStart = pos;
        input.selectionEnd = pos;
    }
    else
    {
        var pos;
        var re = new RegExp('^[0-9]{0,3}$');
        
		while(!re.test(pos))
            pos = prompt("insertion (0.." + input.value.length + "):", "0");
        if(pos > input.value.length)
			pos = input.value.length;
			
        var insText = prompt("Veuillez taper le texte");
        input.value = input.value.substr(0, pos) + bbdebut + insText + bbfin + input.value.substr(pos);
    }
    
}

////////////////////////////////////////////////////////
// BBCode Img
/////////////////////////////////////////////////////////
function smilies(img) 
{
	window.document.formulaire.message.value += '' + img + '';
}

////////////////////////////////////////////////////////
// Right click
/////////////////////////////////////////////////////////
var request_uri = window.location.protocol+"//"+window.location.host+"/";

ejs_context_elemt = new Array;
ejs_context_elemt[0] = "Page précédente|history.go(-1)";
ejs_context_elemt[1] = "Page suivante|history.go(1)";
ejs_context_elemt[2] = "";
ejs_context_elemt[3] = 'Accueil|document.location.href=request_uri+\'Accueil\'';
ejs_context_elemt[4] = 'Inscription|window.location.href=request_uri+\'Register\'';
ejs_context_elemt[5] = 'Forum|document.location.href=request_uri+\'Forum\'';
ejs_context_elemt[6] = 'Vote|document.location.href=request_uri+\'Vote\'';
ejs_context_elemt[7] = 'Boutique|document.location.href=request_uri+\'Store\'';
ejs_context_elemt[8] = 'Armurerie|document.location.href=request_uri+\'Armory\'';
function ejs_context_position(e)
{
    ejs_context_x = (navigator.appName.substring(0,3) == "Net") ? e.pageX : event.x+document.body.scrollLeft;
    ejs_context_y = (navigator.appName.substring(0,3) == "Net") ? e.pageY : event.y+document.body.scrollTop;
}
 
function ejs_context_open()
{
    document.getElementById("ejs_context_box").style.top = ejs_context_y;
    document.getElementById("ejs_context_box").style.left = ejs_context_x;
    document.getElementById("ejs_context_ombre").style.top = ejs_context_y+2;
    document.getElementById("ejs_context_ombre").style.left = ejs_context_x+2;
    document.getElementById("ejs_context_box").style.visibility = "visible";
    document.getElementById("ejs_context_ombre").style.visibility = "visible";
    return(false);
}
 
function ejs_context_close()
{
    if (document.getElementById)
    {
        document.getElementById("ejs_context_box").style.top = 0;
        document.getElementById("ejs_context_box").style.left = 0;
        document.getElementById("ejs_context_ombre").style.top = 0;
        document.getElementById("ejs_context_ombre").style.left = 0;
        document.getElementById("ejs_context_box").style.visibility = "hidden";
        document.getElementById("ejs_context_ombre").style.visibility = "hidden";
    }
}
 
function ejs_context_hl(mode, element)
{
    if(mode == 1)
    {
        element.style.background = '#316AC5';
        element.style.color = '#ffffff';
    }
    else
    {
        element.style.background ='#ffffff';
        element.style.color = '#000000';
    }
}
 
if(navigator.appName.substring(0,3) == "Net") 
{
    document.captureEvents(Event.MOUSEMOVE);
}