<h1 class="center_first"><?php echo SHOP_TITLE; ?></h1>
<?php
if(Session::VerifSession('login'))
{
    echo ('Veuillez vous connecter');
}
else {
    $Store = new Store($BddSite, $BddLogon, $BddChar);
    ?>
    <div class="StoreHome">
        <p>
            <span class="ClickItem" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/armure.png" /></span>
            <span class="ClickMount" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/animal.png" /></span>
            <span class="ClickMascotte" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/compagnons.jpg" /></span>
            <span class="ClickLevel" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/level.jpg" /></span>
            <span class="ClickPO" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/or.png" /></span>
            <br />-------<br />
            <span class="ClickFaction" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/animal.png" /></span>
            <span class="ClickRace" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/animal.png" /></span>
            <span class="ClickPerso" style="cursor: pointer;"><img src="<?php echo Url::base_url(); ?>img/boutique/animal.png" /></span>
        </p>
    </div>
    <section>
        <p class="StoreItem" style="display: none;">
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            <?php
            foreach ($Store->RecupListeItem("set") as $row) {
                echo '<p class="ClickAddItem" id="'.$row['ID'].'" style="cursor: pointer;display: none;">'.$row['name'].' (x '.$row['quantity'].') pour '.$row['price'].' points</p>';
            }
            ?>
        </p>
        <p class="StoreItemAddSection"></p>
        <p class="StoreItemAddSectionTY"></p>
    </section>
    <div class="StoreMount" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            <?php
            foreach ($Store->RecupListeItem("montures") as $row) {
                echo '<p class="ClickAddMount" id="'.$row['ID'].'" style="cursor: pointer;display: none;">'.$row['name'].' (x '.$row['quantity'].') pour '.$row['price'].' points</p>';
            }
            ?>
        </p>
        <p class="StoreItemAddSection"></p>
        <p class="StoreItemAddSectionTY"></p>
    </div>
    <div class="StoreMascotte" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            <?php
            foreach ($Store->RecupListeItem("mascottes") as $row) {
                echo '<p class="ClickAddMascotte" id="'.$row['ID'].'" style="cursor: pointer;display: none;">'.$row['name'].' (x '.$row['quantity'].') pour '.$row['price'].' points</p>';
            }
            ?>
        </p>
        <p class="StoreItemAddSection"></p>
        <p class="StoreItemAddSectionTY"></p>
    </div>
    <div class="StoreLevel" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            <?php
            foreach ($Store->RecupListeItem("level") as $row) {
               echo '<p class="ClickAddLevel" id="'.$row['ID'].'" style="cursor: pointer;display: none;">'.$row['name'].' (x '.$row['quantity'].') pour '.$row['price'].' points</p>';
            }
            ?>
        </p>
        <p class="StoreItemAddSection"></p>
        <p class="StoreItemAddSectionTY"></p>
    </div>
    <div class="StorePO" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            <?php
            foreach ($Store->RecupListeItem("pieces") as $row) {
                echo '<p class="ClickAddPO" id="'.$row['ID'].'" style="cursor: pointer;display: none;">'.$row['name'].' (x '.$row['quantity'].') pour '.$row['price'].' points</p>';
            }
            ?>
        </p>
        <p class="StorePOAddSection"></p>
        <p class="StorePOAddSectionTY"></p>
    </div>
    <div class="StoreFaction" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            test Faction
        </p>
    </div>
    <div class="StoreRace" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            test Race
        </p>
    </div>
    <div class="StorePerso" style="display: none;">
        <p>
            <span class="ClickBack ButtonFofo" style="cursor: pointer;">Revenir</span><br />
            test Perso
        </p>
    </div>
    <?php
}
?>