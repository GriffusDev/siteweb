<h1>Accueil Armurerie</h1>
<p>L'armurerie vous permet de chercher un Personnage, une Guilde, ou encore une Team d'arène. </p>

<form id="armoryportal" name="armoryportal" method="post">
    <input type="text" name="search" id="search" placeholder="Recherche...">
    <input type="text" value="<?php echo url::base_url(); ?>" name="href" id="href" style="display: none;" placeholder="Lien">
    <br />
    <button type="submit" class="btn ButtonFofo btn-xs">Rechercher</button>
</form>