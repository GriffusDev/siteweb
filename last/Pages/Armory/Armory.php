<?php
//Les autres pages
if(!isset($_GET['pages'])) {
    url::redirect('Armory/Home');
}else {
   
    switch($_GET['pages'])
    {
        case 1:
            include(Url::GetAbsoluteLink('Pages/Armory/Home.php'));
        break;
    
        case 2:
            include (Url::GetAbsoluteLink('Pages/Armory/Search.php'));
        break;
    
        case 3:
            include (Url::GetAbsoluteLink('Pages/Armory/Guid.php'));
        break;

        default:
            Url::redirect('Armory/Home');
        break;

    }
    
}