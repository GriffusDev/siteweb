<h1>Recherche dans l'armurerie</h1>
<p>
    <?php
    if (isset($_GET['search'])) {
        $requete = htmlentities($_GET['search']);
        $bdd_char = sqlconnect("char");
        $requete_char = $bdd_char->query("SELECT * FROM characters WHERE name LIKE '%$requete%' ORDER BY guid DESC");
        $result_char = $requete_char->rowCount();
        $requete_guild = $bdd_char->query("SELECT * FROM guild WHERE name LIKE '%$requete%' ORDER BY guildid DESC");
        $result_guild = $requete_guild->rowCount();
        $requete_arena = $bdd_char->query("SELECT * FROM arena_team WHERE name LIKE '%$requete%' ORDER BY arenaTeamId DESC");
        $result_arena = $requete_arena->rowCount();
        ?>
        Personnages :
        <br />
        <?php
        if ($result_char >= 1) {
            foreach ($requete_char as $charact) {
                echo ('<a href="'.url::base_url().'Armory/Guid/'.$charact['guid'].'">'.$charact['name'].'</a><br />');
            }
        }
        else {
            echo ('Aucun personnage');
        }
        ?>
        <br />
        Guilde :
        <br />
        <?php
        if ($result_guild >= 1) {
            foreach ($requete_guild as $charact) {
                echo ('<a href="'.url::base_url().'Armory/Guild/'.$charact['guildid'].'">'.$charact['name'].'</a><br />');
            }
        }
        else {
            echo ('Aucune guilde');
        }
        ?>
        <br />
        Arene :
        <br />
        <?php
        if ($result_arena >= 1) {
            foreach ($requete_arena as $charact) {
                echo ('<a href="'.url::base_url().'Armory/Arena/'.$charact['arenaTeamId'].'">'.$charact['name'].'</a><br />');
            }
        }
        else {
            echo ('Aucune team d\'arene');
        }
    }
    else {
        url::redirect('Armory/Home');
    }
    ?>
</p>