<?php
//On charge la class de news 
if(isset($_GET['news'])) {
    include ($_SERVER['DOCUMENT_ROOT'].'/scripts/Class/Account.class.php');
    $Account = new Account($BddSite, $BddLogon, $BddChar);
    foreach ($News->get_news($_GET['news']) as $news): 
    ?>
    <h1 class="center_first"><?php echo $news['titre']; ?></h1>
    <div class="stafflogo"></div>
    <div class="center_contenu">
        <div class="news_picture"><img src="<?php echo Url::base_url(); ?>img/News/tech.png" alt="" /></div>
        <div class="news_text"><?php echo Parsing::BBCode($news['news']); ?></div>
        <div class="clear"></div>
    </div>
    <h1 style="text-align: center;">Commentaire(s)</h1>
    <?php
    foreach ($News->get_liste_comm($_GET['news']) as $comm):
    echo ('<div class="sep"></div>');
    if ($Account->RecupGMLevel($comm['poster']) >= 2) {
        echo ('<div class="stafflogo"></div>');
    }
    ?>
    <div class="center_contenu">
        <div class="news_picture"><img src="<?php echo Url::base_url(); ?>img/Avatar/felindro.png" alt="" /></div>
        <div class="news_text">
            <div class="comm_info">
                Par <?php echo $Account->RecupPseudo($comm['poster']) .' / '. $comm['date']; ?>
            </div>
            <?php echo Parsing::BBCode($comm['commentaire']); ?>
        </div>
        <div class="clear"></div>
    </div>
    <?php
    endforeach;
    ?>
    <div class="sep"></div>
    <section class="commentaire">
        <img class="comm_avatar" src="<?php echo Url::base_url(); ?>img/Avatar/noavatar.jpg" height="100px" width="100px" />
        <div class="comm">
            <div class="comm_info">
                Ajouter un commentaire
            </div>
            <div class="comm_text">
                <?php
                if(session::VerifSession('login')) {
                echo ('Merci de vous connecter ou de vous <a href="'.Url::base_url().'Register">Inscrire</a>');
                }
                else {
                ?>
                    <div class="ComAdd" style="cursor: pointer;">
                        Cliquer ici pour ajouter un commentaire...
                    </div>
                    <div class="ComAdd2" style="display: none;">
                        <form class="formAddCom" name="formulaire">
                            <input type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('<gras>', '</gras>');return(false)" class="btn ButtonFofo btn-xs" />
                            <input type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('<italique>', '</italique>');return(false)" class="btn ButtonFofo btn-xs" />
                            <input type="button" id="souligné" name="souligné" value="Souligné" onClick="javascript:bbcode('<souligne>', '</souligne>');return(false)" class="btn ButtonFofo btn-xs" />
                            <input type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('<lien>', '</lien>');return(false)" class="btn btn-sm ButtonFofo" />
                            <input type="button" id="image" name="image" value="Images" onClick="javascript:bbcode('<images>', '</images>');return(false)" class="btn btn-sm ButtonFofo" />
                            <br />
                            <img src="http://fr.openclassrooms.com/Templates/images/smilies/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(' :D ');return(false)" />
                            <img src="http://fr.openclassrooms.com/Templates/images/smilies/rire.gif" title="lol" alt="lol" onClick="javascript:smilies(' :lol: ');return(false)" />
                            <img src="http://fr.openclassrooms.com/Templates/images/smilies/triste.png" title="triste" alt="triste" onClick="javascript:smilies(' :( ');return(false)" />
                            <img src="http://fr.openclassrooms.com/Templates/images/smilies/huh.png" title="choc" alt="choc" onClick="javascript:smilies(' :o ');return(false)" />
                            <br />
                            <textarea class="TextCommentaire" id="message" rows="3" placeholder="Commentaire..." name="message"></textarea>
                            <br />
                            <button type="submit" class="btn ButtonFofo btn-xs">Ajouter</button>
                            <br />
                            <br />
                            <input type="text" value="<?php echo Url::base_url(); ?>" name="href" id="href" style="display: none;" placeholder="Lien">
                            <input type="text" value="<?php echo $_SESSION['id']; ?>" name="par" id="par" style="display: none;" placeholder="Lien">
                            <input type="text" value="<?php echo Url::LastSegment(); ?>" name="id" id="id" style="display: none;" placeholder="Lien">
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
    </section>
    <?php
    endforeach;
}
else {
    url::redirect('Accueil');
}