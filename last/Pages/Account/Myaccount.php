<?php
if(!isset($_SESSION['login']))
	echo'<p>'.ERROR_MUST_LOGGED.'</p>';
else
{
	$account = new Account($BddSite, $BddLogon, $BddChar);
	
	echo'
	<h1>'.MA_TITLE.'</h1>
	<section class="accountheader">
		<div class="ma_column">
			<div class="ma_field">	
				<div class="accountfield">'.MA_ACCOUNT.' : </div>
				<div class="accountvalue">'.$account->RecupPseudo($_SESSION['id']).'</div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_EMAIL.' : </div>
				<div class="accountvalue">'.$account->RecupMail($_SESSION['id']).'</div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_CHARACTER.' : </div>
				<div class="accountvalue">'.$account->RecupCharacter($_SESSION['id']).'</div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_IP.' : </div>
				<div class="accountvalue">'.$account->RecupIP($_SESSION['id']).'</div>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="ma_column">
			<div class="ma_field">	
				<div class="accountfield">'.MA_POINTVOTE_NUMBER.' : </div>
				<div class="accountvalue">'.$account->RecupNBVote($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_GETMORE.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_POINTSHINS_NUMBER.' : </div>
				<div class="accountvalue">'.$account->RecupNBPoint($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_GETMORE.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_WARNING.' : </div>
				<div class="accountvalue">'.$account->RecupWarning($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_VIEW.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_PURCHASESTORE.' : </div>
				<div class="accountvalue">'.$account->RecupListPurchaseStore($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_VIEW.'</a></div>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="ma_column">
			<div class="ma_field">	
				<div class="accountfield">'.MA_SUPPORTPENDING.' : </div>
				<div class="accountvalue">0</div>
				<div class="accountlink"><a href="#">'.MA_VIEW.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_AUCTIONPENDING.' : </div>
				<div class="accountvalue">'.$account->RecupListAuctionPending($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_GETMORE.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_PURCHASEPENDING.' : </div>
				<div class="accountvalue">'.$account->RecupListPurchasePending($_SESSION['id']).'</div>
				<div class="accountlink"><a href="#">'.MA_VIEW.'</a></div>
				<div class="clear"></div>
			</div>
			<div class="ma_field">	
				<div class="accountfield">'.MA_LOGINHISTORY.'</div>
				<div class="accountvalue"></div>
				<div class="accountlink"><a href="#">'.MA_VIEW.'</a></div>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="ma_avatar">
			<img src="img/1600/noavatar.png" alt="" />
		</div>

		<div class="clear"></div>
	</section>

    <a href="'.Url::base_url().'ChangePassword" class="ma_password ma_button"></a>
	<a href="'.Url::base_url().'Help/Account" class="ma_store ma_button"></a>
	<a href="'.Url::base_url().'Store" class="ma_faction ma_button"></a>
	<a href="'.Url::base_url().'Store" class="ma_point ma_button"></a>
	<a href="'.Url::base_url().'Store" class="ma_block ma_button"></a>
	<a href="'.Url::base_url().'Store" class="ma_race ma_button"></a>
	';
	
	
}
?>

</body>
</html>