<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1);
 
//initialisation des sessions
session_start();

// include du fichier sql
include('../scripts/php/sql.php');

//include des defines
include('../scripts/php/define.php');

//include des fichiers class
include('../scripts/class/monitoring.class.php');
include('../scripts/class/mysql.class.php');
include('../scripts/class/url.class.php');
include('../scripts/class/session.class.php');

$pass = $_POST['pass'];
$newpass = $_POST['newpass'];
$newpassverif = $_POST['newpassverif'];

$mdp = strtoupper(sha1(strtoupper(''.$_SESSION['login'].'').':'.strtoupper(''.$pass.'')));
$bdd = sqlconnect("logon");
$req=$bdd->prepare('SELECT id FROM account WHERE username = ? AND sha_pass_hash= ?');
$req->execute(array($_SESSION['login'],$mdp));
$resultat=$req->fetch();

if($resultat)
{
	if(strlen($newpass) >= 6)
	{
		if($newpassverif == $newpass)
		{
			if($newpass != $pass)
			{
				$bdd = sqlconnect("logon");
				$new_pass_hache= strtoupper(sha1(strtoupper(''.$_SESSION['login'].'').':'.strtoupper(''.$newpass.'')));
				$req=$bdd->prepare('UPDATE account SET sha_pass_hash=? WHERE username=?');
				$req->execute(array($new_pass_hache,$_SESSION['login']));
				$req->closeCursor();
				echo 5;
			}
			else
			{
				echo 4;
				$error = 1;
			}
		}
		else
		{
			echo 3;
			$error = 1;
		}
	}
	else
	{
		echo 2;
		$error = 1;
	}
}
else
{
	echo 1;
	$error = 1;
}

?>