<h1 class="center_first"><?php echo VOTE_TITLE; ?></h1>
<p class="vote">
<?php
if(Session::VerifSession('login')){
    echo ('<div class="error">'.VOTE_LOGOFF.'</div>');
    echo ('<a href="#" target="_blank" id="gowonda">Gowonda</a> ');
    echo ('<a href="#" target="_blank" id="rpg">RPG</a>');
}
else {
    $vote = new Vote($BddSite);
    if ($vote->Oneverif($_SESSION['id'], 1) === TRUE) {
        echo ('<a href="#" target="_blank" id="gowonda" class="'.$_SESSION['id'].'">Gowonda</a>');
    }
    else {
        echo ('Gowonda');
    }
	
    if ($vote->Oneverif($_SESSION['id'], 2) === TRUE) {
        echo ('<a href="#" target="_blank" id="rpg" class='.$_SESSION['id'].'>RPG</a>');
    }
    else {
        echo ('RPG');
    }
}
?>