<?php
$forum = $_GET['forum'];
foreach ($Forum->GetForum($forum) as $data) {
    $nombreDeMessagesParPage = 25;
    $totalDesMessages = $data['forum_topic'] + 1;
    $nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);
    $page = (isset($_GET['pagination']))?intval($_GET['pagination']):1;
    echo '<p>Page : ';
    for ($i = 1 ; $i <= $nombreDePages ; $i++)
    {
        if ($i == $page)
        {
        echo $i;
        }
        else
        {
        echo '
        <a href="'.Url::base_url().'/Forum/Forums/'.$forum.'/'.$i.'">'.$i.'</a>';
        }
    }
    echo '</p>';
    $premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

    echo '<h1>'.stripslashes(htmlspecialchars($data['forum_name'])).'</h1><br /><br />';
    if(!Session::VerifSession('login'))
    {
        if ($_SESSION['lvl'] >= 1) {
            echo'<a href="'.Url::base_url().'Forum/AddTopic/'.$forum.'"><img src="./images/nouveau.gif" alt="Nouveau sujet" title="Poster un nouveau sujet" /></a>';
        }
    }
}
?>
<table>   
<tr>
    <th><img src="./images/annonce.gif" alt="Annonce" /></th>
    <th class="titre"><strong>Titre</strong></th>             
    <th class="nombremessages"><strong>Réponses</strong></th>
    <th class="nombrevu"><strong>Vus</strong></th>
    <th class="auteur"><strong>Auteur</strong></th>                       
    <th class="derniermessage"><strong>Dernier message</strong></th>
</tr>
<?php
foreach ($Forum->GetTopic($forum) as $data) {
    echo'<tr><td><img src="./images/annonce.gif" alt="Annonce" /></td>
        <td id="titre"><strong>Annonce : </strong>
        <strong><a href="'.Url::base_url().'Forum/Topic/'.$data['topic_id'].'"                 
        title="Topic commencé à
        '.date('H\hi \l\e d M,y',$data['topic_time']).'">
        '.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
        <td class="nombremessages">'.$data['topic_post'].'</td>
        <td class="nombrevu">'.$data['topic_vu'].'</td>
        <td><a href="'.Url::base_url().'./profil.php?m='.$data['topic_createur'].'
        &amp;action=consulter">
        '.stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';
        $nombreDeMessagesParPage = 15;
        $nbr_post = $data['topic_post'] +1;
        $page = ceil($nbr_post / $nombreDeMessagesParPage);
        echo '<td class="derniermessage">Par
        <a href="'.Url::base_url().'./profil.php?m='.$data['post_createur'].'
        &amp;action=consulter">
        '.stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
        A <a href="'.Url::base_url().'Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'].'">'.date('H\hi \l\e d M y',$data['post_time']).'</a></td></tr>';
}
echo ('</table><table>
<tr>
<th><img src=".style/template/default/images/message.gif" alt="Message"/></th>
<th class="titre"><strong>Titre</strong></th>             
<th class="nombremessages"><strong>Réponses</strong></th>
<th class="nombrevu"><strong>Vus</strong></th>
<th class="auteur"><strong>Auteur</strong></th>                       
<th class="derniermessage"><strong>Dernier message  </strong></th>
</tr>');
foreach ($Forum->GetAllTopicPag($forum) as $data) {
    echo'<tr><td><img src="./images/message.gif" alt="Message" /></td>
    <td class="titre">
    <strong><a href="'.Url::base_url().'Forum/Topic/'.$data['topic_id'].'"                 
    title="Topic commencé à
    '.date('H\hi \l\e d M,y',$data['topic_time']).'">
    '.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
    <td class="nombremessages">'.$data['topic_post'].'</td>
    <td class="nombrevu">'.$data['topic_vu'].'</td>
    <td><a href="'.Url::base_url().'./profil.php?m='.$data['topic_createur'].'
    &amp;action=consulter">
    '.stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';
    $nombreDeMessagesParPage = 15;
    $nbr_post = $data['topic_post'] +1;
    $page = ceil($nbr_post / $nombreDeMessagesParPage);
    echo '<td class="derniermessage">Par
    <a href="'.Url::base_url().'/profil.php?m='.$data['post_createur'].'
    &amp;action=consulter">
    '.stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
    A <a href="'.Url::base_url().'Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'].'">'.date('H\hi \l\e d M y',$data['post_time']).'</a></td></tr>';
}
echo ('</table>');