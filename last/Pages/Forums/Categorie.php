<?php
foreach ($Forum->GetCat($lvl) as $data) 
{
    if( $categorie != $data['cat_id'] )
    {       
        $categorie = $data['cat_id'];
		echo'<h1 class="forum_head">'.stripslashes(htmlspecialchars($data['cat_nom'])).'</h1>';      
	}
	
    // contenue catégorie
    echo'
	<div class="forum_bg">
		<div class="icon_open"></div>
		<div class="forum_title"><a href="'.Url::base_url().'Forum/Forums/'.$data['forum_id'].'">'.utf8_decode(stripslashes(htmlspecialchars($data['forum_name']))).'</a></div>
		<div class="forum_desc">'.utf8_decode(nl2br(stripslashes(htmlspecialchars($data['forum_desc'])))).'<br /><a href="#">Archives</a></div>
		
		<div class="forum_topics">'.$data['forum_topic'].' Sujets <br />'.$data['forum_post'].' Réponses</div>';

    if (!empty($data['forum_post']))
    {
        $nombreDeMessagesParPage = 15;
        $nbr_post = $data['topic_post'] +1;
        $page = ceil($nbr_post / $nombreDeMessagesParPage);

        echo'<div class="forum_lastpost">
            <a href="'.Url::base_url().'Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'].'">'.$data['topic_titre'].'</a><br />
        '.date('d/m/Y H:i',$data['post_time']).'<br />
        <a href="#">'.ucfirst(strtolower($data['account_name'])).'</a>'
                . '</div>';
     }
     else
     {
         echo'<div class="forum_lastpost">Pas de message</div>';
     }
     $totaldesmessages += $data['forum_post'];
	 
	 echo'</div>';
}
?>
