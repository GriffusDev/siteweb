<?php
$Forum = new Forum($BddSite, $BddLogon, $BddChar);
$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:1;
$id=(isset($_SESSION['id']))?(int) $_SESSION['id']:0;
$pseudo=(isset($_SESSION['login']))?$_SESSION['login']:'';
define('VISITEUR',1);
define('MEMBRE NON VERIFIE',2);
define('MEMBRE INSCRIT',3);
define('MODERATOR',4);
define('GAMEMASTER',5);
define('COMMUNITY MANAGER',6);
define('CORE DEVELOPER',7);
define('DATABASE DEVELOPER',8);
define('ADMINISTRATOR',9);

$Lang->LoadFile('forum', 'french');

$totaldesmessages = 0;
$categorie = NULL;

?>

<?php
if(!isset($_GET['pages'])) {
    
    include(Url::GetAbsoluteLink('Pages/Forums/Categorie.php'));
    
}
else {
    switch($_GET['pages'])
    {
        case 1:
        {
            
            include(Url::GetAbsoluteLink('Pages/Forums/Forum.php'));
            break;
            
        }
        
        case 2: 
        {
            include(Url::GetAbsoluteLink('Pages/Forums/Topic.php'));
            break;
        }
        
        case 3:
        {
            $forum = $_GET['forum'];
            ?>
            <form class="addtopic" method="post" action="">
                <input type="text" value="" name="title" id="title" placeholder=""><br />
                <input type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('<gras>', '</gras>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('<italique>', '</italique>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="souligné" name="souligné" value="Souligné" onClick="javascript:bbcode('<souligne>', '</souligne>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('<lien>', '</lien>');return(false)" class="btn btn-sm ButtonFofo" />
                <input type="button" id="image" name="image" value="Images" onClick="javascript:bbcode('<images>', '</images>');return(false)" class="btn btn-sm ButtonFofo" />
                <br />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(' :D ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/rire.gif" title="lol" alt="lol" onClick="javascript:smilies(' :lol: ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/triste.png" title="triste" alt="triste" onClick="javascript:smilies(' :( ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/huh.png" title="choc" alt="choc" onClick="javascript:smilies(' :o ');return(false)" />
                <br />
                <textarea cols="80" rows="8" id="message" name="message"></textarea>
                <input type="text" value="<?php echo $forum; ?>" name="Idforum" id="Idforum" style="display: none;" placeholder="Idforum">
                <input type="text" value="<?php echo Url::base_url(); ?>" name="href" id="href" style="display: none;" placeholder="Lien">
                <input type="submit" name="submit" value="Envoyer" />
            </form>
            <?php
            break;
        }
        
    }
}

?>