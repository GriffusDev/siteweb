<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE); 
ini_set("display_errors", 1);
ini_set('error_reporting', E_ALL);
header('Content-Type: text/html; charset=UTF-8');

//initialisation des sessions
session_start();

//include des fichiers class
include('scripts/Class/Autoload.class.php');

// Init website var
$Lang = new Lang();

//include du fichier de config
include('scripts/Php/Config.php');

//Cache init
$Cache = new Cache($config->SiteName);

$BddSite = new Mysql(sqlconnect('site'), $Cache);
$BddLogon = new Mysql(sqlconnect('logon'), $Cache);
$BddChar = new Mysql(sqlconnect('char'), $Cache);
$News = new News($BddSite);

if(Css::GetSupport() == 0)
{
    if(isset($_SESSION['id']))
    {
        $account = new Account($BddSite, $BddLogon, $BddChar);
        $account->GetAllAcces();
    }
    $Template = new Template('pc');
}
else
{
    $Template = new Template('mobile');
    echo'site mobile';
}
?>
