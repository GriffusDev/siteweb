<?php
//////////////////////////////////////////
// Navigation
//////////////////////////////////////////
define('NAV_JOINUS', 'Nous rejoindre');
define('NAV_REGISTER', 'Inscription');
define('NAV_BUGTRACKER', 'Bugtracker');
define('NAV_FORUMS', 'Forums');
define('NAV_ARMORY', 'Armurerie');
define('NAV_VOTE', 'Voter');
define('NAV_AUCTION', 'Hotel des ventes');
define('NAV_SUPPORT', 'Assistance');

////////////////////////////////////////////
// Website
////////////////////////////////////////////
define('SITE_TITLE', 'Shindorei | Renaissance');

////////////////////////////////////////////
// Login section
////////////////////////////////////////////
define('LOGIN_TITLE', 'Identification');
define('LOGIN_USERNAME', 'Nom de compte');
define('LOGIN_PASSWORD', '*******');
define('LOGIN_SUBMIT_BUTTON', 'Connexion');

////////////////////////////////////////////
// Realm section
////////////////////////////////////////////
define('REALM_TITLE', 'Statut serveur');