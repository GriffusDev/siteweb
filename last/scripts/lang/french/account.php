<?php
////////////////////////////////////////////
// My Account Section
////////////////////////////////////////////
define('MY_ACCOUNT_TITLE',                  'Mon compte');
define('MY_ACCOUNT_ACCOUNT',                'Nom de compte');
define('MY_ACCOUNT_EMAIL',                  'Email');
define('MY_ACCOUNT_CHARACTER',              'Personnage principal');
define('MY_ACCOUNT_IP',                     'Ip');
define('MY_ACCOUNT_POINTVOTE_NUMBER',       'Points vote');
define('MY_ACCOUNT_POINTSHINS_NUMBER',      'Points Shins');
define('MY_ACCOUNT_WARNING',                'Avertissement(s)');
define('MY_ACCOUNT_PURCHASESTORE',          'Achat(s) éffectué(s)');
define('MY_ACCOUNT_SUPPORTPENDING',         'Assistance en attente');
define('MY_ACCOUNT_AUCTIONPENDING',         'Enchères en cours');
define('MY_ACCOUNT_PURCHASEPENDING',        'Achat(s) en attente');
define('MY_ACCOUNT_LOGINHISTORY',           'Historiques de connexion');