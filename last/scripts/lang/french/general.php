<?php
////////////////////////////////////////////
// Website
////////////////////////////////////////////
define('SITE_TITLE', 				'Shindorei | Renaissance');

////////////////////////////////////////////
// Template
////////////////////////////////////////////
define('TEMPLATE_NOFILE_FOR_PAGE',              'Pas de page definie dans la configuration du template pour [URI]');
define('TEMPLATE_PAGE_INTROUVABLE',              'Page introuvable [URI]');

////////////////////////////////////////////
// Navigation
////////////////////////////////////////////
define('NAV_1_LOGOUT',				'Nous rejoindre');
define('NAV_2_LOGOUT',				'Inscription');
define('NAV_3_LOGOUT',				'Bugtracker');
define('NAV_4_LOGOUT',				'Forums');
define('NAV_5_LOGOUT',				'Armurerie');
define('NAV_6_LOGOUT',				'Voter');
define('NAV_7_LOGOUT',				'Hotel des ventes');
define('NAV_8_LOGOUT',				'Assistance');
define('NAV_1_LOGIN', 				'Nous rejoindre');
define('NAV_2_LOGIN', 				'Inscription');
define('NAV_3_LOGIN', 				'Bugtracker');
define('NAV_4_LOGIN', 				'Forums');
define('NAV_5_LOGIN', 				'Armurerie');
define('NAV_6_LOGIN', 				'Voter');
define('NAV_7_LOGIN', 				'Hotel des ventes');
define('NAV_8_LOGIN', 				'Assistance');

////////////////////////////////////////////
// Login section
////////////////////////////////////////////
define('LOGIN_TITLE', 				'Identification');
define('LOGIN_USERNAME', 			'Nom de compte');
define('LOGIN_PASSWORD', 			'*******');
define('LOGIN_SUBMIT_BUTTON',                   'Connexion');

////////////////////////////////////////////
// Right section
////////////////////////////////////////////
define('RIGHT_AUTH_MYACCOUNT', 				'Mon compte');
define('RIGHT_AUTH_LOGOUT', 				'Déconnexion');
define('RIGHT_AUTH_STORE', 				'Boutique');

////////////////////////////////////////////
// Realm section
////////////////////////////////////////////
define('REALM_TITLE', 				'Statut serveur');
define('REALM_LOGON', 				'Identification');
define('REALM_LOGON_VERSION',                   '5.4.2');
define('REALM_REALM_TYPE', 			'Blizzlike');
define('REALM_TS', 				'Teamspeak');
define('REALM_TS_DESC', 			'Discussion');

////////////////////////////////////////////
// News Info section
////////////////////////////////////////////
define('NEWSINFO_TITLE', 			'FIL d\'ACTUALITE');

////////////////////////////////////////////
// Shop section
////////////////////////////////////////////
define('SHOP_TITLE', 				'Boutique');

////////////////////////////////////////////
// Vote section
////////////////////////////////////////////
define('VOTE_LOGOFF', 				'Attention vous n\'étes pas connecté, donc vos votes ne serrons pas pris en compte sur votre compte !!!');
define('VOTE_TITLE', 				'Vote');

////////////////////////////////////////////
// Register section
////////////////////////////////////////////
define('REGISTER_ACCOUNT', 			'Nom de compte');
define('REGISTER_PASSWORD',                     'Mot de passe');
define('REGISTER_PASSWORD_REPEAT',              'Confirmer votre mot de passe');
define('REGISTER_MAIL', 			'Email');
define('REGISTER_MAIL_REPEAT',                  'Confirmer votre email');
define('REGISTER_SECURITY_CODE',                'Code de s&eacute;curit&eacute;');
define('REGISTER_SUBMIT_VALUE',                 'Inscription');

////////////////////////////////////////////
// Mysql section
////////////////////////////////////////////
define('MYSQL_CONNECT_ERROR',                   'Erreur de connection à la base de données les identifiants saisie sont incorrect');
define('MYSQL_NO_SELECT_ALL',                   'Pour des raisons de performance la propriété \'*\' ne peut étre utilisé dans une requéte sql SELECT');
define('MYSQL_EMPTY_FIELD',                     'La requete n\'a pas de champs renseigné');
define('MYSQL_NO_TABLE_NAME',                   'Aucun nom de table renseigné');
define('MYSQL_NO_TABLE_EXIST',                  'Une des tables renseigné n\'existe pas');
define('MYSQL_NO_FIELD_TABLE_NAME',             'Lorsque plusieurs table sont présente dans la requéte vous devez indiquer la table de référence devant le champ (ex : matable.monchamp)');
define('MYSQL_NO_FIELD_FOR_TABLE',              'Le champ spécifier dansla table n\'existe pas');
define('MYSQL_DELETE_MORE_TABLE',               'La requetes delete fait référence à plusieur tables');
define('MYSQL_DELETE_NO_CONDITION',             'Aucune condition d�finis pour la requete delete');
define('MYSQL_DELETE_TABLE_EXIST',              'La table de la requete DELETE n\'existe pas');
define('MYSQL_QUERY_NO_CONDITION',              'La requéte ne posséde pas de condition');
define('MYSQL_QUERY_NO_CONNECTOR_EXEC',         'La requéte ne posséde pas de connecteur, la variable execute doit donc étre vide');
define('MYSQL_QUERY_NO_CONNECTOR_CONDITION',    'Aucune condition n\'a été déclaré dans le execture pour un connecteur');
define('MYSQL_QUERY_NO_CONDITION_COUNT',        'Le nombre de condition et de connecteur ne sont pas identique');
define('MYSQL_UPDATE_MORE_TABLE',               'La requetes update fait référence à plusieur tables');
define('MYSQL_INSERT_MORE_TABLE',               'La requetes insert into fait référence à plusieur tables');

////////////////////////////////////////////
// Error section
////////////////////////////////////////////
define('ERROR_START',                           'Une erreur est survenue sur la page : ');
define('ERROR_FUNCTION',                        'Fonction : ');
define('ERROR_CLASS',                           'Classe : ');
define('ERROR_LINE',                            'Ligne : ');
define('ERROR_CODE',                            'Code erreur : ');
define('ERROR_MUST_LOGGED',                     'Vous devez etre connecte pour voir cette page');
define('ERROR_AUTOLOAD_NO_ID',                  'Aucune variable de type de connection n\'est déclaré dans le fichier Autoload');
define('ERROR_AUTOLOAD_NO_ID_EXIST',            'la variable de type de connection déclaré dans le fichier Autoload est inccorect');

////////////////////////////////////////////
// Général section
////////////////////////////////////////////
define('GETMORE',                'Obtenir');
define('VIEW',                   'Voir');