<?php
define('FORUM_ERR_IS_CO',			'Vous ne pouvez pas accéder à cette page.');
define('FORUM_ERR_IS_NOT_CO',                   'Vous ne pouvez pas accéder à cette page.');
define('FORUM_ERR_TOPIC_LOCK',                  'Ce sujet est vérouillé.');
define('FORUM_NO_TOPIC',			'Ce sujet est vérouillé.');
define('FORUM_BY',				'Par ');
define('FORUM_AT',				'Le ');
define('FORUM_ANNOUNCE',			'Annonce ');
define('FORUM_TOPIC_DATE',			'Topic commencé à ');