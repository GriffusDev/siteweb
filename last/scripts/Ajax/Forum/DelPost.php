<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if (isset($_POST['post'])) 
{
    if(isset($_SESSION['id']))
    {
        
        $post = $_POST['post'];
        $query = $BddSite->Query2('SELECT post_createur, post_texte, forum_id, topic_id, auth_modo
        FROM forum_post
        LEFT JOIN forum_forum ON forum_post.post_forum_id = forum_forum.forum_id
        WHERE post_id=:post', array('post' => $post));
        $data = $query->fetch();
        $topic = $data['topic_id'];
        $forum = $data['forum_id'];
        $poster = $data['post_createur'];
        if ($data['post_createur'] == $_SESSION['id'] OR $_SESSION['lvl'] >= 3) {
            
            $query2 = $BddSite->Query2('SELECT topic_first_post, topic_last_post FROM forum_topic WHERE topic_id = :topic', array('topic' => $topic));
            $data_post=$query2->fetch();
            
            if ($data_post['topic_first_post']==$post)
            {
                echo 0;
            }
            elseif ($data_post['topic_last_post']==$post)
            {
                $BddSite->Query2('DELETE FROM forum_post WHERE post_id = :post', array('post' => $post));
                $query3 = $BddSite->Query2('SELECT post_id FROM forum_post WHERE topic_id = :topic ORDER BY post_id DESC LIMIT 0,1', array('topic' => $topic));
                $data3=$query3->fetch();
                $last_post_topic=$data3['post_id'];
                $query4 = $BddSite->Query2('SELECT post_id FROM forum_post WHERE post_forum_id = :forum ORDER BY post_id DESC LIMIT 0,1', array('forum' => $forum));
                $data4=$query4->fetch();
                $last_post_forum=$data4['post_id'];
                $BddSite->Query2('UPDATE forum_topic SET topic_last_post = :last WHERE topic_last_post = :post', array('last' => $last_post_topic,'post' => $post));
                $BddSite->Query2('UPDATE forum_forum SET forum_post = forum_post - 1, forum_last_post_id = :last WHERE forum_id = :forum', array('last' => $last_post_forum,'forum' => $forum));
                $BddSite->Query2('UPDATE forum_topic SET topic_post = topic_post - 1 WHERE topic_id = :topic', array('topic' => $topic));
                $BddSite->Query2('UPDATE membres SET membre_post = membre_post - 1 WHERE id = :id', array('id' => $poster));
                
                echo 1;
                
            }
            else {
                
                $BddSite->Query2('DELETE FROM forum_post WHERE post_id = :post', array('post' => $post));
                $BddSite->Query2('UPDATE forum_forum SET forum_post = forum_post - 1  WHERE forum_id = :forum', array('forum' => $forum));
                $BddSite->Query2('UPDATE forum_topic SET topic_post = topic_post - 1 WHERE topic_id = :topic', array('topic' => $topic));
                $BddSite->Query2('UPDATE membres SET  membre_post = membre_post - 1 WHERE id = :id', array('id' => $poster));
                echo 1;
            }
        }
        else
            echo 0;
    }
    else
        echo 0;
}
else
    echo 0;