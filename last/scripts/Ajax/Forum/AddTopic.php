<?php
error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if (isset($_POST['title'])) 
{
    if(isset($_SESSION['id']))
    {
	$titre = $_POST['title'];
	$texte = $_POST['message'];
	$by = $_SESSION['id'];
        $forum = $_POST['forum'];
        $temps = time();
	$BddSite->Query2('INSERT INTO forum_topic (forum_id, topic_titre, topic_createur, topic_vu, topic_time, topic_genre)
        VALUES(:forum, :titre, :id, 1, :temps, :genre)', array('forum' => $forum, 'titre' => $titre, 'id' => $by, 'temps' => time(), 'genre' => "Normal"));
        $nouveaupost = $BddSite->lastInsertId();
        $BddSite->Query2('INSERT INTO forum_post (post_createur, post_texte, post_time, topic_id, post_forum_id)
        VALUES (:id, :mess, :temps, :nouveautopic, :forum)', array('id' => $by, 'mess' => $texte, 'temps' => time(), 'nouveautopic' => $nouveaupost, 'forum' => $forum));
        $nouveaupost2 = $BddSite->lastInsertId();
        $BddSite->Query2('UPDATE forum_topic SET topic_last_post = :nouveaupost, topic_first_post = :nouveaupost WHERE topic_id = :nouveautopic', array('nouveaupost' => $nouveaupost2, 'nouveautopic' => $nouveaupost));
        $BddSite->Query2('UPDATE forum_forum SET forum_post = forum_post + 1 ,forum_topic = forum_topic + 1, 
        forum_last_post_id = :nouveaupost
        WHERE forum_id = :forum', array('nouveaupost' => $nouveaupost, 'forum' => $forum));
        $BddSite->Query2('UPDATE membres SET membre_post = membre_post + 1 WHERE id = :id', array('id' => $by));
	echo 1;
    }
    else
        echo 0;
}
else
    echo 0;