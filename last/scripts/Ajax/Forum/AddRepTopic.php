<?php
error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if (isset($_POST['id'])) 
{
    if(isset($_SESSION['id']))
    {
	$topic = $_POST['id'];
	$texte = $_POST['message'];
	$by = $_SESSION['id'];
        $id = $_SESSION['id'];
        $forum = $_POST['forum'];
	$BddSite->Query2('INSERT INTO forum_post(post_createur, post_texte, post_time, topic_id, post_forum_id) VALUES(:id,:mess,:temps,:topic,:forum)', array('id' => $by, 'mess' => $texte, 'temps' => time(), 'topic' => $topic, 'forum' => $forum));
        $nouveaupost = $BddSite->lastInsertId();
        $BddSite->Query2('UPDATE forum_topic SET topic_post = topic_post + 1, topic_last_post = :nouveaupost WHERE topic_id =:topic', array('nouveaupost' => $nouveaupost, 'topic' => $topic));
        $BddSite->Query2('UPDATE forum_forum SET forum_post = forum_post + 1 , forum_last_post_id = :nouveaupost WHERE forum_id = :forum', array('nouveaupost' => $nouveaupost, 'forum' => $forum));
        $BddSite->Query2('UPDATE membres SET membre_post = membre_post + 1 WHERE id = :id', array('id' => $by));
        $BddSite->Query2('UPDATE forum_topic_view SET tv_post_id = :post, tv_poste = :poste WHERE tv_id = :id AND tv_topic_id = :topic', array('post' => $nouveaupost, 'poste' => '1', 'id' => $id, 'topic' => $topic));
	echo 1;
    }
    else
        echo 0;
}
else
    echo 0;