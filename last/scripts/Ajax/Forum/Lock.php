<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if (isset($_POST['topic'])) 
{
    if(isset($_SESSION['id']))
    {
        if($_SESSION['lvl'] >= 3)
        {
            if ($_POST['type'] == "1") { //On lock
                $topic = $_POST['topic'];
                $BddSite->Query2('UPDATE forum_topic SET topic_locked = :lock WHERE topic_id = :topic', array('lock' => 1, 'topic' => $topic));
                echo 1;
            }
            else { //On delock
                $topic = $_POST['topic'];
                $BddSite->Query2('UPDATE forum_topic SET topic_locked = :lock WHERE topic_id = :topic', array('lock' => 0, 'topic' => $topic));
                echo 1;
            }
        }
        else
            echo 0;
    }
    else
        echo 0;
}
else
    echo 0;