<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if (isset($_POST['topic'])) 
{
    if(isset($_SESSION['id']))
    {
        
        $topic = $_POST['topic'];
        $query = $BddSite->Query2('SELECT forum_topic.forum_id, auth_modo
        FROM forum_topic
        LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id
        WHERE topic_id=:topic', array('topic' => $topic));
        $data = $query->fetch();
        $forum = $data['forum_id'];
        $query2 = $BddSite->Query2('SELECT topic_post FROM forum_topic WHERE topic_id = :topic', array('topic' => $topic));
        $data2 = $query2->fetch();
        $nombrepost = $data2['topic_post'] + 1;
        $query4 = $BddSite->Query2('SELECT post_id FROM forum_post WHERE post_forum_id = :forum ORDER BY post_id DESC LIMIT 0,1', array('forum' => $forum));
        $data4 = $query4->fetch();

        $BddSite->Query2('DELETE FROM forum_topic WHERE topic_id = :topic', array('topic' => $topic));
        
        $query3 = $BddSite->Query2('SELECT post_createur, COUNT(*) AS nombre_mess FROM forum_post WHERE topic_id = :topic GROUP BY post_createur', array('topic' => $topic));
        
        while($data3 = $query3->fetch())
        {
            $BddSite->Query2('UPDATE membres SET membre_post = membre_post - :mess WHERE id = :id', array('mess' => $data3['nombre_mess'], 'id' => $data3['post_createur']));
        }
        
        $BddSite->Query2('DELETE FROM forum_post WHERE topic_id = :topic', array('topic' => $topic));
        
        
        $BddSite->Query2('UPDATE forum_forum SET forum_topic = forum_topic - 1, forum_post = forum_post - :nbr, forum_last_post_id = :id
        WHERE forum_id = :forum', array('nbr' => $nombrepost, 'id' => $data4['post_id'], 'forum' => $forum));
        
        echo 1;
        
    }
    else
        echo 0;
}
else
    echo 0;