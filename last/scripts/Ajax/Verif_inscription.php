<?php
include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if(!isset($_POST['login']) || !isset($_POST['mdp']) || !isset($_POST['mdpconf']) || !isset($_POST['email']) || !isset($_POST['emailr']) || !isset($_POST['captcha']))
die('Form is not full');

$login = $_POST['login'];
$mdp = $_POST['mdp'];
$mdpconf = $_POST['mdpconf'];
$email = $_POST['email'];
$emailr = $_POST['emailr'];
$captcha = $_POST['captcha'];
$error = 0;
$errors = array();

$reponse = $BddLogon->Query("SELECT * FROM account WHERE username=:login", array('login' => $login));
$result = count($reponse);

$reponse1 = $BddLogon->Query("SELECT * FROM account WHERE email=:email", array('email' => $email));
$result1 = count($reponse1);

if(strlen($login) >= 4)
{
    if($result < 1)
    {
        if(strlen($mdp) >= 6)
        {
            if($mdp != $login)
            {
                if($mdpconf == $mdp)
                {
                    if($emailr == $email)
                    {
                        if(filter_var($email, FILTER_VALIDATE_EMAIL))
                        {
                            if($result1 < 1)
                            {
                                if(strlen($captcha) == 7)
                                {
                                    if($captcha == $_SESSION['captcha'])
                                    {
                                        if($error == 0)
                                        {
                                                $mdpdb = strtoupper(sha1(strtoupper($_POST['login']).':'.strtoupper($mdp)));

                                                $key_length = 65;
                                                $key = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $key_length);		
                                                $reponse2 = $BddSite->Query2('INSERT INTO membres_register(username,password,email,key_register,ip) VALUES(:username, :sha_pass_hash, :email, :key_register, :ip)',
												array(
                                                    'username' => strtoupper($login), 
                                                    'sha_pass_hash' => $mdpdb, 
                                                    'email' => $email, 
                                                    'key_register' => $key, 
                                                    'ip' => $_SERVER['REMOTE_ADDR']
                                                ));

                                                include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/Register_mail.php');

                                                echo 11;
                                        }
                                    }
                                    else
                                    {
                                            echo 10;
                                            $error = 1;
                                    }
                                }
                                else
                                {
                                        echo 9;
                                        $error = 1;
                                }
                            }
                            else
                            {
                                    echo 8;
                                    $error = 1;
                            }
                        }
                        else
                        {
                                echo 7;
                                $error = 1;
                        }
                    }
                    else
                    {
                            echo 6;
                            $error = 1;
                    }
                }
                else
                {
					echo 5;
					$error = 1;
                }
            }
            else
            {
                echo 4;
                $error = 1;
            }
        }
        else
        {
            echo 3;
            $error = 1;
        }
    }
    else
    {
        echo 2;
        $error = 1;
    }
}
else
{
	echo 1;
	$error = 1;
}