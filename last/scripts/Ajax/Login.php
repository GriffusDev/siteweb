<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1);

include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/AjaxHeader.php');

if(isset($_POST['login']) && isset($_POST['mdp']))
{
	$login = htmlentities($_POST['login']);
	$mdp = htmlentities($_POST['mdp']);
	$mdpdb = strtoupper(sha1(strtoupper(''.$login.'').':'.strtoupper(''.$mdp.'')));
	$row = $BddLogon->Query("SELECT sha_pass_hash, id FROM account WHERE username = :login", array('login' => strtoupper($login)));
	
	if(empty($row))
		echo 0;
	elseif($mdpdb == $row[0]['sha_pass_hash'])
	{
		$account = new Account($BddSite);
		$reponse2 = $BddSite->Query("SELECT membre_rank FROM membres WHERE account_name = :login", array('login' => strtoupper($login)));

		Session::CreateSession('login', ucfirst($login));
		Session::CreateSession('id', $row[0]['id']);
		Session::CreateSession('lvl', $reponse2[0]['membre_rank']);
		$account->UpdateToken($login);
		echo 1;
	}
	else
		echo 0;
}	
else
	echo 0;
Session::CreateSession('login', 'Deathart');
		Session::CreateSession('id', '15');
		Session::CreateSession('lvl', '15');
?>