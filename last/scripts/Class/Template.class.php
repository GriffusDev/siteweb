<?php
class Template
{
	private $plateform;
	private $configt;

	/////////////////////////////////////////////////////////////////////////////////////
	// Function de recuperation des variable (utilisateur)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $plateform = website platform (default : pc) - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	public function __construct($plateform = 'pc')
	{
            global $config;

            if($plateform == 'pc')
                $this->plateform = 'html_pc';
            else
                $this->plateform = 'html_mobile';

            include(Url::GetAbsoluteLink('Templates/'.$config->Template .'/Config.php'));
            $this->configt = $configt->page;

            $cut = substr($_SERVER['REQUEST_URI'], 1);
            $var = array(0,1,2,3,4,5,6,7,8,9);
            $cut = str_replace($var, '$1', $cut);

            if($cut == false || $cut == 'Home')
            {
                $page = file_get_contents(Url::GetAbsoluteLink('Templates/'.$config->Template .'/'.$this->plateform.'/'.$this->configt['']));
                $page = $this->ParsePageDeux($page, 1);

                $js = file_get_contents(Url::GetAbsoluteLink('scripts/js/All.php'));

                $structure = file_get_contents(Url::GetAbsoluteLink('Templates/'.$config->Template .'/'.$this->plateform.'/default.html'));
                $structure = $this->ParsePage($structure);
                $structure = str_replace("[WEBSITE_PAGE_CONTENT]", $page, $structure);
                $structure = str_replace("[SCRIPTS_JS]", $js, $structure);

                echo $structure;
            }
            else
            {
                if(array_key_exists($cut, $this->configt))
                {
                    $page = file_get_contents(Url::GetAbsoluteLink('Templates/'.$config->Template .'/'.$this->plateform.'/'.$this->configt[$cut]));

                    if (!$page) {
                        die(str_replace("[URI]", $cut, TEMPLATE_PAGE_INTROUVABLE));
                    }
                    else {
                        $page = $this->ParsePageDeux($page);
                        
                        $js = file_get_contents(Url::GetAbsoluteLink('scripts/js/All.php'));

                        $structure = file_get_contents(Url::GetAbsoluteLink('Templates/'.$config->Template .'/'.$this->plateform.'/default.html'));
                        $structure = $this->ParsePage($structure);
                        $structure = str_replace("[WEBSITE_PAGE_CONTENT]", $page, $structure);
                        $structure = str_replace("[SCRIPTS_JS]", $js, $structure);

                        echo $structure;
                    }
                }
                else
                    die(str_replace("[URI]", $cut, TEMPLATE_NOFILE_FOR_PAGE));
            }
	}
        
        /////////////////////////////////////////////////////////////////////////////////////
	// Function ParsePagedeux (Structure page)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParsePageDeux($text, $id=0)
	{
            global $config;
            
            if($id == 1) {
                $text = $this->ParseNew($text);
            }
            else {
                $text = $this->ParseForum($text);
                $text = $this->ParseAccount($text);
                $text = $this->ParseCom($text);
            }
            
            $text = $this->ParseAuthLogged($text);
            $text = $this->ParseAuthLogout($text);
            $text = $this->ParseText($text);
            $text = str_replace("[WEBSITE_TITLE]", $config->SiteName, $text);
            $text = str_replace("[WEBSITE_LANG]", $config->Lang, $text);
            $text = str_replace("[WEBSITE_URL]", Url::base_url(), $text);
            $text = str_replace("[TEMPLATE_URL]", '/Templates/'.$config->Template .'/'.$this->plateform.'', $text);
            $text = str_replace("[FB_MODULE]", $this->FbModule(), $text);
            
            return $text;
            
        }
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParsePage (Structure default)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParsePage($text)
	{
            
            global $config;
            
            $text = str_replace("[WEBSITE_TITLE]", $config->SiteName, $text);
            $text = str_replace("[WEBSITE_LANG]", $config->Lang, $text);
            $text = str_replace("[WEBSITE_URL]", Url::base_url(), $text);
            $text = str_replace("[TEMPLATE_URL]", '/Templates/'.$config->Template .'/'.$this->plateform.'', $text);
            $text = str_replace("[FB_MODULE]", $this->FbModule(), $text);

            if(Session::VerifSession('id')){
                
                $text = str_replace("[SESSION_LOGIN]", $_SESSION['login'], $text);
                $text = str_replace("[SESSION_ID]", $_SESSION['id'], $text); 
                $text = str_replace("[SESSION_LEVEL]", $_SESSION['lvl'], $text);
                
            }
            else{
                $text = str_replace("[SESSION_LOGIN]", '', $text);
            }
            
            $text = $this->ParseAuthLogged($text);
            $text = $this->ParseAuthLogout($text);
            $text = $this->ParseRealm($text);
            $text = $this->ParseText($text);
            $text = $this->ParseNav($text);
                
            return utf8_encode($text);
            
	}
		
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseForum
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseForum($text)
	{
		global $BddLogon, $BddSite, $BddChar;
	
		$forum = new Forum($BddSite, $BddLogon, $BddChar);
		
		//Homepage
		if(stripos($text, "<!-- FORUM_CATEGORIE_START -->") !== false)
		{
			return $forum->HomeCategorieParse($text);
		}
		//Forum Page
		else if(stripos($text, "<!-- FORUM_FORUMS_START -->") !== false)
		{
			return $forum->ForumsParse($text);
		}
		//Topic Page
		else if(stripos($text, "<!-- FORUM_TOPIC_START -->") !== false)
		{
			return $forum->TopicParse($text);
		}
		else
			return $text;
	}	
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseNew
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseNew($text)
	{
            global $News;
            global $config;
            $first = true;
            $new = '';

            if(stripos($text, "<!-- NEWS_START_STRUCTURE -->") !== false)
            {
                $verif = explode("<!-- NEWS_START_STRUCTURE -->", $text, 2);
                $verif2 = explode("<!-- NEWS_END_STRUCTURE -->", $verif[1], 2);
                $structure = $verif2[0];

                foreach ($News->get_liste() as $newsb)
                {
                    $strucfinal = str_replace("[NEWS_TITRE]", $newsb['titre'], $structure);
                    $strucfinal = str_replace("[NEWS_LINK]", Url::base_url().'News/'.$newsb['idnews'], $strucfinal);
                    $strucfinal = str_replace("[NEWS_COM_COUNT]", $News->get_count_com($newsb['idnews']), $strucfinal);
                    $strucfinal = str_replace("[NEWS_PICTURE]", '<img src="'.Url::base_url().'Templates/'.$config->Template .'/'.$this->plateform.'/Images/News/'.$newsb['Pictures_News'].'" alt="" />', $strucfinal);
                    $strucfinal = str_replace("[NEWS_DATA]", Parsing::CoupeChaine(Parsing::BBCode($newsb['news']), Url::base_url().'News/'.$newsb['idnews']), $strucfinal);

                    $new .= $strucfinal;	
                }	

                return $new;
            }
            else
                return $text;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseText
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseText($text)
	{
            if(stripos($text, "[TEXT=") !== false)
            {
                $count = substr_count($text, '[TEXT=');
                $final = $text;

                for($i = 0; $i < $count; $i++)
                {
                    $verif = explode("[TEXT=", $final, 2);
                    $verif2 = explode("]", $verif[1], 2);
                    $replace = str_replace($verif2[0], constant($verif2[0]), $verif2[0]);
                    $final = $verif[0].$replace.$verif2[1];	
                }
                return $final;
            }
            else {
                return $text;
            }
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseRealm
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseRealm($text)
	{
		global $config;
	
		if(stripos($text, '[LOGON_FIRST_STATUT]') !== false)
		{
			$royaume = new monitoring($config->LogonIp, $config->LogonPort);
			$text = str_replace("[LOGON_FIRST_STATUT]", $royaume->PingHostRealm(), $text);
		}
		
		if(stripos($text, '[REALM_FIRST_STATUT]') !== false)
		{
			$royaume = new monitoring($config->RealmIp, $config->RealmPort);
			$text = str_replace("[REALM_FIRST_STATUT]", $royaume->PingHostRealm(), $text);
		}
		
		if(stripos($text, '[TEAMSPEAK_STATUT]') !== false)
		{
			$royaume = new monitoring($config->RealmIp, $config->RealmPort);
			$text = str_replace("[TEAMSPEAK_STATUT]", $royaume->PingHostRealm(), $text);
		}
		
		return $text;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseAuthLog (if you are logged or not)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseAuthLogged($text)
	{
		if(stripos($text, "<!-- AUTH_ON -->") !== false)
		{
			if(Session::VerifSession('id'))
			{
				$count = substr_count($text, '<!-- AUTH_ON -->');
				$final = $text;
				
				for($i = 0; $i < $count; $i++)
				{
						$verif = explode("<!-- AUTH_ON -->", $final, 2);
						$verif2 = explode("<!-- AUTH_ON_END -->", $verif[1], 2);
						$final = $verif[0].$verif2[0].$verif2[1];	
				}
				return $final;
			}
			else
			{
				$count = substr_count($text, '<!-- AUTH_ON -->');
				$final = $text;
				
				for($i = 0; $i < $count; $i++)
				{
						$verif = explode("<!-- AUTH_ON -->", $final, 2);
						$verif2 = explode("<!-- AUTH_ON_END -->", $verif[1], 2);
						$final = $verif[0].$verif2[1];
				}
				return $final;
			}
		}
		else
			return $text;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseAuthLogout (if you are not logged)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseAuthLogout($text)
	{
		if(stripos($text, "<!-- AUTH_OFF -->") !== false)
		{
			if(!Session::VerifSession('id'))
			{
				$count = substr_count($text, '<!-- AUTH_OFF -->');
				$final = $text;
				
				for($i = 0; $i < $count; $i++)
				{
						$verif = explode("<!-- AUTH_OFF -->", $final, 2);
						$verif2 = explode("<!-- AUTH_OFF_END -->", $verif[1], 2);
						$final = $verif[0].$verif2[0].$verif2[1];	
				}
				return $final;
			}
			else
			{
				$count = substr_count($text, '<!-- AUTH_OFF -->');
				$final = $text;
				
				for($i = 0; $i < $count; $i++)
				{
						$verif = explode("<!-- AUTH_OFF -->", $final, 2);
						$verif2 = explode("<!-- AUTH_OFF_END -->", $verif[1], 2);
						$final = $verif[0].$verif2[1];
				}
				return $final;
			}
		}
		else
			return $text;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function FbModule
	// -------------------------------------
	// Variable | valeur
	// =========================================
	/////////////////////////////////////////////////////////////////////////////////////
	private function FbModule()
	{
		global $config;
	
		return '<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1&appId='.$config->FbAppId.'";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, \'script\', \'facebook-jssdk\'));</script>';
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function ParseNav (Navigation Head bar Parse)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $text = page text - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	private function ParseNav($text)
	{
            global $config;
            $text = str_replace("[MENUTEXT1]", $config->Menu[1]['item'], $text);
            $text = str_replace("[MENULINK1]", '/'.$config->Menu[1]['url'], $text);
            $text = str_replace("[MENUTEXT2]", $config->Menu[2]['item'], $text);
            $text = str_replace("[MENULINK2]", '/'.$config->Menu[2]['url'], $text);
            $text = str_replace("[MENUTEXT3]", $config->Menu[3]['item'], $text);
            $text = str_replace("[MENULINK3]", '/'.$config->Menu[3]['url'], $text);
            $text = str_replace("[MENUTEXT4]", $config->Menu[4]['item'], $text);
            $text = str_replace("[MENULINK4]", '/'.$config->Menu[4]['url'], $text);
            $text = str_replace("[MENUTEXT5]", $config->Menu[5]['item'], $text);
            $text = str_replace("[MENULINK5]", '/'.$config->Menu[5]['url'], $text);
            $text = str_replace("[MENUTEXT6]", $config->Menu[6]['item'], $text);
            $text = str_replace("[MENULINK6]", '/'.$config->Menu[6]['url'], $text);
            $text = str_replace("[MENUTEXT7]", $config->Menu[7]['item'], $text);
            $text = str_replace("[MENULINK7]", '/'.$config->Menu[7]['url'], $text);
            $text = str_replace("[MENUTEXT8]", $config->Menu[8]['item'], $text);
            $text = str_replace("[MENULINK8]", '/'.$config->Menu[8]['url'], $text);
            return $text;
	}
        
    /////////////////////////////////////////////////////////////////////////////////////
    // Function ParseAccount
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $text = page text - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    private function ParseAccount($text)
    {
        
        if(Session::VerifSession('id')) {
            
            global $config, $BddLogon, $BddSite, $BddChar;
            
            $account = new Account($BddSite, $BddLogon, $BddChar);
            
            Lang::LoadFileStatic('account', 'french');
            
            $text = str_replace("[MA_ACCOUNT]", $account->RecupPseudo($_SESSION['id']), $text);
            $text = str_replace("[MA_EMAIL]", $account->RecupMail($_SESSION['id']), $text);
            $text = str_replace("[MA_CHARACTER]", $account->RecupCharacter($_SESSION['id']), $text);
            $text = str_replace("[MA_IP]", $account->RecupIP($_SESSION['id']), $text);
            $text = str_replace("[MA_POINTVOTE_NUMBER]", $account->RecupNBVote($_SESSION['id']), $text);
            $text = str_replace("[MA_POINTSHINS_NUMBER]", $account->RecupNBPoint($_SESSION['id']), $text);
            $text = str_replace("[MA_WARNING]", $account->RecupWarning($_SESSION['id']), $text);
            $text = str_replace("[MA_PURCHASESTORE]", $account->RecupListPurchaseStore($_SESSION['id']), $text);
            $text = str_replace("[MA_SUPPORTPENDING]", '0', $text);
            $text = str_replace("[MA_AUCTIONPENDING]", $account->RecupListAuctionPending($_SESSION['id']), $text);
            $text = str_replace("[MA_PURCHASEPENDING]", $account->RecupListPurchasePending($_SESSION['id']), $text);
            
            return $text;
            
        }

    }
    
    private function ParseCom($text)
    {

        global $News, $config, $BddLogon, $BddSite, $BddChar;
        
        $Account = new Account($BddSite, $BddLogon, $BddChar);
        
        Lang::LoadFileStatic('com', 'french');
        
        $data = $News->get_news(Url::LastSegment());
        
        $text = str_replace("[NEWS_TITRE]", $data['titre'], $text);
        $text = str_replace("[NEWS_PICTURE]", '<img src="'.Url::base_url().'Templates/'.$config->Template .'/'.$this->plateform.'/Images/News/'.$data['Pictures_News'].'" alt="" />', $text);
        $text = str_replace("[NEWS_DATA]", $data['news'], $text);

        if(stripos($text, "<!-- COM_START_STRUCTURE -->") !== false)
        {
            $verif = explode("<!-- COM_START_STRUCTURE -->", $text, 2);
            $verif2 = explode("<!-- COM_END_STRUCTURE -->", $verif[1], 2);
            $structure = $verif2[0];
            
            foreach ($News->get_liste_comm(Url::LastSegment()) as $textb)
            {
                $strucfinal = str_replace("[COMMENTAIRE_CONTENT]", $textb['commentaire'], $structure);
                $strucfinal = str_replace("[COMMENTAIRE_BY]", $Account->RecupPseudo($textb['poster']), $strucfinal);
                $strucfinal = str_replace("[COMMENTAIRE_DATES]", $textb['date'], $strucfinal);
                if ($Account->RecupGMLevel($textb['poster']) >= 2){
                    $strucfinal = str_replace("[COMMENTAIRE_STAFF]", '<div class="stafflogo"></div>', $strucfinal);
                }
                else {
                    $strucfinal = str_replace("[COMMENTAIRE_STAFF]", '', $strucfinal);
                }

                $text .= $strucfinal;	
            }

            return $text;
        }

    }
	
}
?>