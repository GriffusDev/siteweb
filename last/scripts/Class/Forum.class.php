<?php
class Forum
{
    private $bdd;
    private $bddlogon;
    private $bddchar;

    /////////////////////////////////////////////////////////////////////////////////////
    // Function construct class Forum
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $bdd = Mysql website - [Mysql Class]
    // $bddlogon = Mysql logon - [Mysql Class]
    // $bddchar = Mysql char - [Mysql Class]
    /////////////////////////////////////////////////////////////////////////////////////
    public function __construct($bdd, $bddlogon, $bddchar)
    {
        $this->bdd = $bdd;
        $this->bddlogon = $bddlogon;
        $this->bddchar = $bddchar;
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetCat (Get forum category)
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $bdd = Mysql website - [Mysql Class]
    // $bddlogon = Mysql logon - [Mysql Class]
    // $bddchar = Mysql char - [Mysql Class]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetCat($lvl) 
	{
        
        return $this->bdd->Query('SELECT cat_id, cat_nom, 
        forum_forum.forum_id, forum_name, forum_desc, forum_post, forum_topic, auth_view, forum_topic.topic_id, forum_topic.topic_titre, forum_topic.topic_post, post_id, post_time, post_createur, account_name, 
        membres.id
        FROM forum_categorie
        LEFT JOIN forum_forum ON forum_categorie.cat_id = forum_forum.forum_cat_id
        LEFT JOIN forum_post ON forum_post.post_id = forum_forum.forum_last_post_id
        LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
        LEFT JOIN membres ON membres.id = forum_post.post_createur
        WHERE auth_view <= :lvl 
        ORDER BY cat_ordre, forum_ordre', array('lvl' => $lvl));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetForum
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $gorum = Forum Id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetForum ($forum) 
	{
        
        return $this->bdd->Query('SELECT forum_name, forum_topic, auth_view, auth_topic FROM forum_forum WHERE forum_id = :forum', array('forum' => $forum));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function construct class Forum
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $bdd = Mysql website - [Mysql Class]
    // $bddlogon = Mysql logon - [Mysql Class]
    // $bddchar = Mysql char - [Mysql Class]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetTopic ($forum) 
	{
        return $this->bdd->Query('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
        Mb.account_name AS membre_pseudo_createur, post_createur, post_time, Ma.account_name AS membre_pseudo_last_posteur, post_id FROM forum_topic 
        LEFT JOIN membres Mb ON Mb.id = forum_topic.topic_createur
        LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
        LEFT JOIN membres Ma ON Ma.id = forum_post.post_createur    
        WHERE topic_genre = "Annonce" AND forum_topic.forum_id = :forum 
        ORDER BY topic_last_post DESC', array('forum' => $forum));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetAllTopicPage
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $forum = Forum ID - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetAllTopicPag ($forum) 
	{
        return $this->bdd->Query('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
        Mb.account_name AS membre_pseudo_createur, post_createur, post_time, Ma.account_name AS membre_pseudo_last_posteur, post_id FROM forum_topic 
        LEFT JOIN membres Mb ON Mb.id = forum_topic.topic_createur
        LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
        LEFT JOIN membres Ma ON Ma.id = forum_post.post_createur    
        WHERE topic_genre <> "Annonce" AND forum_topic.forum_id = :forum
        ORDER BY topic_last_post DESC', array('forum' => $forum));
        //LIMIT :premier ,:nombre', array('forum' => $forum, 'premier' => $premierMessageAafficher, 'nombre' => $nombreDeMessagesParPage));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetTopicFil
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic ID - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetTopicFil ($topic) {
        
        return $this->bdd->Query('SELECT topic_titre, topic_post, forum_topic.forum_id, topic_last_post,
        forum_name, auth_view, auth_topic, auth_post 
        FROM forum_topic 
        LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id 
        WHERE topic_id = :topic', array('topic' => $topic));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetAllPost
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic ID - [String]
    // $premier = ??????????? - [Unknow]
    // $nombre = ???????????? - [Unknow]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetAllPost ($topic, $premier, $nombre) {
        
        return $this->bdd->Query('SELECT post_id , post_createur , post_texte , post_time ,
        id, account_name, membre_inscrit, membre_avatar, membre_localisation, membre_post, membre_signature
        FROM forum_post
        LEFT JOIN membres ON id = forum_post.post_createur
        WHERE topic_id =:topic
        ORDER BY post_id', array('topic' => $topic));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetAllPost2
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic ID - [String]
    // $premier = ??????????? - [Unknow]
    // $nombre = ???????????? - [Unknow]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetAllPost2 ($topic, $premier, $nombre) 
	{    
        return $this->bdd->Query2('SELECT post_id , post_createur , post_texte , post_time ,
        id, account_name, membre_inscrit, membre_avatar, membre_localisation, membre_post, membre_signature
        FROM forum_post
        LEFT JOIN membres ON id = forum_post.post_createur
        WHERE topic_id =:topic
        ORDER BY post_id', array('topic' => $topic));
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetRead
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic ID - [String]
    // $id = TV_id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetRead ($topic, $id) 
	{       
        return $this->bdd->Query2('SELECT COUNT(*) FROM forum_topic_view WHERE tv_topic_id = :topic AND tv_id = :id', array('topic' => $topic, 'id' => $id));
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetAddRead
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = forum_topic_view ID - [String]
    // $topic = Topic ID - [String]
    // $forum = Forum ID - [String]
    // $last_post = LastPost ID - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetAddRead ($id, $topic, $forum, $last_post) 
	{
        return $this->bdd->Query2('INSERT INTO forum_topic_view (tv_id, tv_topic_id, tv_forum_id, tv_post_id) VALUES (:id, :topic, :forum, :last_post)', array('id' => $id, 'topic' => $topic, 'forum' => $forum, 'last_post' => $last_post));
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetUpdateRead
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = tv_id - [String]
    // $topic = tv_topic_id - [String]
    // $last_post = tv_post_id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetUpdateRead ($id, $topic, $last_post) 
	{  
        return $this->bdd->Query2('UPDATE forum_topic_view SET tv_post_id = :last_post WHERE tv_topic_id = :topic AND tv_id = :id', array('last_post' => $last_post, 'topic' => $topic, 'id' => $id));  
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetUpdateReadOne
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic Id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetUpdateReadOne ($topic) 
	{ 
        return $this->bdd->Query2('UPDATE forum_topic SET topic_vu = topic_vu + 1 WHERE topic_id = :topic', array('topic' => $topic)); 
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetSelectLock
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $topic = Topic ID - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetSelectLock ($topic) 
	{    
        return $this->bdd->Query2('SELECT topic_locked FROM forum_topic WHERE topic_id = :topic', array('topic' => $topic))->fetch();    
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetUpdateForumTopic
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $forum = Forum Id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetUpdateForumTopic ($forum) 
	{ 
        return $this->bdd->Query2('SELECT forum_id, forum_name FROM forum_forum WHERE forum_id <> :forum', array('forum' => $forum)); 
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetSelectAutomess
    // -------------------------------------
    // Variable | valeur
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetSelectAutomess () {
        
        return $this->bdd->Query2('SELECT automess_id, automess_titre FROM forum_automess');
        
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetFirstPost
    // -------------------------------------
    // Variable | valeur
    // =========================================
	// $topic = Topic ID - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetFirstPost ($topic) {
        
        $query = $this->bdd->Query2('SELECT topic_first_post, topic_last_post FROM forum_topic WHERE topic_id = :topic', array('topic' => $topic))->fetch();
        return $query['topic_first_post'];
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
    // Function HomeCategorieParse
    // -------------------------------------
    // Variable | valeur
    // =========================================
	// $text = HtmlStructure - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function HomeCategorieParse($text) 
	{    
		global $config;
	
		$totaldesmessages = 0;
		$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:1;
		$categorie = '';
        $verif = explode("<!-- FORUM_CATEGORIE_START -->", $text,2);
		$verif2 = explode("<!-- FORUM_CATEGORIE_END -->", $verif[1], 2);
		$structure = $verif2[0];
				
		$verifa = explode("<!-- FORUM_FORUM_START -->", $structure);
		$verifa2 = explode("<!-- FORUM_FORUM_END -->", $verifa[1]);
		
		$cat_start = $verifa[0];
		$cat_forum = $verifa2[0];
		$cat_end = $verifa2[1];
		$cat = '';
		
		foreach($this->GetCat($lvl) as $data)
		{
			$strucfinal = '';
		
			if($categorie != $data['cat_id'])
			{
				$categorie = $data['cat_id'];
				$strucfinal = str_replace("[FORUM_CAT_NAME]", utf8_decode(stripslashes(htmlspecialchars($data['cat_nom']))), $cat_start);
			}
			
			if($categorie == $data['cat_id'])
			{			
				$forum = $cat_forum;
				$forum = str_replace("[FORUM_TITLE]", stripslashes(htmlspecialchars($data['forum_name'])), $forum);
				$forum = str_replace("[FORUM_LINK]", Url::base_url().'Forum/Forums/'.$data['forum_id'], $forum);
				$forum = str_replace("[FORUM_DESC]", nl2br(stripslashes(htmlspecialchars($data['forum_desc']))), $forum);
				$forum = str_replace("[FORUM_TOPICS]", $data['forum_topic'].' Sujets <br />'.$data['forum_post'].' R&eacute;ponses', $forum);
				
				if(!empty($data['forum_post']))
				{
					$nombreDeMessagesParPage = $config->PostPerTopic;
					$nbr_post = $data['topic_post'] +1;
					$page = ceil($nbr_post / $nombreDeMessagesParPage);
				
					$forum = str_replace("[FORUM_LAST_POST_LINK]", Url::base_url().'Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'], $forum);
					$forum = str_replace("[FORUM_LAST_POST_TITLE]", $data['topic_titre'], $forum);
					$forum = str_replace("[FORUM_LAST_POST_TIME]", date('d/m/Y H:i',$data['post_time']), $forum);
					$forum = str_replace("[FORUM_LAST_POST_AUTHOR]", ucfirst(strtolower($data['account_name'])), $forum);
				}
				else
				{
					$forum = str_replace("[FORUM_LAST_POST_LINK]", ' ', $forum);
					$forum = str_replace("[FORUM_LAST_POST_TITLE]", '', $forum);
					$forum = str_replace("[FORUM_LAST_POST_TIME]", '', $forum);
					$forum = str_replace("[FORUM_LAST_POST_AUTHOR]", '', $forum);
				}
				$totaldesmessages += $data['forum_post'];
				
				// $forum = $this->AccesAuth('1', $forum, 3, $data['forum_id']);
				$strucfinal .= utf8_decode($this->AccesAuth($data['auth_view'], $forum, 3, $data['forum_id']));
			}
			
			$strucfinal .= $cat_end;
			
			$cat .= $strucfinal;	
		}	
		
		return $cat;
    }
	
	public function ForumsParse($text) 
	{  	
		global $config;
	
		if(isset($_GET['forum']) && is_numeric($_GET['forum']))
			$forum = $_GET['forum'];
		else
			die('Stop');

		$lvl = (isset($_SESSION['level']))?(int) $_SESSION['level']:1;

		$name =	$this->GetForumName($forum);
		$text = str_replace('[FORUM_TITRE_NAME]', $name[0]['forum_name'], $text);
		
		$structure = $text;
		
		$verif = explode("<!-- FORUM_ANNOUNCETOPIC_START -->", $text,2);
		$verif2 = explode("<!-- FORUM_ANNOUNCETOPIC_END -->", $verif[1], 2);
		$strucannounce = $verif2[0];
		$strucannouncefinal = '';
		
		$verifa = explode("<!-- FORUM_TOPIC_START -->", $verif2[1],2);
		$verifa2 = explode("<!-- FORUM_TOPIC_END --> ", $verifa[1], 2);
		$topicannounce = $verifa2[0];
		$structopicfinal = '';
		
		$pagestruc = '';
		
		foreach($this->GetForum($forum) as $data) 
		{
			$nombreDeMessagesParPage = $config->TopicPerPage;
			$totalDesMessages = $data['forum_topic'] + 1;
			$nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);
			$page = (isset($_GET['pagination']))?intval($_GET['pagination']):1;
			$pagestruc = '<p>Page : ';
			for ($i = 1 ; $i <= $nombreDePages; $i++)
			{
				if ($i == $page)
					$pagestruc .= $i;
				else
					$pagestruc .= '<a href="'.Url::base_url().'/Forum/Forums/'.$forum.'/'.$i.'">'.$i.'</a>';
			}
			
			$pagestruc .= '</p>';
			$premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

			$pagestruc .= '<h1>test</h1><br /><br />';
			if(!Session::VerifSession('login'))
				if ($lvl >= 1)
					$pagestruc .= '<a href="'.Url::base_url().'Forum/AddTopic/'.$forum.'"><img src="./images/nouveau.gif" alt="Nouveau sujet" title="Poster un nouveau sujet" /></a>';
		}
		
		foreach($this->GetTopic($forum) as $data) 
		{
			$nombreDeMessagesParPage = 15;
			$nbr_post = $data['topic_post'] +1;
			$page = ceil($nbr_post / $nombreDeMessagesParPage);
			$page = (isset($_GET['pagination']))?intval($_GET['pagination']):1;
		
			$announcefinal = $strucannounce;
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_LINK]', '/Forum/Topic/'.$data['topic_id'], $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_DATE]', date('d/m/Y H:i',$data['topic_time']), $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_TITLE]', stripslashes(htmlspecialchars($data['topic_titre'])), $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_NBR_POST]', $data['topic_post'], $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_NBR_VIEW]', $data['topic_vu'], $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_OWNER]', '/profil.php?m='.$data['post_createur'].'&amp;action=consulter', $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_AUTHOR]', stripslashes(htmlspecialchars($data['membre_pseudo_createur'])), $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_POST_USER]', stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])), $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_POST_LAST_DATE]', date('d/m/Y H:i',$data['post_time']), $announcefinal);
			$announcefinal = str_replace('[LAST_POST_USER_LINK]', '/profil.php?m='.$data['post_createur'], $announcefinal);
			$announcefinal = str_replace('[TOPIC_ANNOUNCE_LAST_TOPIC_LINK]', '/Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'], $announcefinal);

			$strucannouncefinal .= $announcefinal;
		}
		
		foreach ($this->GetAllTopicPag($forum) as $data)
		{
			$topicfinal = $topicannounce;
			$topicfinal = str_replace('[TOPIC_TITLE]', stripslashes(htmlspecialchars($data['topic_titre'])), $topicfinal);
			$topicfinal = str_replace('[TOPIC_DATE]', date('H\hi \l\e d M,y',$data['topic_time']), $topicfinal);
			$topicfinal = str_replace('[TOPIC_LINK]', '/Forum/Topic/'.$data['topic_id'], $topicfinal);
			$topicfinal = str_replace('[TOPIC_NBR_POST]', $data['topic_post'], $topicfinal);
			$topicfinal = str_replace('[TOPIC_NBR_VIEW]', $data['topic_vu'], $topicfinal);
			$topicfinal = str_replace('[TOPIC_OWNER]', '/profil.php?m='.$data['topic_createur'].'&amp;action=consulter', $topicfinal);
			$topicfinal = str_replace('[TOPIC_AUTHOR]', stripslashes(htmlspecialchars($data['membre_pseudo_createur'])), $topicfinal);
			$topicfinal = str_replace('[LAST_POST_USER_LINK]', '/profil.php?m='.$data['post_createur'].'&amp;action=consulter', $topicfinal);
			$topicfinal = str_replace('[TOPIC_POST_USER]', stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])), $topicfinal);
			$topicfinal = str_replace('[TOPIC_LAST_TOPIC_LINK]', '/Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'], $topicfinal);
			$topicfinal = str_replace('[TOPIC_POST_LAST_DATE]', date('d/m/Y H:i',$data['post_time']), $topicfinal);
			$structopicfinal .= $topicfinal;
		}
		
		// $structure = $pagestruc.$verif[0].$strucannouncefinal.$verifa[0].$structopicfinal.$verifa2[1];
		
		$auth =	$this->GetForumAuth($forum);
		$structure = $this->AccesAuth($auth[0]['auth_view'], $verif[0].$strucannouncefinal.$verifa[0].$structopicfinal.$verifa2[1], 2);
		
		return $structure;
	}
	
	public function GetForumAuth($forum)
	{
		return $this->bdd->Query('SELECT auth_view FROM forum_forum WHERE forum_id = :forum', array('forum' => $forum)); 
	}
	
	public function GetForumName($forum)
	{
		return $this->bdd->Query('SELECT forum_name FROM forum_forum WHERE forum_id = :forum', array('forum' => $forum)); 
	}
	
	public function TopicParse($text) 
	{
		global $config;
		
		if(isset($_GET['topic']) && is_numeric($_GET['topic']))
			$topic = $_GET['topic'];
		else
			die('Stop');
			
		$data = $this->GetTopicFil($topic);
		$forum = $data[0]['forum_id']; 
		$totalDesMessages = $data[0]['topic_post'] + 1;
		$nombreDeMessagesParPage = $config->PostPerTopic;
		$nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);
		
		return $this->AccesAuth($data[0]['auth_view'], $text, 1, $forum);
	}
	
	private function AccesAuth($acces, $text, $type, $data = 0)
	{
		switch($type)
		{
			case 1 : // Topic
			{
				if($acces == '1' && !isset($_SESSION['login']))
					return 'vous devez etre connecte pour lire ce sujet';
				elseif(isset($_SESSION['login']))
				{
					$account = new Account($this->bdd);
					$stock = $account->GetForumCanView($data);
					
					if(!$stock)
						return 'Ce forum ne vous est pas autoriser';
					else
						return $text;
				}
				else
					return $text;
					
				break;
			}
			case 2 : //Forum list
			{
				if($acces == '1' && !isset($_SESSION['login']))
					return '';
				elseif(isset($_SESSION['login']) && $acces != '0')
				{
					if(isset($_GET['forum']) && is_numeric($_GET['forum']))
					$forum = $_GET['forum'];
					
					$account = new Account($this->bdd);
					$stock = $account->GetForumCanView($forum);
					
					if(!$stock)
						return 'Ce forum ne vous est pas autoriser';
					else
						return $text;
				}
				else
					return $text;
					
				break;
			}
			case 3 : //Home list
			{
				if($acces == '1' && !isset($_SESSION['login']))
					return '';
				elseif(isset($_SESSION['login']) && $acces != '0')
				{
					$forum = $data;
					
					$account = new Account($this->bdd);
					$stock = $account->GetForumCanView($forum);
					
					if(!$stock)
						return '';
					else
						return $text;
				}
				else
					return $text;
					
				break;
			}
			default:
			{
				die('error acces');
			}
		}
	}
    
}