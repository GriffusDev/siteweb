<?php
class Lang {
    
    public function __construct() {
        
        include ($_SERVER['DOCUMENT_ROOT'].'/scripts/lang/french/general.php');
        
    }
    
    public function LoadFile ($file, $lang) {
        
        return include_once ($_SERVER['DOCUMENT_ROOT'].'/scripts/lang/'.$lang.'/'.$file.'.php');
        
    }
	
    public static function LoadFileStatic ($file, $lang) {
        
        return include_once ($_SERVER['DOCUMENT_ROOT'].'/scripts/lang/'.$lang.'/'.$file.'.php');
    }
    
}
?>