<?php
include($_SERVER['DOCUMENT_ROOT'].'/scripts/Php/Sql.php');
	
class Autoload 
{
    /////////////////////////////////////////////////////////////////////////////////////
    // Function construct
    // -------------------------------------
    // Variable | value
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function __construct() 
    {
        spl_autoload_register(function ($class) {
            include($_SERVER['DOCUMENT_ROOT'].'/scripts/Class/' . $class . '.class.php');
        });
   }  
	
}

// init autoload class
$autoload = new autoload();