<?php

class HArmory {
    
    public function __construct() {
        
        
    }
    
    public static function Cl ($id) {
        
        $char_race = array(
            1 => 'human',
            2 => 'orc',
            3 => 'dwarf',
            4 => 'nightelf',
            5 => 'scourge',
            6 => 'tauren',
            7 => 'gnome',
            8 => 'troll',
            10 => 'bloodelf',
            11 => 'draenei',
            24 => 'pandaren'
        );
        
        return $char_race[$id];
        
    }
    
    public static function Ge ($id) {
        
        $char_gender = array(
            0 => 'male',
            1 => 'female'
        );
        
        return $char_gender[$id];
        
    }
    
    public static function PlayerByte ($one, $two) {
        
        $b = $one;
        $b2 = $two;

        $ha = ($b>>16)%256;
        $hc = ($b>>24)%256;
        $fa = ($b>>8)%256;
        $sk = $b%256;
        $fh = $b2%256;
        
        return '&amp;sk='.$sk.'&amp;ha='.$ha.'&amp;hc='.$hc.'&amp;fa='.$fa.'&amp;fh='.$fh.'&amp;fc=5';
        
    }
    
    public static function Classe ($id, $gender) {
        
        if ($gender == 0) {
            $char_class_fr = array(1 => 'Guerrier',2 => 'Paladin',3 => 'Chasseur',4 => 'Voleur',5 => 'Pretre',6 => 'Chevalier de la mort',7 => 'chaman',8 => 'Mage',9 => 'Demoniste',11 => 'Druide');
        }
        else {
            $char_class_fr = array(1 => 'Guerrierre',2 => 'Paladin',3 => 'Chasseresse',4 => 'Voleuse',5 => 'Pretreresse',6 => 'Chevalier de la mort',7 => 'chaman',8 => 'Mage',9 => 'Demoniste',11 => 'Druidesse');
        }
        return $char_class_fr[$id];
                            
    }
    
    public static function Race ($id, $gender) {
        
        if ($gender == 0) {
            $char_race_fr = array(1 => 'Humain',2 => 'Orc',3 => 'Nain',4 => 'Elfe de la nuit',5 => 'Mort vivant',6 => 'Tauren',7 => 'Gnome',8 => 'Troll',10 => 'Elfe de sang',11 => 'Draeneir');
        }
        else {
            $char_race_fr = array(1 => 'Humaine',2 => 'Orc',3 => 'Naine',4 => 'Elfette de la nuit',5 => 'Morte vivante',6 => 'Tauren',7 => 'Gnome',8 => 'Troll',10 => 'Elfette de sang',11 => 'Draeneir');
        }
        return $char_race_fr[$id];
        
    }
    
    public static function Genre ($id) {
        
        $char_gender_fr = array(
        
            0 => 'Homme',
            1 => 'Femme'
            
        );
        
        return $char_gender_fr[$id];
        
    }
    
    public static function Faction ($id) {
        
        $char_faction_fr = array(
        
            1 => 'Alliance',
            2 => 'Horde',
            3 => 'Alliance',
            4 => 'Alliance',
            5 => 'Horde',
            6 => 'Horde',
            7 => 'Alliance',
            8 => 'Horde',
            10 => 'Horde',
            11 => 'Alliance'
            
        );
        
        return $char_faction_fr[$id];
        
    }
    
    public static function ManaAndCo ($id) {
        
        $Resultat = array(
            1 => 'power4',
            2 => 'power1',
            3 => 'power1',
            4 => 'power4',
            5 => 'power1',
            6 => 'None',
            7 => 'power1',
            8 => 'power1',
            9 => 'power1',
            11 => 'power1'
        );
        
        return $Resultat[$id];
        
    }
    
    public static function ManaAndCoName ($id) {
        
        $Resultat = array(
            1 => 'rage',
            2 => 'mana',
            3 => 'mana',
            4 => 'energy',
            5 => 'mana',
            6 => 'None',
            7 => 'mana',
            8 => 'mana',
            9 => 'mana',
            11 => 'mana'
        );
        
        return $Resultat[$id];
        
    }
    
    public static function ManaAndCoCss ($id) {
        
        $Resultat = array(
            1 => 'error',
            2 => 'info',
            3 => 'info',
            4 => 'warning',
            5 => 'info',
            6 => 'None',
            7 => 'info',
            8 => 'info',
            9 => 'info',
            11 => 'info'
        );
        
        return $Resultat[$id];
        
    }
    
    public static function ItemOK ($id, $entry) {

        if ($id == 21) {
            $retour = $id.','.$entry;
        }
        else {
            $retour = $id.','.$entry.',';
        }
            
        return $retour;
        
    }
    
    public static function item_template2 ($entry) {
        
        $bdd_world = sqlconnect("world");
        return $bdd_world->query("SELECT displayid, InventoryType FROM item_template WHERE entry = '".$entry."'");
        
    }
    
    public static function item_template ($entry=35) {
        
        $bdd_world = sqlconnect("world");
        $reponse = $bdd_world->query("SELECT * FROM item_template WHERE entry = '".$entry."'");
	foreach ($reponse as $row) {
            return $row['displayid'];
        }
    }
    
    public static function item_instance_by_guid_and_slot ($slot) {
        
        $bdd_world = sqlconnect("char");
        return $bdd_world->query('SELECT * FROM item_instance WHERE `guid` = "'.$slot.'" LIMIT 0,1');
        
    }
    
    public static function item_recup($guid) {
        
        $bdd_world = sqlconnect("char");
        
        for ($i = 0; $i < 19; $i++) {
            
            $row_item[$i] = array();
            
            $r = $bdd_world->query('SELECT item FROM character_inventory WHERE `slot` = "'.$i.'" AND `bag` = "0" AND `guid` = "'.$guid.'" LIMIT 0,1')->fetch();  
           
            //foreach ($r->fetch() as $slot) {
			
                if ($r) {
                                        
                    foreach (self::item_instance_by_guid_and_slot($r['item']) as $row_item[$i] ) {
                        
                        foreach (self::item_template2($row_item[$i]['itemEntry']) as $resulta) {
                            
                            //$row_item[$i]->Quality = 0;
                            $row_item[$i]['displayid'] = 0;
                            //$row_item[$i]->Quality = $resulta['Quality'];
                            $row_item[$i]['displayid'] = $resulta['displayid'];
                            $row_item[$i]['itemEntry'];
                            
                        }
                        
                    }
				
                }
                
            //}
            
	}
        
	return $row_item;
        
    }   
    
}