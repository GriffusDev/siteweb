<?php
class Account 
{
    private $bdd;
    private $bddlogon;
    private $bddchar;

    /////////////////////////////////////////////////////////////////////////////////////
    // Function constrcut class Account
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $bdd = Mysql website - [Mysql Class]
    // $bddlogon = Mysql logon - [Mysql Class]
    // $bddchar = Mysql char - [Mysql Class]
    /////////////////////////////////////////////////////////////////////////////////////
    public function __construct($bdd = 0, $bddlogon = 0, $bddchar = 0)
    {
            $this->bdd = $bdd;
            $this->bddlogon = $bddlogon;
            $this->bddchar = $bddchar;
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
    // Function Update token (for websocket)
    // -------------------------------------
    // Variable | valeur
    // =========================================
	// $login = Account name - [string]
    /////////////////////////////////////////////////////////////////////////////////////
    public function UpdateToken($login) 
    {
		$token = uniqid(rand(), true);
		$this->bdd->Query2('UPDATE membres SET Token=:token WHERE account_name = :login', array('token' => $token, ':login' => $login));
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupTotal
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $pseudo = Account name - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupTotal($pseudo) 
    {
        return $this->bdd->Query('SELECT * FROM membres WHERE account_name= :username', array('username' => strtoupper($pseudo)));
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupPseudo
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupPseudo($id) 
    {
        $recup = $this->bdd->Query('SELECT account_name FROM membres WHERE id = :id', array('id' => $id));
        return ucfirst($recup[0]['account_name']);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupMail
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupMail($id) 
    {
        $recup = $this->bddlogon->Query('SELECT email FROM account WHERE id=:id', array('id' => $id));
        return ucfirst($recup[0]['email']);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupGMLevel
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupGMLevel($id) 
    {
        $recup = $this->bdd->query2('SELECT membre_rank FROM membres WHERE id=:id', array('id' => $id))->fetch();
        return $recup['membre_rank'];
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
    // Function RecupGMLevel
    // -------------------------------------
    // Variable | valeur
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetGroup() 
    {
        $recup = $this->bdd->query('SELECT group_member.id_group, group_data.name, group_data.Rank FROM group_member, group_data WHERE group_member.id_m=:id AND group_data.id = group_member.id_group', array('id' => $_SESSION['id']));
        return $recup;
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetAllAcces (Group and User)
    // -------------------------------------
    // Variable | valeur
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetAllAcces($type = 0) 
    {
			$recup = $this->bdd->query('SELECT group_data.Rank, membres.membre_acces FROM group_member, group_data,membres WHERE group_member.id_m=:id AND membres.id=:id AND group_data.id = group_member.id_group', array('id' => $_SESSION['id']));
			
			$finalacces = '';
			$acces = count($recup);
			
			for($i = 0; $i < $acces; $i++)
				$finalacces[$i] = $recup[$i]['Rank'];
					
			if(array_key_exists('membres_acces', $recup[0]))
				$finalacces[] = $recup[0]['membre_acces'];
			
			return $finalacces;
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
    // Function GetForumView (Group and User)
    // -------------------------------------
    // Variable | valeur
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function GetForumCanView($forumid) 
    {
			$recup = $this->bdd->query('SELECT group_data.Forum_view FROM group_member, group_data,membres WHERE group_member.id_m=:id AND membres.id=:id AND group_data.id = group_member.id_group', array('id' => $_SESSION['id']));
			
			$finalacces = array();
			$acces = '';
			$accesc = count($recup);
			
			for($i = 0; $i < $accesc; $i++)
				if($recup[$i]['Forum_view'] != "")
					$acces[$i] = $recup[$i]['Forum_view'];

			$acces2 = count($acces);
			
			for($i = 0; $i < $acces2; $i ++)
			{
				$var = $acces[$i];
				if(stripos($var, ' ') !== false)
				{
					$explode = explode(' ', $var);
					$explodec = count($explode);
					$key = '';
					
					for($c = 0; $c < $explodec; $c++)
					{
						$key = $explode[$c];
						
						if(!in_array($key, $finalacces, true))
						$finalacces[] = $explode[$c];
					}
				}
				elseif($var != '')
				{
					$finalacces[] = $var;
				}
			}
			
			
		
			if(!in_array($forumid, $finalacces))			
				return false;
			else
				return true;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupNBVote
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupNBVote($id) 
    {
        $recup = $this->bdd->query('SELECT total_vote FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['total_vote'];   
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupNBVoteAct
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupNBVoteAct($id) 
    {
        $recup = $this->bdd->Query('SELECT nb_point_vote FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['nb_point_vote']; 
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupNBPoint
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupNBPoint($id) 
    {
        $recup = $this->bdd->Query('SELECT nb_point FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['nb_point'];
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupPerso
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupCharacter($id) 
    {
        $recup = $this->bdd->Query('SELECT perso FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['perso'];
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupIP
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupIP($id) 
    {
        $recup = $this->bdd->Query('SELECT membre_ip FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['membre_ip'];
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupWarning
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupWarning($id) 
    {
        $recup = $this->bdd->Query('SELECT avert FROM membres WHERE id=:id', array('id' => $id));
        return $recup[0]['avert'];
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupListAchatStore
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupListPurchaseStore($id) 
    {
        $recup = $this->bdd->Query('SELECT * FROM log_achat WHERE id_membre=:id', array('id' => $id));
        return count($recup);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupListAchatAttenteStore
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupListPurchasePending($id) 
    {
        $recup = $this->bdd->Query('SELECT * FROM log_achat WHERE id_membre=:id AND attente = 0', array('id' => $id));
        return count($recup);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupListAchatAttenteStore
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $id = Account id - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupListAuctionPending($id) 
    {
        //$recup = $this->bddchar->Query('SELECT * FROM auctionhouse WHERE id_membre=:id AND attente = 0', array('id' => $id));
        //return count($recup);
		return 0;
    }
    
}