<?php
class News
{
    private $bdd;

    public function __construct($bdd) {
        $this->bdd = $bdd;	
    }
    
    public function get_liste() {
        return $this->bdd->Query('SELECT *, DATE_FORMAT(`date_news`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date_news\' FROM news ORDER BY idnews DESC');
    }
    
    public function get_news($id) {  
        return $this->bdd->Query2('SELECT *, DATE_FORMAT(`date_news`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date_news\' FROM news WHERE idnews= :id ORDER BY idnews DESC', array('id' => $id))->fetch();
    }
    
    public function get_liste_comm ($id) {
        return $this->bdd->Query('SELECT *, DATE_FORMAT(`date`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date\' FROM news_commentaire WHERE id_news=:id ORDER BY id_com ASC', array('id' => $id));
    }
    
    public function get_count_com ($id) {
        return count($this->bdd->Query('SELECT * FROM news_commentaire WHERE id_news=:id', array('id' => $id)));
    }
    
}

$news = new News(sqlconnect('site'));