<?php
class Session{
    
    static public function CreateSession ($key, $value) {
        
        return $_SESSION[$key] = $value;
        
    }
    
    static public function VerifSession ($key) {
        
        if(isset($_SESSION[$key]))
            return true;
        else
            return false;
        
    }
    
    static public function UpdateSession ($key, $value) {
        
        if (self::VerifSession($key) === TRUE) {
            return $_SESSION[$key] = $value;
        }
        else {
            return FALSE;
        }
        
    }
    
    static public function DestroyOneSession ($key) {
        
        return session_unset($_SESSION[$key]);
        
    }
    
    static public function DestroyAllSession () {
        
        $_SESSION = array();
        return session_destroy();
        
    }
    
}