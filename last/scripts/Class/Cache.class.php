<?php
class Cache
{
	private $sitename;

	/////////////////////////////////////////////////////////////////////////////////////
	// Function constructeur de la classe
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $name = Name of site - [String]
	/////////////////////////////////////////////////////////////////////////////////////
	public function __construct($name)
	{
		$this->sitename = $name;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de creation du cache (utilisateur)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $nom = nom de la variable
	// $valeur = valeur de la variable
	/////////////////////////////////////////////////////////////////////////////////////
	function create($nom, $valeur)
	{
		if(isset($_SESSION['id']))
                {
                     apc_add($this->sitename.'_'.$_SESSION['id']."_".$nom, $valeur);
                     return true;
                }
                else
                     die('erreur de creation du cache');
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de creation du cache global
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $nom = nom de la variable
	// $valeur = valeur de la variable
	/////////////////////////////////////////////////////////////////////////////////////
	function createglobal($nom, $valeur)
	{
            apc_add($this->sitename.'_'.$nom, $valeur);
            return true;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de test d'une variable cache
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $global = [1] variable global, [0] variable utilisateur
	// $nom = nom de la variable
	/////////////////////////////////////////////////////////////////////////////////////
	function test($global, $nom)
	{
            if($global == 0)
                    $var = apc_fetch($this->sitename.'_'.$_SESSION['id']."_".$nom);
            else
                    $var = apc_fetch($this->sitename.'_'.$nom);

            if(empty($var))
                    return false;
            else
                    return true;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de recuperation des variable (utilisateur)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $nom = nom de la variable
	/////////////////////////////////////////////////////////////////////////////////////
	function fetch($global, $nom)
	{
		if($global == 0)
			$var = apc_fetch($this->sitename.'_'.$_SESSION['id']."_".$nom);
		else
			$var = apc_fetch($this->sitename.'_'.$nom);
		
		if(empty($var))
			die('erreur lors du retour de la variable utilisateur : '.$nom);
		else
		{
			$this->build_log('fetch');
			return $var;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de supression du cache (utilisateur)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $nom = nom de la variable
	/////////////////////////////////////////////////////////////////////////////////////
	function delete($global, $nom)
	{
		if($global == 0)
		{
			$var = apc_fetch($this->sitename.'_'.$_SESSION['id']."_".$nom);
			
			 if(empty($var))
				 die('erreur lors de la supression d\'une variable cache utilisateur, la variable '.$nom.' n\'existe pas');
		
			 if(isset($_SESSION['id']))
				 apc_delete($this->sitename.'_'.$_SESSION['id']."_".$nom);
			 else
				 die('Erreur de suppression d\'une variable cache utilisateur, la session est inexistante.');
		}
		else
		{
			 $var = apc_fetch($this->sitename.'_'.$nom);
			
			 if(empty($var))
				 die('erreur lors de la supression d\'une variable cache utilisateur, la variable '.$nom.' n\'existe pas');
		
			 if(isset($nom))
				 apc_delete($this->sitename.'_'.$nom);
			 else
				 die('Erreur de suppression d\'une variable cache utilisateur, la session est inexistante.');
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Function d'ecriture des log dans un fichier
	// -------------------------------------
	// Variable | valeur
	// =========================================
	/////////////////////////////////////////////////////////////////////////////////////
	private function build_log($type)
	{
		if($type == 'fetch')
		{
			$filename = fopen($_SERVER['DOCUMENT_ROOT'].'/logs/cache/fetch.log', 'r+');
			$ligne = fgets($filename);
			$ligne ++;
			fseek($filename, 0);
			fputs($filename, $ligne);
			fclose($filename);
		}
	}
}
?>