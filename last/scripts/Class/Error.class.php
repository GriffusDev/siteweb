<?php
class Error
{
	/////////////////////////////////////////////////////////////////////////////////////
	// Function de test de la requ�te select
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $error = erreur (array)
	/////////////////////////////////////////////////////////////////////////////////////
	public static function AfficheError($error)
	{
            $view = '';

            $errorc = count($error->getTrace()) - 1;
            $uri = strlen($_SERVER['DOCUMENT_ROOT'].$_SERVER['REQUEST_URI']);

            echo ERROR_START;
            echo substr($error->getTrace()[$errorc]['file'], ($uri - strlen($error->getTrace()[$errorc]['file'])));
            echo'<br />';
            echo ERROR_FUNCTION;
            echo $error->getTrace()[$errorc]['function'];
            echo"<br />";
            echo ERROR_CLASS;
            echo $error->getTrace()[$errorc]['class'];
            echo"<br />";
            echo ERROR_LINE;
            echo $error->getTrace()[$errorc]['line'];
            echo'<br />';
            echo ERROR_CODE;

            switch($error->getCode())
            {
                case 1:
                {
                    $view = MYSQL_EMPTY_FIELD;
                        break;
                }
                case 2:
                {
                        $view = MYSQL_NO_SELECT_ALL;
                        break;
                }
                case 3:
                {
                        $view = MYSQL_NO_TABLE_NAME;
                        break;
                }
                case 4:
                {
                        $view = MYSQL_NO_TABLE_EXIST;
                        break;
                }
                case 5:
                {
                        $view = MYSQL_NO_FIELD_TABLE_NAME;
                        break;
                }
                case 6:
                {
                        $view = MYSQL_NO_FIELD_FOR_TABLE;
                        break;
                }
                case 7:
                {
                        $view = MYSQL_DELETE_MORE_TABLE;
                        break;
                }
                case 8:
                {
                        $view = MYSQL_DELETE_NO_CONDITION;
                        break;
                }
                case 9:
                {
                        $view = MYSQL_DELETE_TABLE_EXIST;
                        break;
                }
                case 10:
                {
                        $view = MYSQL_QUERY_NO_CONDITION;
                        break;
                }
                case 11:
                {
                        $view = MYSQL_QUERY_NO_CONNECTOR_EXEC;
                        break;
                }
                case 12:
                {
                        $view = MYSQL_QUERY_NO_CONNECTOR_CONDITION;
                        break;
                }
                case 13:
                {
                        $view = MYSQL_QUERY_NO_CONDITION_COUNT;
                        break;
                }
                case 14:
                {
                        $view = MYSQL_UPDATE_MORE_TABLE;
                        break;
                }
                case 15:
                {
                        $view = MYSQL_INSERT_MORE_TABLE;
                        break;
                }
                case 16:
                {
                        $view = ERROR_AUTOLOAD_NO_ID;
                        break;
                }
                case 17:
                {
                        $view = ERROR_AUTOLOAD_NO_ID_EXIST;
                        break;
                }
            }

            echo $view.'<br /><br />';
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// function for generate error
	// -------------------------------------
	// Variable | valeur
	// =========================================
	// $error = number of error
	/////////////////////////////////////////////////////////////////////////////////////
	public static function CreateError($error)
	{
		try 
		{
			throw new Exception('', $error);
		}
		catch(Exception $e) 
		{
			self::AfficheError($e);
		}
	}
}
?>