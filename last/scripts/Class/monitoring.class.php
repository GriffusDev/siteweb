<?php
class monitoring
{
	private $host;
	private $port;
	
	/////////////////////////////////////////////////////
	// Function du constructeur de classe
	// --------------------------------------------------
	// Recupere l'ip et le porte envoyer lorsque la 
	// classe a ete cree
	// ##################################################
	// $host = ip a teste
	// $port = port a teste
	/////////////////////////////////////////////////////
	public function __construct($host, $port)
	{
		$this->host = $host;
		$this->port = $port;
	}
	
	/////////////////////////////////////////////////////
	// Function de retour de Ping des royaumes
	// --------------------------------------------------
	// Ping l'host indique dans la classe et envoie une
	// valeur de retour
	// ##################################################
	// Aucun parametre
	/////////////////////////////////////////////////////
	public function PingHostRealm()
	{
            $fp = @fsockopen($this->host, $this->port, $errno, $errstr, 1);

            if($fp >= 1) 
                return'up.png';
            else
                return'down.png';
	}
	
	
}
?>