<?php
class Store
{
    private $bdd;
    private $bddlogon;
    private $bddchar;

    /////////////////////////////////////////////////////////////////////////////////////
    // Function constrcut class Account
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $bdd = Mysql website - [Mysql Class]
    // $$bddlogon = Mysql logon - [Mysql Class]
    /////////////////////////////////////////////////////////////////////////////////////
    public function __construct($bdd, $bddlogon, $bddchar)
    {
        $this->bdd = $bdd;
        $this->bddlogon = $bddlogon;
        $this->bddchar = $bddchar;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Function RecupListeItem
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $type = Type d'pbjet à recherche dans la bdd
    /////////////////////////////////////////////////////////////////////////////////////
    public function RecupListeItem ($type) {
        
        return $this->bdd->Query("SELECT * FROM boutique WHERE type=:type", array('type' => $type));
        
    }
    
    
}