<?php
class Url {

    //Renvoie l'url de base
    static public function base_url()
	{  
        $baseUrl = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
        $baseUrl .= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
	$baseUrl .= '/';
        
        return $baseUrl;
        
    }
    
    //Renvoie l'url entiere
    static public function link() {
        
        $link = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
        $link .= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
        $link .= $_SERVER['REQUEST_URI'];
        
        return $link;
    }

    /**
     * Crée une url en fonction de la route.
     * @param string $route
     * @param mixed $vars variable(s) à ajouter dans l'url
     * @return string
     */
    static public function create($route='', $vars=null)
    {
        $url = self::base_url().$route;
        
        //si pas de variable(s), pas besoin d'en insérer
        if($vars===null)
            return $url;
        //si plusieurs variables dans un tableau
        if(is_array($vars))
        {
            //parcourt le tableau et ajoute les variables en les encodant
            foreach($vars as $var)
                $url.='/'.self::encode($var);
        }else
            $url.='/'.self::encode($vars); //si qu'une seule variable, l'ajoute et l'encode
        
        //retourne l'url formé
        return $url;
    }
    
    static public function LastSegment () {
        
        $parts = explode('/', "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        $last = end($parts);
        return $last;
        
    }
	
    /**
     * Redirige vers une route
     * @param string $route
     * @param mixed $vars
     * @param int $time 
     */
    static public function redirect($route='', $vars=null, $time=0)
    {
        if($time===0 && !headers_sent())
            header('location: '.self::create($route, $vars));
        else
        {
            echo '<meta http-equiv="refresh" content="'.$time.';url='.self::create($route, $vars).'" />';
        }
    }
	
    /**
     * Encode une variable pour qu'elle puisse passer par l'url
     * @param mixed $var
     * @return string
     */
    static public function encode($var)
    {
        //is_numeric arrive à reconnaitre les nombre dans des chaines
        if(is_numeric($var))
        {
            //transforme en float, puis sera tranformé par la suite en int
            $var=(float)$var;
        }
        //si c'est déjà une chaine : pas besoin de d'encodage json
        if(is_string($var))
            $var='_'.$var;
        else //sinon, on encode en json, puis on encode pour qu'il puisse être compatible avec l'url
            $var=json_encode($var);
        
        //retourne la variable encodé pour l'url
        return urlencode($var);
    }
	
	//
	static public function GetAbsoluteLink($link)
	{
		return $_SERVER['DOCUMENT_ROOT'].'/'.$link;
	}
    
}
