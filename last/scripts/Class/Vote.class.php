<?php
class Vote 
{
    private $bdd;

    /////////////////////////////////////////////////////////////////////////////////////
    // Function construct
    // -------------------------------------
    // Variable | valeur
    // =========================================
    /////////////////////////////////////////////////////////////////////////////////////
    public function __construct ($bdd)
    { 
        $this->bdd = $bdd;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function OneVerif
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $account = Account name - [String]
    // $vote = Vote - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function OneVerif ($account, $vote)  
	{
        $row = $this->bdd->Query("SELECT DATE_FORMAT(ADDTIME(date_vote1,'02:00:00'),'%Y-%m-%d %H:%i:%s') as date_vote1, DATE_FORMAT(ADDTIME(date_vote2,'02:00:00'),'%Y-%m-%d %H:%i:%s') as date_vote2 FROM membres_vote WHERE account=:login ", array('login' => $account));
        $date_now = date("Y-m-d H:i:s"); 
        if ($vote === 1) 
		{
            if($row == null || ($row[0]['date_vote1'] == null) || ($date_now > $row[0]['date_vote1'])) 
                return true;
            else 
                return false;
        }
        elseif ($vote === 2) 
		{
            if(($row == null || $row[0]['date_vote2'] == null) || ($date_now > $row[0]['date_vote2']))
                return true;
            else
                return false;
        }
        else
            return ('ERREUR');
        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////
    // Function AddVote
    // -------------------------------------
    // Variable | valeur
    // =========================================
    // $account = Account name - [String]
    // $vote = Vote - [String]
    /////////////////////////////////////////////////////////////////////////////////////
    public function AddVote ($account, $vote) 
	{
		if($account == $_SESSION['id'])
		{
			$reponse = $this->bdd->Query("SELECT membres_vote.id, membres.nb_point_vote FROM membres_vote, membres WHERE membres_vote.account= :login AND membres.id = :login", array('login' => $account));

			$date_now = date("Y-m-d H:i:s");
			
			if ($reponse == null) 
			{
					$this->bdd->Query2("INSERT INTO membres_vote (account) VALUES('".$account."')");
					$reponse = $this->bdd->Query("SELECT membres_vote.id, membres.nb_point_vote FROM membres_vote, membres WHERE membres_vote.account= :login AND membres.id = :login", array('login' => $account));
			}
			
			if ($vote === 1)  
			{
				$this->bdd->Query2("UPDATE membres_vote SET date_vote1 = :vote WHERE account = :account", array('vote' => $date_now, 'account' => $account));
				
				if ($reponse[0]['nb_point_vote'] > 2)
					$this->bdd->Query2("UPDATE membres SET nb_point_vote = 0, nb_point = nb_point + 1 WHERE id=:account", array('account' => $account));
				else
					$this->bdd->Query2("UPDATE membres SET nb_point_vote = nb_point_vote + 1, total_vote = total_vote + 1 WHERE id=:account", array('account' => $account));

				return 1;
			}
			elseif ($vote === 2)  
			{
				$this->bdd->Query2("UPDATE membres_vote SET date_vote2 = :vote WHERE account= :account", array('vote' => $date_now, 'account' => $account));
				
				if ($reponse[0]['nb_point_vote'] > 2)
					$this->bdd->Query2("UPDATE membres SET nb_point_vote = 0, nb_point = nb_point + 1 WHERE id= :account", array('account' => $account));
				else
					$this->bdd->Query2("UPDATE membres SET nb_point_vote = nb_point_vote + 1, total_vote = total_vote + 1 WHERE id = :account", array('account' => $account));

				return 1;
			}
			else
				return 0;
		}
		else
		{
			return 0;
		}
    }  
}