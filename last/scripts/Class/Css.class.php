<?php
class Css
{
	/////////////////////////////////////////////////////////////////////////////////////
	// Function GetSupport (If return 0 is computer, if return 1 is phone or tablet)
	// -------------------------------------
	// Variable | valeur
	// =========================================
	/////////////////////////////////////////////////////////////////////////////////////
	public static function GetSupport()
	{
		if(strstr($_SERVER['HTTP_USER_AGENT'],'Android') || strstr($_SERVER['HTTP_USER_AGENT'],'iPhone'))
			return 1;
		else
			return 0;
	}
}
?>