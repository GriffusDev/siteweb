<?php
$config = new stdclass();

///////////////////////////////////////////////
// Global Config
///////////////////////////////////////////////
$config->Template 	= 'Shindorei';
$config->Lang 		= 'fr';
$config->SiteName 	= 'Shindorei | Renaissance';

///////////////////////////////////////////////
// Social Config
///////////////////////////////////////////////
$config->FbAppId 	= '533082433429693';

///////////////////////////////////////////////
// Forum Config
///////////////////////////////////////////////
$config->TopicPerPage 	= 25;
$config->PostPerTopic 	= 15;

///////////////////////////////////////////////
// Social Config
///////////////////////////////////////////////
$config->LogonIp    = '37.187.26.230';
$config->LogonPort  = 3724;
$config->RealmIp    = '37.187.26.230';
$config->RealmPort  = 8086;
$config->TsIp       = '37.187.26.230';
$config->TsPort     = 10011;

///////////////////////////////////////////////
// Menu Config
///////////////////////////////////////////////
$config->Menu = array();
if(!Session::VerifSession('id'))
{
    $config->Menu[1]['item'] = NAV_1_LOGOUT;
    $config->Menu[1]['url'] = './JoinUS';
    $config->Menu[2]['item'] = NAV_2_LOGOUT;
    $config->Menu[2]['url'] = './Register';
    $config->Menu[3]['item'] = NAV_3_LOGOUT;
    $config->Menu[3]['url'] = './Bugtracker';
    $config->Menu[4]['item'] = NAV_4_LOGOUT;
    $config->Menu[4]['url'] = './Forum';
    $config->Menu[5]['item'] = NAV_5_LOGOUT;
    $config->Menu[5]['url'] = './Armory';
    $config->Menu[6]['item'] = NAV_6_LOGOUT;
    $config->Menu[6]['url'] = './Vote';
    $config->Menu[7]['item'] = NAV_7_LOGOUT;
    $config->Menu[7]['url'] = './Auction';
    $config->Menu[8]['item'] = NAV_8_LOGOUT;
    $config->Menu[8]['url'] = './Help';
}
else
{
    $config->Menu[1]['item'] = NAV_1_LOGIN;
    $config->Menu[1]['url'] = './JoinUS';
    $config->Menu[2]['item'] = NAV_2_LOGIN;
    $config->Menu[2]['url'] = './Register';
    $config->Menu[3]['item'] = NAV_3_LOGIN;
    $config->Menu[3]['url'] = './Bugtracker';
    $config->Menu[4]['item'] = NAV_4_LOGIN;
    $config->Menu[4]['url'] = './Forum';
    $config->Menu[5]['item'] = NAV_5_LOGIN;
    $config->Menu[5]['url'] = './Armory';
    $config->Menu[6]['item'] = NAV_6_LOGIN;
    $config->Menu[6]['url'] = './Vote';
    $config->Menu[7]['item'] = NAV_7_LOGIN;
    $config->Menu[7]['url'] = './Auction';
    $config->Menu[8]['item'] = NAV_8_LOGIN;
    $config->Menu[8]['url'] = './Help';
}