<?php
session_start();
include ('../Class/Session.class.php');
$captcha_length = 7;
Session::CreateSession("captcha", substr(str_shuffle("23456789ABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, $captcha_length));

$img = imagecreatetruecolor(120, 30);
$fill_color = imagecolorallocate($img,255,255,255);
imagefilledrectangle($img, 0, 0, 120, 30, $fill_color);

$text_color = imagecolorallocate($img,10,10,10);
$font = '../font/28DaysLater.ttf';
imagettftext($img, 23, 0, 5,30, $text_color, $font, $_SESSION['captcha']);

header("Content-Type: image/jpeg");
imagejpeg($img);
imagedestroy($img);

?>