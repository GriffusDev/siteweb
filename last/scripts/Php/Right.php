<div id="right">
<?php

if(!Session::VerifSession('login'))
{
	echo'
	<div id="login">
		<h1>'.LOGIN_TITLE.'</h1>
		<form id="formlogin" method="post" action="/">
			<input type="text" name="login" id="logina" placeholder="'.LOGIN_USERNAME.'" />
			<input type="password" name="mdp" id="mdp" placeholder="'.LOGIN_PASSWORD.'" />
            <input type="hidden" value="'.Url::base_url().'" name="href" id="href" />
			<input type="submit" id="submit" value="'.LOGIN_SUBMIT_BUTTON.'" />
		</form>
	</div>';
}
else
{
	echo'
	<div id="login">
		<h1>'.$_SESSION['login'].'</h1>
		<div id="account">
        <ul>
            <li class="firstli">
                <div class="accountlibg"></div><a href="'.Url::base_url().'Account">'.RIGHT_AUTH_MYACCOUNT.'</a>
            </li>
            <li><div class="accountlibg"></div><a href="'.Url::base_url().'Help">'.NAV_8_LOGOUT.'</a></li>
            <li><div class="accountlibg"></div><a href="'.Url::base_url().'Vote">'.NAV_6_LOGOUT.'</a></li>
            <li><div class="accountlibg"></div><a href="'.Url::base_url().'Store">'.RIGHT_AUTH_STORE.'</a></li>
            <li class="lastli"><div class="accountlibg"></div><a id="logout" href="'.Url::base_url().'Logout">'.RIGHT_AUTH_LOGOUT.'</a></li>
        </ul>
        </div>
    </div>';
        
}

echo'<div id="realm">
	<h1>'.REALM_TITLE.'</h1>
	<div class="realm_mop">
            <p>Identification</p>
            <div class="realmsmall">5.4.2</div>[LOGON_FIRST_STATUT]
        </div>
        <div class="realm_mop">
            <p>Tharan\'zu</p>
            <div class="realmsmall">Blizzlike</div>[REALM_FIRST_STATUT]
        </div>
        <div class="realm_mop_last">
            <p>Teamspeak</p>
            <div class="realmsmall">Discussion</div>[TEAMSPEAK_STATUT]
        </div>
    </div>
    <div id="actu">
        <h1>'.NEWSINFO_TITLE.'</h1>
        <div class="fb-like-box" data-href="http://www.facebook.com/pages/Shindorei/129740900438931" data-width="250" data-height="400" data-colorscheme="dark" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
    </div>
</div>';