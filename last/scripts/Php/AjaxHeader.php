<?php
error_reporting(E_ALL); 
ini_set("display_errors", 1);

session_start();

// include class files
include($_SERVER['DOCUMENT_ROOT'].'/scripts/Class/Autoload.class.php');

// Connexion sql
// $bdd = sqlconnect("logon");
// $bdd_site = sqlconnect("site");

//init var of class
$Lang = new Lang();
$Cache = new Cache('shindorei');

$BddSite = new Mysql(sqlconnect('site'), $Cache);
$BddLogon = new Mysql(sqlconnect('logon'), $Cache);
?>