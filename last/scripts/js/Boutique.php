<script>
//Script pour fofo
﻿$(document).ready(function(){
    $(".ClickItem").click(function(){
        $('.StoreHome').fadeOut(1000, function()
		{
            $('.StoreItem').fadeIn(500);
            $(".ClickAddItem").css('display', 'block');
            $(".ClickBack").click(function()
			{
                $('.StoreItem').fadeOut(1000, function()
				{
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddItem").css('display', 'none');
                    return false;
                });
            });
            $(".ClickAddItem").click(function()
			{
                var IdItem = $(this).attr('id');
                $('.StoreItem').fadeOut(1000, function()
				{
                    $('.StoreItemAddSection').fadeIn(500, function()
					{
                        $(".ClickAddItem").css('display', 'none');
                        $('.StoreItemAddSection').html('<p>Êtes vous sur de vouloir acheté l\'item : '+IdItem+' ? <span class="StoreOuiAddItem" style="cursor: pointer;">Oui</span> / <span class="StoreNonAddItem" style="cursor: pointer;">Non</span></p>');
                        $(".StoreNonAddItem").click(function()
						{
                            $('.StoreItemAddSection').fadeOut(1000, function()
							{
                                $('.StoreItem').fadeIn(500, function()
								{
                                    $(".ClickAddItem").css('display', 'block');
                                    return false;
                                });
                            });
                        });
						
                        $(".StoreOuiAddItem").click(function()
						{
                            $('.StoreItemAddSection').fadeOut(1000, function()
							{
                                $('.StoreItemAddSectionTY').fadeIn(500, function()
								{
                                    $('.StoreItemAddSectionTY').html('<p>Merci de votre achat : '+IdItem+' Redirection vers la liste des items dans 3 secondes</p>').delay(3000);
                                    $('.StoreItemAddSectionTY').fadeOut(1000, function()
									{
                                        $('.StoreItem').fadeIn(500, function() 
										{
                                            $(".ClickAddItem").css('display', 'block');
                                            return false;
                                        });
                                    });
                                });
                            });
                        });
                    });                     
                });
                return false;
            });
            return false;
        });
    });
	
    $(".ClickMount").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StoreMount').fadeIn(500);
            $(".ClickAddMount").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StoreMount').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddMount").css('display', 'none');
                    return false;
                });
            });
            $(".ClickAddMount").click(function(){
                var IdItem = $(this).attr('id');
                $('.StoreMount').fadeOut(1000, function(){
                    $('.StoreMountAddSection').fadeIn(500, function(){
                        $(".ClickAddMount").css('display', 'none');
                        $('.StoreMountAddSection').html('<p>Êtes vous sur de vouloir acheté la monture : '+IdItem+' ? <span class="StoreOuiAddItem" style="cursor: pointer;">Oui</span> / <span class="StoreNonAddItem" style="cursor: pointer;">Non</span></p>');
                        $(".StoreNonAddMount").click(function(){
                            $('.StoreMountAddSection').fadeOut(1000, function(){
                                $('.StoreMount').fadeIn(500, function() {
                                    $(".ClickAddMount").css('display', 'block');
                                    return false;
                                });
                            });
                        });
                        $(".StoreOuiAddItem").click(function(){
                            $('.StoreItemAddSection').fadeOut(1000, function(){
                                $('.StoreItemAddSectionTY').fadeIn(500, function(){
                                    $('.StoreItemAddSectionTY').html('<p>Merci de votre achat : '+IdItem+' Redirection vers la liste des items dans 3 secondes</p>').delay(3000);
                                    $('.StoreItemAddSectionTY').fadeOut(1000, function(){
                                        $('.StoreItem').fadeIn(500, function() {
                                            $(".ClickAddItem").css('display', 'block');
                                            return false;
                                        });
                                    });
                                });
                            });
                        });
                    });                     
                });
                return false;
            });
            return false;            
        });
    });
    $(".ClickMascotte").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StoreMascotte').fadeIn(500);
            $(".ClickAddMascotte").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StoreMascotte').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddMascotte").css('display', 'none');
                    return false;
                });
            });
        });
    });
    $(".ClickLevel").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StoreLevel').fadeIn(500);
            $(".ClickAddLevel").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StoreLevel').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddLevel").css('display', 'none');
                    return false;
                });
            });
        });
    });
    $(".ClickPO").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StorePO').fadeIn(500);
            $(".ClickAddPO").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StorePO').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddPO").css('display', 'none');
                    return false;
                });
            });
        });
    });
    $(".ClickFaction").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StoreFaction').fadeIn(500);
            $(".ClickAddFaction").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StoreFaction').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddFaction").css('display', 'none');
                    return false;
                });
            });
        });
    });
    $(".ClickRace").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StoreRace').fadeIn(500);
            $(".ClickAddRace").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StoreRace').fadeOut(1000, function(){
                    $('.StoreHome').fadeIn(500);
                    $(".ClickAddRace").css('display', 'none');
                    return false;
                });
            });
        });
    });
    $(".ClickPerso").click(function(){
        $('.StoreHome').fadeOut(1000, function(){
            $('.StorePerso').fadeIn(500);
            $(".ClickAddMPerso").css('display', 'block');
            $(".ClickBack").click(function(){
                $('.StorePerso').fadeOut(1000, function(){
                    $(".ClickAddPerso").css('display', 'none');
                    $('.StoreHome').fadeIn(500);
                    return false;
                });
            });
        });
    });
    return false;
});
</script>