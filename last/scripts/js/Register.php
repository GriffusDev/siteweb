<script>
////////////////////////////////////////////////////////
// Register Action
///////////////////////////////////////////////////////// 
$('#formregister').submit(function()
{
	var login = $('#logina').val();
	var mdp = $('#pass').val();
	var mdpconf = $('#pass_confirm').val();
	var email = $('#email').val();
	var emailr = $('#emailr').val();
	var captcha = $('#captcha').val();
	var hrefs = $('#href').val();

	$.ajax({
		type: 'POST',
		url: hrefs + 'scripts/Ajax/Verif_inscription.php',
		data: 'login='+login+'&mdp='+mdp+'&mdpconf='+mdpconf+'&email='+email+'&emailr='+emailr+'&captcha='+captcha,
		success: function(html)
		{
			switch(parseInt(html))
			{
				case 1:
				{
					Notification.Add('register', 'error', 'Le login doit contenir au minimum 4 caractères');
					return false;
					break;
				}
				case 2:
				{
					Notification.Add('register', 'error', 'Ce login est déjà utilisé par un membre');
					return false;
					break;
				}
				case 3:
				{
					Notification.Add('register', 'error', 'Le mot de passe doit contenir au minimum 6 caractères.');
					return false;
					break;
				}
				case 4:
				{
					Notification.Add('register', 'error', 'Le mot de passe doit être différent du login.');
					return false;
					break;
				}
				case 5:
				{
					Notification.Add('register', 'error', 'Les deux mots de passe doivent être identiques.');
					return false;
					break;
				}
				case 6:
				{
					Notification.Add('register', 'error', 'Les deux adresses mails doivent être identiques.');
					return false;
					break;
				}
				case 7:
				{
					Notification.Add('register', 'error', 'Le format de l\'adresse mail est incorrect.');
					return false;
					break;
				}
				case 8:
				{
					Notification.Add('register', 'error', 'Cette adresse mail est déjà utilisée par un membre.');
					return false;
					break;
				}
				case 9:
				{
					Notification.Add('register', 'error', 'Le code de sécurité doit contenir 7 caractères.');
					return false;
					break;
				}
				case 10:
				{
					Notification.Add('register', 'error', 'Le code de sécurité est incorrect.');
					return false;
					break;
				}
				case 11:
				{
					$('#center').fadeIn(500, function(){
						$('.success').remove();
						$('#center').prepend('<div class="success">Inscription réussie, vous allez recevoir un mail à l\'adresse '+email+'.<br />Pensez à vérifier vos spams si vous ne voyez pas le mail.<br />Redirection en cours, merci de patienter...</div>');
						$('#registerbg').hide();
						$('.success').fadeOut(6500, function(){
								window.location.href = 'Home';
						});
					});
					return false;
					break;
				}
			}
		}
	});
	return false;
});
</script>