<script>
////////////////////////////////////////////////////////
// Add com Action
/////////////////////////////////////////////////////////
$(document).ready(function(){
    $(".ComAdd").click(function(){     
        $('.ComAdd').fadeOut(1000, function(){
            $('.ComAdd2').fadeIn(500, function(){
                return false;
            });
        });
    });
});


////////////////////////////////////////////////////////
// AddCom Form Submit
/////////////////////////////////////////////////////////
$(".formAddCom").submit( function() 
{   
    var idCssAddCom = $('#id').val();
    var hrefs = $('#href').val();
    var ChampAddCom = $(".TextCommentaire").val();
    var By = $("#par").val();
    
    if (ChampAddCom === "") 
	{
        $('form[class=formAddCom] input[type=submit]').attr('disabled','disabled');
        $(".TextCommentaire").css('border','solid 1px #FF0000');
        
        $('#center').fadeIn(500, function(){
            $('.nt_error').remove();
            $('#center').prepend('<div class="nt_error">Merci de remplire le champs</div>');
            $('.nt_error').fadeOut(5000, function(){
                $('form[class=formAddCom] input[type=submit]').removeAttr('disabled');
		$('.TextCommentaire').css('border','solid 1px green');
            });
        });
        return false;
    }
    else 
	{
        $.ajax({
            type: 'POST',
            url: hrefs+'scripts/Ajax/Addcom.php',
            data: 'id='+idCssAddCom+'&comm='+ChampAddCom,
            success: function(html)
			{
				switch(parseInt(html))
				{
					case 1:
					{
						$('.formAddCom').remove();
						$('#center').fadeIn(500, function(){
							$('.success').remove();
							$('#center').prepend('<div class="success"><img src="http://shindorei-serveur.com/img/loading.gif" />Commentaire posté, rechargement de la page merci de patienter...</div>');
							$('.success').fadeOut(2000, function(){
											location.reload();
							});
						});
						return false;a;
						break;
					}
					case 0: 
					{
						$('.nt_error').remove();
						$('#center').fadeIn(500, function(){
							$('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
							$('.nt_error').fadeOut(7500);
						});
						return false;
						break;
					}
				}
            }
        });
        return false;
    }
    return false;  
});
</script>