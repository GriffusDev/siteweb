<script>
////////////////////////////////////////////////////////
// Notification add
/////////////////////////////////////////////////////////
function NotificationDiv()
{
        this.Add = function(page, type, texte)
        {
                switch(page)
                {
                    case 'logout':
                    {
                        switch(type) 
                        {
                            case 'success': 
                            {
                                $('#center').fadeIn(500, function(){
                                    $('.nt_success').remove();
                                    $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                    $('.nt_success').fadeOut(3500, function(){
                                        window.location.href = 'Home';
                                    });
                                });
                                return false;
                                break;
                            }
                        }
                        break;
                    }
                    case 'login':
                    {
                            switch(type)
                            {
                                    case 'error':
                                    {
                                            $('#center').fadeIn(10000, function()
                                            {
                                                    $('.nt_error').remove();
                                                    $('#center').prepend('<div class="nt_error"><div class="nt_title_error">Une erreur est survenue</div><div class="nt_text_error">' + texte + '</div></div>');
                                                    $('.nt_error').fadeOut(3500, function(){
                                                            $('form[id=formlogin] input[type=submit]').removeAttr('disabled');
                                                            $('form[id=formlogin] input[type=text]').css('border','');
                                                            $('form[id=formlogin] input[type=password]').css('border','');
                                                    });
                                            });
                                            break;
                                    }
                                    case 'success':
                                    {
                                            $('#formlogin').remove();
                                            $('#login').html('<center><img src="http://i.stack.imgur.com/qq8AE.gif" alt="" width="60px" height="60px" style="margin-top: 70px;"/></center>');
                                            $('#center').fadeIn(500, function(){
                                                $('.nt_success').remove();
                                                $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                                $('.nt_success').fadeOut(3500, function(){
                                                    location.reload();
                                                });
                                            });
                                            break;
                                    }
                            }
                            break;
                    }
                    case 'register':
                    {
                            switch(type)
                            {
                                    case 'error':
                                    {
                                            $('#registerbg').fadeIn(500, function()
                                            {
                                                    $('.nt_error').remove();
                                                    $('#registerbg').prepend('<div class="nt_error">'+ texte + '</div>');
                                                    $('form[id=formregister] input[id=logina]').css('border','solid 1px #FF0000');
                                                    $('form[id=formregister] input[type=submit]').attr('disabled','disabled');
                                                    $('.nt_error').fadeOut(3500, function(){
                                                                    $('form[id=formregister] input[type=submit]').removeAttr('disabled');
                                                    })
                                            });
                                            break;
                                    }
                            }
                            break;
                    }
                    
                    case 'forum':
                    {
                            switch(type)
                            {
                                    case 'errorrep':
                                    {
                                        $('form[class=reponserapide] input[type=submit]').attr('disabled','disabled');
                                        $('form[class=reponserapide] input[type=text]').css('border','solid 1px #FF0000');
                                        $('#center').fadeIn(500, function(){
                                            $('.nt_error').remove();
                                            $('#center').prepend('<div class="nt_error"><div class="nt_title_error">Une erreur est survenue</div><div class="nt_text_error">' + texte + '</div></div>');
                                            $('.nt_error').fadeOut(2000, function(){
                                                $('form[class=reponserapide] input[type=submit]').attr('disabled','');
                                                $('form[class=reponserapide] input[type=text]').css('border','');
                                                return false;
                                            });
                                        });
                                        break;
                                    }
                                    case 'successrep':
                                    {
                                        $('form[class=reponserapide] input[type=submit]').attr('disabled','');
                                        $('#center').fadeIn(500, function(){
                                            $('.nt_success').remove();
                                            $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">' + texte + '</div></div>');
                                            $('.nt_success').fadeOut(3500, function(){
                                                location.reload();
                                            });
                                        });
                                        break;
                                    }
                            }
                            break;
                    }
                    
                }

        }
}

////////////////////////////////////////////////////////
// Var Init
///////////////////////////////////////////////////////// 
var Notification = new NotificationDiv();
$(document).ready(function()
{

	////////////////////////////////////////////////////////
	// Logout Action
	///////////////////////////////////////////////////////// 
	$('#logout').click(function(){
            $.ajax({
                type: 'POST',
                url: '/scripts/Ajax/Logout.php',
                success: function(html)
                {
                    switch(parseInt(html))
                    {
                        case 1:
                        {
                            Notification.Add('logout', 'success', 'Déconnexion en cours, Merci de patienté...');
                            return false;
                            break;
                        }
                    }
                }
            });
            return false;
	});

	////////////////////////////////////////////////////////
	// Login Action
	///////////////////////////////////////////////////////// 
	$('#formlogin').submit(function(){
		var login = $('#logina').val();
		var mdp = $('#mdp').val();

		if(login === "") 
		{
			$('form[id=formlogin] input[type=submit]').attr('disabled','disabled');
			$('form[id=formlogin] input[type=text]').css('border','solid 1px #FF0000');
			$('form[id=formlogin] input[type=password]').css('border','solid 1px #FF0000');
			Notification.Add('login', 'error', 'Les identifiants que vous avez saisis pour vous connecter sont incorrect');
			return false;
		}
		else 
		{
			$.ajax({
				type: 'POST',
				url: '/scripts/Ajax/Login.php',
				data: 'login='+login+'&mdp='+mdp,
				success: function(html)
				{
					switch(parseInt(html))
					{
						case 1:
						{
							Notification.Add('login', 'success', 'Connexion réussie, chargement en cours...');
							return false;
							break;
						}
						case 0:
						{
                            $('form[id=formlogin] input[type=submit]').attr('disabled','disabled');
                            $('form[id=formlogin] input[type=text]').css('border','solid 1px #FF0000');
                            $('form[id=formlogin] input[type=password]').css('border','solid 1px #FF0000');
                            Notification.Add('login', 'error', 'Les identifiants que vous avez saisis pour vous connecter sont incorrect');
                            return false;
                            break;
						}
					}
				}
			});
			return false;
		}
	});
	return false;
});
</script>

<?php
if(isset($_GET['page'])) {
    switch($_GET['page'])
    {
	case 3:
	{
            include(Url::GetAbsoluteLink('scripts/js/Register.php'));
            break;
	}
	case 4:
	{
            include(Url::GetAbsoluteLink('scripts/js/PasswordEdit.php'));
            break;
	}
	case 8:
	{
            include(Url::GetAbsoluteLink('scripts/js/Comment.php'));
            break;
	}
        case 9:
	{
            include(Url::GetAbsoluteLink('scripts/js/Forum.php'));
            break;
	}
	case 10:
	{
            include(Url::GetAbsoluteLink('scripts/js/Armory.php'));
            break;
	}
	case 11:
	{
            include(Url::GetAbsoluteLink('scripts/js/Vote.php'));
            break;
	}
	case 12:
	{
            include(Url::GetAbsoluteLink('scripts/js/Boutique.php'));
            break;
	}
    }
}
?>