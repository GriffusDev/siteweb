<script>
$(document).ready(function()
{
    $('.reponserapide').submit(function() 
    {
        var message = $('#message').val();
        var idTopic = $('#Idtopic').val();
        var idForum = $('#Idforum').val();
        var hrefs = $('#href').val();
        if (message === "") 
	{
            Notification.Add('forum', 'errorrep', 'Merci de remplir le champs');
            return false;
        }
        else {
            $.ajax({
            type: 'POST',
            url: hrefs+'scripts/Ajax/Forum/AddRepTopic.php',
            data: 'id='+idTopic+'&message='+message+'&forum='+idForum,
            success: function(html)
                {
                    switch(parseInt(html))
                    {
                        case 1:
                        {
                                Notification.Add('forum', 'successrep', 'Réponse posté, rechargement de la page merci de patienter...');
                                return false;
                                break;
                        }
                        case 0: 
                        {
                                Notification.Add('forum', 'errorrep', 'Une erreur est survenu');
                                return false;
                                break;
                        }
                    }
                }
            });
            return false;
        }
        return false;  
    });
    
    $('.addtopic').submit(function() 
    {
        var message = $('#message').val();
        var title = $('#title').val();
        var idForum = $('#Idforum').val();
        var hrefs = $('#href').val();
        if (message === "") 
	{
            Notification.Add('forum', 'errorrep', 'Merci de remplir les champs');
            return false;
        }
        else {
            if (title === "") {
                Notification.Add('forum', 'errorrep', 'Merci de remplir les champs');
                return false;
            }
            else {
                $.ajax({
                type: 'POST',
                url: hrefs+'scripts/Ajax/Forum/AddTopic.php',
                data: 'title='+title+'&message='+message+'&forum='+idForum,
                success: function(html)
                    {
                        switch(parseInt(html))
                        {
                            case 1:
                            {
                                    Notification.Add('forum', 'successrep', 'Sujet posté, rechargement de la page merci de patienter...');
                                    return false;
                                    break;
                            }
                            case 0: 
                            {
                                    Notification.Add('forum', 'errorrep', 'Une erreur est survenu');
                                    return false;
                                    break;
                            }
                        }
                    }
                });
                return false;
            }
        }
        return false;  
    });
    
    $('.locktopic').click(function() 
    {
        var Idtopic = $(this).attr('id');
        var hrefs = '<?php echo Url::base_url(); ?>';
        
        $( "#dialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "blind",
                duration: 1000
            },
            buttons: {
                "oui": function() {
                    $.ajax({
                        type: 'POST',
                        url: hrefs + 'scripts/Ajax/Forum/Lock.php',
                        data: 'topic='+Idtopic+'&type=1',
                        success: function(html)
                        {
                            switch(parseInt(html))
                            {
                                case 1:
                                {
                                    Notification.Add('forum', 'successrep', 'Sujet vérouillez...');
                                    return false;
                                    break;
                                }
                                case 0:
                                {
                                    
                                    Notification.Add('forum', 'error', 'error');
                                    return false;
                                    break;
                                }
                            }
                        }
                    });
                    return false;
                },
                    "Non": function() {
                    $( this ).dialog( "close" );
                }
            }
            
        });

        $( "#dialog" ).dialog( "open" );
        
    });
    
    $('.unlocktopic').click(function() 
    {
        var Idtopic = $(this).attr('id');
        var hrefs = '<?php echo Url::base_url(); ?>';
        
        $( "#dialog2" ).dialog({
            autoOpen: false,
            show: {
                effect: "clip",
                duration: 1000
            },
            hide: {
                effect: "clip",
                duration: 1000
            },
            buttons: {
                "oui": function() {
                    $.ajax({
                        type: 'POST',
                        url: hrefs + 'scripts/Ajax/Forum/Lock.php',
                        data: 'topic='+Idtopic+'&type=0',
                        success: function(html)
                        {
                            switch(parseInt(html))
                            {
                                case 1:
                                {
                                    Notification.Add('forum', 'successrep', 'Sujet dévérouillez...');
                                    return false;
                                    break;
                                }
                                case 0:
                                {
                                    
                                    Notification.Add('forum', 'error', 'error');
                                    return false;
                                    break;
                                }
                            }
                        }
                    });
                    return false;
                },
                    "Non": function() {
                    $( this ).dialog( "close" );
                }
            }
            
        });

        $( "#dialog2" ).dialog( "open" );
    });
    
    $('.delpost').click(function() 
    {
        var Idpost = $(this).attr('id');
        var hrefs = '<?php echo Url::base_url(); ?>';
        
        $( "#dialogdelpost" ).dialog({
            autoOpen: false,
            show: {
                effect: "clip"
            },
            hide: {
                effect: "clip",
                duration: 1000
            },
            buttons: {
                "oui": function() {
                    $.ajax({
                        type: 'POST',
                        url: hrefs + 'scripts/Ajax/Forum/DelPost.php',
                        data: 'post='+Idpost,
                        success: function(html)
                        {
                            switch(parseInt(html))
                            {
                                case 1:
                                {
                                    Notification.Add('forum', 'successrep', 'Post supprimÃ©...');
                                    return false;
                                    break;
                                }
                                case 0:
                                {
                                    Notification.Add('forum', 'error', 'error');
                                    return false;
                                    break;
                                }
                            }
                        }
                    });
                    return false;
                },
                    "Non": function() {
                    $( this ).dialog( "close" );
                }
            }
            
        });

        $( "#dialogdelpost" ).dialog( "open" );
    });
    
    $('.deltopic').click(function() 
    {
        var Idtopic = $(this).attr('id');
        var Idforum = $(this).attr('idforum');
        var hrefs = '<?php echo Url::base_url(); ?>';
        
        $( "#dialogdeltopic" ).dialog({
            autoOpen: false,
            show: {
                effect: "clip"
            },
            hide: {
                effect: "clip",
                duration: 1000
            },
            buttons: {
                "oui": function() {
                    $.ajax({
                        type: 'POST',
                        url: hrefs + 'scripts/Ajax/Forum/DelTopic.php',
                        data: 'topic='+Idtopic,
                        success: function(html)
                        {
                            switch(parseInt(html))
                            {
                                case 1:
                                {
                                    Notification.Add('forum', 'successaddtopic', 'Topic supprimé...');
                                    setTimeout(function(){
                                        window.location.href = hrefs + "Forum/Forums/" + Idforum;
                                    }, 3500);
                                    return false;
                                    break;
                                }
                                case 0:
                                {
                                    Notification.Add('forum', 'error', 'error');
                                    return false;
                                    break;
                                }
                            }
                        }
                    });
                    return false;
                },
                    "Non": function() {
                    $( this ).dialog( "close" );
                }
            }
            
        });

        $( "#dialogdeltopic" ).dialog( "open" );
    });
    
});
</script>