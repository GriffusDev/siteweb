<?php
$configt = new stdclass();

$configt->page = array(
    ''            	=> 'Pages/Index.html',
    'News/$1'           => 'Pages/News/Com.html',
    'Armory' 		=> 'Pages/Armory/home.html',
    'Armory/Home' 	=> 'Pages/Armory/home.html',
    'Vote' 		=> 'Pages/Vote/Home.html',
    'Forum' 		=> 'Pages/Forum/Home.html',
    'Forum/Forums/$1'	=> 'Pages/Forum/Forum.html',
    'Forum/Topic/$1'	=> 'Pages/Forum/Topic.html',
    'Register'		=> 'Pages/Auth/Register.html',
    'Account'		=> 'Pages/Account/Home.html',
    'Store'		=> 'Pages/Store/Home.html',
    'Changelog'		=> 'Pages/Changelog/Home.html',
    'Bugtracker'	=> 'Pages/Bugtracker/Home.html',
);
?>