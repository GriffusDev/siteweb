/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-09 19:49:26
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `log_register`
-- ----------------------------
DROP TABLE IF EXISTS `log_register`;
CREATE TABLE `log_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `nb_register` int(10) NOT NULL DEFAULT '0',
  `nb_activation` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_register
-- ----------------------------
