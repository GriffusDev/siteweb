/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-06 19:53:30
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `membres_vote`
-- ----------------------------
DROP TABLE IF EXISTS `membres_vote`;
CREATE TABLE `membres_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL,
  `date_vote1` datetime NOT NULL DEFAULT '2009-07-01 09:00:00',
  `date_vote2` datetime NOT NULL DEFAULT '2009-07-01 09:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of membres_vote
-- ----------------------------
