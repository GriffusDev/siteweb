/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-13 07:40:54
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `forum_topic_view`
-- ----------------------------
DROP TABLE IF EXISTS `forum_topic_view`;
CREATE TABLE `forum_topic_view` (
  `tv_id` int(11) NOT NULL,
  `tv_topic_id` int(11) NOT NULL,
  `tv_forum_id` int(11) NOT NULL,
  `tv_post_id` int(11) NOT NULL,
  `tv_poste` enum('0','1') NOT NULL,
  PRIMARY KEY (`tv_id`,`tv_topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forum_topic_view
-- ----------------------------
