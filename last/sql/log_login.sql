/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-09 19:49:20
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `log_login`
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login` (
  `id_conn` int(10) NOT NULL AUTO_INCREMENT,
  `type_conn` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_account` int(10) NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n/a',
  PRIMARY KEY (`id_conn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log_login
-- ----------------------------
