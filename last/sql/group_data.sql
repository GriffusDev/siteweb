/*
Navicat MySQL Data Transfer

Source Server         : 37.187.7.75
Source Server Version : 50533
Source Host           : 37.187.7.75:3306
Source Database       : Shindo_site

Target Server Type    : MYSQL
Target Server Version : 50533
File Encoding         : 65001

Date: 2014-01-15 17:46:53
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `group_data`
-- ----------------------------
DROP TABLE IF EXISTS `group_data`;
CREATE TABLE `group_data` (
  `Id` int(25) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `Rank` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group_data
-- ----------------------------
INSERT INTO group_data VALUES ('1', 'Floodeur Pro', 'a');
INSERT INTO group_data VALUES ('2', 'Fan de griffus', 'a');
