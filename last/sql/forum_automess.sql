/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-13 07:40:17
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `forum_automess`
-- ----------------------------
DROP TABLE IF EXISTS `forum_automess`;
CREATE TABLE `forum_automess` (
  `automess_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `automess_mess` text NOT NULL,
  `automess_titre` varchar(200) NOT NULL,
  PRIMARY KEY (`automess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forum_automess
-- ----------------------------
INSERT INTO forum_automess VALUES ('1', 'test automess text', 'test automess titre');
