/*
Navicat MySQL Data Transfer

Source Server         : 37.187.7.75
Source Server Version : 50533
Source Host           : 37.187.7.75:3306
Source Database       : Shindo_site

Target Server Type    : MYSQL
Target Server Version : 50533
File Encoding         : 65001

Date: 2014-01-15 17:46:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `group_member`
-- ----------------------------
DROP TABLE IF EXISTS `group_member`;
CREATE TABLE `group_member` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `id_m` int(25) DEFAULT NULL,
  `id_group` int(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group_member
-- ----------------------------
INSERT INTO group_member VALUES ('1', '1', '1');
INSERT INTO group_member VALUES ('2', '1', '2');
