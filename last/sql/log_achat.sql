/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-09 19:49:16
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `log_achat`
-- ----------------------------
DROP TABLE IF EXISTS `log_achat`;
CREATE TABLE `log_achat` (
  `id_achat` int(10) NOT NULL AUTO_INCREMENT,
  `id_item` int(10) NOT NULL,
  `id_membre` int(10) NOT NULL,
  `id_perso` int(10) NOT NULL,
  `date_achat` datetime NOT NULL,
  `id_boutique` int(10) NOT NULL,
  PRIMARY KEY (`id_achat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_achat
-- ----------------------------
