/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-09 19:47:37
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `membres`
-- ----------------------------
DROP TABLE IF EXISTS `membres`;
CREATE TABLE `membres` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL DEFAULT 'none',
  `membre_lang` varchar(50) NOT NULL DEFAULT 'french',
  `membre_mdp` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_email` varchar(250) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_sexe` varchar(30) NOT NULL DEFAULT 'inconnu',
  `membre_msn` varchar(250) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_siteweb` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_avatar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_signature` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_localisation` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `membre_inscrit` int(11) NOT NULL DEFAULT '0',
  `membre_derniere_visite` int(11) NOT NULL DEFAULT '0',
  `membre_gmlevel` tinyint(4) NOT NULL DEFAULT '0',
  `membre_post` int(11) NOT NULL DEFAULT '0',
  `cacher_email` int(1) NOT NULL DEFAULT '1',
  `membre_rank` int(1) NOT NULL DEFAULT '1',
  `membre_ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `nb_point_vote` int(10) NOT NULL DEFAULT '0',
  `nb_point` int(10) NOT NULL DEFAULT '0',
  `total_vote` int(11) NOT NULL DEFAULT '0',
  `chatban` int(10) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '1',
  `perso` varchar(250) DEFAULT 'null',
  `faction` int(1) DEFAULT '0',
  `race` int(11) DEFAULT '0',
  `class` int(11) DEFAULT '0',
  `avert` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of membres
-- ----------------------------
