/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-05 20:08:20
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `news_commentaire`
-- ----------------------------
DROP TABLE IF EXISTS `news_commentaire`;
CREATE TABLE `news_commentaire` (
  `id_com` int(10) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `poster` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `commentaire` longtext NOT NULL,
  PRIMARY KEY (`id_com`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

