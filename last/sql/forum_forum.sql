/*
Navicat MySQL Data Transfer

Source Server         : shindorei
Source Server Version : 50535
Source Host           : 37.187.26.230:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2014-02-19 16:42:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for forum_forum
-- ----------------------------
DROP TABLE IF EXISTS `forum_forum`;
CREATE TABLE `forum_forum` (
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_cat_id` mediumint(8) NOT NULL,
  `forum_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `forum_desc` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `forum_ordre` mediumint(8) NOT NULL,
  `forum_last_post_id` int(11) NOT NULL,
  `forum_topic` mediumint(8) NOT NULL,
  `forum_post` mediumint(8) NOT NULL,
  `auth_view` tinyint(4) NOT NULL,
  `auth_post` tinyint(4) NOT NULL,
  `auth_topic` tinyint(4) NOT NULL,
  `auth_annonce` tinyint(4) NOT NULL,
  `auth_modo` tinyint(4) NOT NULL,
  `count_topic` int(11) DEFAULT '0',
  `count_post` int(11) DEFAULT '0',
  PRIMARY KEY (`forum_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forum_forum
-- ----------------------------
INSERT INTO `forum_forum` VALUES ('1', '1', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '60', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('2', '1', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '50', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('3', '1', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '40', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('4', '2', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '60', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('5', '2', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '50', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('6', '3', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '60', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `forum_forum` VALUES ('7', '3', 'PrÃ©sentation', 'Nouveau sur le forum? Venez vous prÃ©senter ici !', '50', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
