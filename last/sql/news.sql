/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2013-12-31 17:43:05
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `idnews` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) NOT NULL DEFAULT '',
  `news` longtext NOT NULL,
  `auteur` varchar(100) NOT NULL DEFAULT '',
  `date_news` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_maj` datetime DEFAULT NULL,
  `maj_par` varchar(100) DEFAULT NULL,
  `archive` int(1) DEFAULT '0',
  `nb_com` int(10) DEFAULT '0',
  PRIMARY KEY (`idnews`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;