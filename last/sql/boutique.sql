/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-09 19:49:45
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `boutique`
-- ----------------------------
DROP TABLE IF EXISTS `boutique`;
CREATE TABLE `boutique` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ID_item` int(10) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '1',
  `price` int(10) NOT NULL,
  `class` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=574 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of boutique
-- ----------------------------
INSERT INTO boutique VALUES ('570', 'set', 'T3,5 : Gants d&#039;incantateur', '27508', '1', '250', '8');
INSERT INTO boutique VALUES ('571', 'set', 'T3,5 : Robe d&#039;incantateur', '28229', '1', '250', '8');
INSERT INTO boutique VALUES ('572', 'arene', '100 pa', '100', '100', '250', '0');
INSERT INTO boutique VALUES ('573', 'honneur', '100', '100', '100', '250', '0');
INSERT INTO boutique VALUES ('433', 'metiers', '50 etoffes de soie', '4306', '50', '40', '0');
INSERT INTO boutique VALUES ('256', 'metiers', '50 Etoffes de lin', '2589', '50', '40', '0');
INSERT INTO boutique VALUES ('257', 'metiers', '50 Etoffes de laine', '2592', '50', '40', '0');
INSERT INTO boutique VALUES ('226', 'montures', 'Loup brun rapide', '18796', '1', '150', '0');
INSERT INTO boutique VALUES ('227', 'montures', 'Loup des bois rapide', '18797', '1', '150', '0');
INSERT INTO boutique VALUES ('228', 'montures', 'Loup gris rapide', '18798', '1', '150', '0');
INSERT INTO boutique VALUES ('65', 'pieces', '15 po (40 points)', '0', '150000', '40', '0');
INSERT INTO boutique VALUES ('66', 'pieces', '50 po (120 points)', '0', '500000', '120', '0');
INSERT INTO boutique VALUES ('67', 'pieces', '100 po (200 points)', '0', '1000000', '200', '0');
INSERT INTO boutique VALUES ('37', 'level', '1 niveau en plus (60 points)', '0', '1', '60', '0');
INSERT INTO boutique VALUES ('38', 'level', '2 niveau en plus (110 points)', '0', '2', '110', '0');
INSERT INTO boutique VALUES ('17', 'mascottes', 'Jeune faucon-dragon (bleu) (60 points)', '29958', '1', '60', '0');
INSERT INTO boutique VALUES ('18', 'mascottes', 'Oeuf de phalÃƒÂ©ne (bleu) (85 points)', '29901', '1', '85', '0');
