/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : shindo_site

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-01-13 07:40:08
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `forum_amis`
-- ----------------------------
DROP TABLE IF EXISTS `forum_amis`;
CREATE TABLE `forum_amis` (
  `ami_from` int(11) NOT NULL,
  `ami_to` int(11) NOT NULL,
  `ami_confirm` enum('0','1') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ami_date` int(11) NOT NULL,
  PRIMARY KEY (`ami_to`,`ami_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forum_amis
-- ----------------------------
