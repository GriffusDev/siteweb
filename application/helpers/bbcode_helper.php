<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/

function BBCode ($texte) {
    
    $texte = stripslashes($texte);
    $texte = htmlspecialchars($texte);
    $texte = nl2br($texte);

    $texte = preg_replace('#&lt;gras&gt;(.+)&lt;/gras&gt;#isU', '<strong>$1</strong>', $texte); 
    $texte = preg_replace('#&lt;italique&gt;(.+)\&lt;/italique&gt;#isU', '<em>$1</em>', $texte); 
    $texte = preg_replace('#&lt;souligne&gt;(.+)\&lt;/souligne&gt;#isU', '<s>$1</s>', $texte); 
    $texte = preg_replace('#&lt;barre&gt;(.+)\&lt;/barre&gt;#isU', '<del>$1</del>', $texte); 
    $texte = preg_replace('#&lt;clignote&gt;(.+)\&lt;/clignote&gt;#isU', '<blink>$1</blink>', $texte);
    $texte = preg_replace('#&lt;lien=&quot;(.+)&quot;&gt;(.+)\&lt;/lien\&gt;#isU', '<a href="http://$1" target="_blank">$2</a>', $texte);
    $texte = preg_replace('#&lt;lien&gt;(.+)\&lt;/lien&gt;#i', '<a href="$1" target="_blank">$1</a>', $texte);
    $texte = preg_replace('#&lt;images&gt;(.+)\&lt;/images\&gt;#isU', '<img src="$1" />', $texte); 
    $texte = preg_replace('#&lt;citation nom=(.+)&gt;(.+)\&lt;/citation\&gt;#isU', "<table border= \"1\" width=\"100%\"><tr><th align=\"left\">Citation de: $1</th></tr><tr><td align=\"left\">$2</td></tr></table>", $texte);
    $texte = preg_replace('#&lt;ttpetit&gt;(.+)\&lt;/ttpetit\&gt;#isU', '<span style="font-size: 7px">$1</span>', $texte); 
    $texte = preg_replace('#&lt;tpetit&gt;(.+)\&lt;/tpetit\&gt;#isU', '<span style="font-size: 12px">$1</span>', $texte); 
    $texte = preg_replace('#&lt;petit&gt;(.+)\&lt;/petit\&gt;#isU', '<span style="font-size: 15px">$1</span>', $texte); 
    $texte = preg_replace('#&lt;gros&gt;(.+)\&lt;/gros\&gt;#isU', '<span style="font-size: 20px">$1</span>', $texte); 
    $texte = preg_replace('#&lt;tgros&gt;(.+)\&lt;/tgros\&gt;#isU', '<span style="font-size: 30px">$1</span>', $texte); 
    $texte = preg_replace('#&lt;ttgros&gt;(.+)\&lt;/ttgros\&gt;#isU', '<span style="font-size: 40px">$1</span>', $texte);
    $texte = preg_replace('#&lt;ttgros&gt;(.+)\&lt;/ttgros\&gt;#isU', '<span style="font-size: 40px">$1</span>' , $texte);
    $texte = preg_replace('#\&lt;align=(left|center|right)\&gt;(.+)\&lt;/align\&gt;#isU', '<div style="float:$1">$2</span>', $texte);
    $texte = preg_replace('#\&lt;color=(red|green|blue|yellow|purple|olive)\&gt;(.+)\&lt;/color\&gt;#isU', '<span style="color:$1">$2</span>', $texte);

    $texte = str_replace(':)', ("<img src='".base_url()."img/smilies/icon_biggrin.gif' />"), $texte);
    $texte = str_replace(':D', ("<img src='".base_url()."img/smilies/icon_lol.gif' />"), $texte);
    $texte = str_replace(':love:', ("<img src='".base_url()."img/smilies/sm3.gif' />"), $texte);
    $texte = str_replace(':o', ("<img src='".base_url()."img/smilies/icon_eek.gif' />"), $texte);
    $texte = str_replace(';)', ("<img src='".base_url()."img/smilies/icon_wink.gif' />"), $texte);
    $texte = str_replace(':cry:', ("<img src='".base_url()."img/smilies/icon_cry.gif' />"), $texte);
    $texte = str_replace(':(', ("<img src='".base_url()."img/smilies/icon_sad.gif' />"), $texte);
    $texte = str_replace(';(', ("<img src='".base_url()."img/smilies/icon_evil.gif' />"), $texte);
    $texte = str_replace(':fuck:', ("<img src='".base_url()."img/smilies/sm30.gif' />"), $texte);
    $texte = str_replace(':cool:', ("<img src='".base_url()."img/smilies/sm28.gif' />"), $texte);
    $texte = str_replace(':p', ("<img src='".base_url()."img/smilies/sm12.gif' />"), $texte);
    $texte = str_replace(':haha:', ("<img src='".base_url()."img/smilies/sm38.gif' />"), $texte);

    return $texte;

}