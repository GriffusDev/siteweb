<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="shortcut icon" type="image/png" href="{function:base_url}{lang_uri}assets/images/1600/favicon.png" />
        <title><?php echo $title_for_layout; ?></title>
        <?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0):
            foreach($includes_for_layout['css'] as $css): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
            <?php endforeach;
        endif;
        if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): 
            foreach($includes_for_layout['js'] as $js): 
                if($js['options'] === NULL OR $js['options'] == 'footer'): ?>
                    <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
                <?php endif; 
            endforeach; 
        endif; ?>
        <!--[if IE]> <link href="{function:base_url}{lang_uri}assets/css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->
        <script type="text/javascript">
            $(document).ready(function()
            {
                ////////////////////////////////////////////////////////
                // Logout Action
                ///////////////////////////////////////////////////////// 
                $('#logout').click(function(){
                    Notification.Add('general', 'warning', 'Déconnexion en cours, Merci de patienter...');
                    $.ajax({
                        type: 'POST',
                        url: '{function:base_url}{lang_uri}Auth/Logout',
                        success: function(html)
                        {
                            switch(parseInt(html))
                            {
                                case 1:
                                {
                                    Notification.Add('logout', 'success', 'Déconnexion réussi...');
                                    return false;
                                    break;
                                }
                            }
                        }
                    });
                    return false;
                });

                ////////////////////////////////////////////////////////
                // Login Action
                ///////////////////////////////////////////////////////// 
                $('#formlogin').submit(function(){
                    Notification.Add('general', 'warning', 'Connexion en cours, Merci de patienter...');
                        var login = $('#logina').val();
                        var mdp = $('#mdp').val();

                        if(login === "") 
                        {
                                $('form[id=formlogin] input[type=submit]').attr('disabled','disabled');
                                $('form[id=formlogin] input[type=text]').css('border','solid 1px #FF0000');
                                $('form[id=formlogin] input[type=password]').css('border','solid 1px #FF0000');
                                Notification.Add('login', 'error', 'Les identifiants que vous avez saisis pour vous connecter sont incorrect');
                                return false;
                        }
                        else 
                        {
                                $.ajax({
                                        type: 'POST',
                                        url: '{function:base_url}{lang_uri}Auth/Login',
                                        data: 'pseudo='+login+'&mdp='+mdp,
                                        success: function(html)
                                        {
                                                switch(parseInt(html))
                                                {
                                                        case 1:
                                                        {
                                                                Notification.Add('login', 'success', 'Connexion réussie, chargement en cours...');
                                                                return false;
                                                                break;
                                                        }
                                                        case 0:
                                                        {
                                                            $('form[id=formlogin] input[type=submit]').attr('disabled','disabled');
                                                            $('form[id=formlogin] input[type=text]').css('border','solid 1px #FF0000');
                                                            $('form[id=formlogin] input[type=password]').css('border','solid 1px #FF0000');
                                                            Notification.Add('login', 'error', 'Les identifiants que vous avez saisis pour vous connecter sont incorrect');
                                                            return false;
                                                            break;
                                                        }
                                                }
                                        }
                                });
                                return false;
                        }
                });
                return false;
            });
        </script>
    </head>
    <body>
        <?php 
        if ($this->session->userdata('account_perso') == "Aucun") {
            ?>
            <div id="No_Perso_Sel">
                <h1>Aucun personnage</h1>
                <div class="persoo">Vous n'avez pas de perso séléctionné !<br />Pour profitez pleinement du site web merci d'en selectionnez un ou d'en créer un!<br /><br /><a href="#">Cliquez içi pour le séléctionner</a></div>
            </div>
            <?php
        }        
        ?>
        <header>
            <object type="application/x-shockwave-flash" id="logoflash" data="{function:base_url}assets/images/Flash/logo.swf" width="250" height="210">
            <param name="menu" value="false">
            <param name="wmode" value="transparent">
            <param name="flashvars" value="url_page={function:base_url}{lang_uri}Home">
            </object>
        </header>
        <nav>
            <ul>
                <li class="nav1"><a href="{function:base_url}{lang_uri}JoinUs/Home">{lang:Title_Page_Join_Us}</a></li>
                <li class="nav2"><a href="{function:base_url}{lang_uri}Auth/Register">{lang:Title_Page_Register}</a></li>
                <li class="nav3"><a href="{function:base_url}{lang_uri}Bug/Home">{lang:Title_Page_Bug}</a></li>
                <li class="nav4"><a href="{function:base_url}{lang_uri}Forum">{lang:Title_Page_Forum}</a></li>
                <li class="navlogo"></li>
                <li class="nav5"><a href="{function:base_url}{lang_uri}Armory/Home">{lang:Title_Page_Armory}</a></li>
                <li class="nav6"><a href="{function:base_url}{lang_uri}Vote/Home">{lang:Title_Page_Voter}</a></li>
                <li class="nav7"><a href="{function:base_url}{lang_uri}HV/Home">{lang:Title_Page_HV}</a></li>
                <li class="nav8"><a href="{function:base_url}{lang_uri}Help/Home">{lang:Title_Page_Help}</a></li>
            </ul>
        </nav>
        <div class="slider"></div>