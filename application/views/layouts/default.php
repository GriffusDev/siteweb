<?php $this->load->view('partials/header'); ?>
<div id="global">
    <div id="center">
        <?php echo $content_for_layout; ?>
    </div>
    <div id="right">
    <?php if ($this->session->userdata('logged_in') == FALSE) { ?>
    <div id="login">
        <h1>{lang:Login_Title}</h1>
        <form id="formlogin" method="post" action="/">
            <input type="text" name="login" id="logina" placeholder="{lang:Login_Username}" />
            <input type="password" name="mdp" id="mdp" placeholder="{lang:Login_Password}" />
            <input type="hidden" value="" name="href" id="href" />
            <input type="submit" id="submit" value="{lang:Login_Submit_Button}" />
        </form>
    </div>
    <?php } else { ?>
    <div id="login">
        <h1>{session:pseudo}</h1>
        <div id="account">
            <ul>
                <li class="firstli"><div class="accountlibg"></div><a href="{function:base_url}Account">{lang:Right_Auth_My_Account}</a></li>
                <li><div class="accountlibg"></div><a href="{function:base_url}Help">{lang:Title_Page_Help}</a></li>
                <li><div class="accountlibg"></div><a href="{function:base_url}Vote">{lang:Title_Page_Voter}</a></li>
                <li><div class="accountlibg"></div><a href="{function:base_url}Store">{lang:Right_Auth_Store}</a></li>
                <li class="lastli"><div class="accountlibg"></div><span id="logout" style="cursor: pointer;color: white;">{lang:Right_Auth_Logout}</span></li>
            </ul>
            <!--
            <div class="Log_Avatar">
                <img src="{function:base_url}assets/images/1600/noavatar.png" width="128" />
            </div>
            <div class="Log_Info">
                Deathart <br />
                DK - 90<br />
                Sylvanar <br />
                123 shins<br />
                132 votes
            </div>
            <div class="Log_Link">
                <a href="{function:base_url}Account"><span class="fa fa-user"></span></a> | <a href="{function:base_url}Help"><i class="fa fa-bug"></i></a> | <a href="{function:base_url}Vote"><i class="fa fa-star-o"></i></a><a href="{function:base_url}Store"><i class="fa fa-money"></i></a><span id="logout" style="cursor: pointer;color: white;"><i class="fa fa-power-off"></i></span>
            </div>
            -->
        </div>
    </div>
    <?php } ?>
    <div id="realm">
        <h1>{lang:Realm_Title}</h1>
        <div class="realm_mop">
            <p>{lang:Realm_Logon}</p>
            <div class="realmsmall">{lang:Realm_Logon_Version}</div>
            <div class="realm_statut"><img src="{function:base_url}assets/images/1600/{statut:logon}" height="25" width="25" alt="" /></div>
        </div>
        <div class="realm_mop">
            <p>tharan'zu</p>
            <div class="realmsmall">{lang:Realm_Realm_Type}</div>
            <div class="realm_statut"><img src="{function:base_url}assets/images/1600/{statut:realm}" height="25" width="25" alt="" /></div>
        </div>
        <div class="realm_mop_last">
            <p>{lang:Realm_TS}</p>
            <div class="realmsmall">{lang:Realm_TS_Desc}</div>
            <div class="realm_statut"><img src="{function:base_url}assets/images/1600/{statut:ts}" height="25" width="25" alt="" /></div>
        </div>
    </div>
    <div id="actu">
        <h1>{lang:News_Info_Title}</h1>
        <div class="fb-like-box" data-href="http://www.facebook.com/pages/Shindorei/129740900438931" data-width="250" data-height="400" data-colorscheme="dark" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
    </div>
    </div>
</div>
<?php $this->load->view('partials/footer'); ?>