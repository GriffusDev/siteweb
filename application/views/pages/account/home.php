<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/

foreach ($this->account_model->RecupGeneral($this->session->userdata('account_id')) as $data) :
?>
<script type="text/javascript">

jQuery(function($){
						   		   
	//Lorsque vous cliquez sur un lien de la classe poplight
	$('a.poplight').on('click', function() {
		var popID = $(this).data('rel'); //Trouver la pop-up correspondante
		var popWidth = $(this).data('width'); //Trouver la largeur

		//Faire apparaitre la pop-up et ajouter le bouton de fermeture
		$('#' + popID).fadeIn().css({ 'width': popWidth}).prepend('<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="X" /></a>');
		
		//Récupération du margin, qui permettra de centrer la fenêtre - on ajuste de 80px en conformité avec le CSS
		var popMargTop = ($('#' + popID).height() + 80) / 2;
		var popMargLeft = ($('#' + popID).width() + 80) / 2;
		
		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		//Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) pour corriger les bogues d'anciennes versions de IE
		$('body').append('<div id="fade"></div>');
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$('body').on('click', 'a.close, #fade', function() { //Au clic sur le body...
		$('#fade , .popup_block').fadeOut(function() {
			$('#fade, a.close').remove();  
	}); //...ils disparaissent ensemble
		
		return false;
	});

	
});

</script>
<h1>{lang:Account_Title}</h1>
<section class="accountheader">
    <div class="ma_column">
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Account} : </div>
                    <div class="accountvalue"><?php echo $data['account_name']; ?></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Email} : </div>
                    <div class="accountvalue"><?php echo $data['membre_email']; ?></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Character} : </div>
                    <div class="accountvalue"><?php echo $data['perso']; ?></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_IP} : </div>
                    <div class="accountvalue"><?php echo $data['membre_ip']; ?></div>
                    <div class="clear"></div>
            </div>
    </div>
    <div class="ma_column">
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Point_Vote_Number} : </div>
                    <div class="accountvalue"><?php echo $data['nb_point_vote']; ?></div>
                    <div class="accountlink"><a href="#">{lang:Account_Get_More}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Point_Shins_Number} : </div>
                    <div class="accountvalue"><?php echo $data['nb_point']; ?></div>
                    <div class="accountlink"><a href="#">{lang:Account_Get_More}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Warning} : </div>
                    <div class="accountvalue"><?php echo $data['avert']; ?></div>
                    <div class="accountlink"><a href="#">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Purchase_Store} : </div>
                    <div class="accountvalue">A venir...</div>
                    <div class="accountlink"><a href="#">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
    </div>
    <div class="ma_column">
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Support_Pending} : </div>
                    <div class="accountvalue">A venir...</div>
                    <div class="accountlink"><a href="#">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Auction_Pending} : </div>
                    <div class="accountvalue">A venir...</div>
                    <div class="accountlink"><a href="#">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Purchase_Pending} : </div>
                    <div class="accountvalue">A venir...</div>
                    <div class="accountlink"><a href="#">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
            <div class="ma_field">	
                    <div class="accountfield">{lang:Account_Login_History}</div>
                    <div class="accountvalue"></div>
                    <div class="accountlink"><a href="#" data-width="420" data-rel="popup_name" class="poplight">{lang:Account_View}</a></div>
                    <div class="clear"></div>
            </div>
    </div>
    <div class="ma_avatar">
            <img src="{function:base_url}assets/images/1600/noavatar.png" alt="" />
    </div>
    <div class="clear"></div>
</section>
<?php endforeach; ?>
<a href="{function:base_url}{lang_uri}ChangePassword" class="ma_password ma_button"></a>
<a href="{function:base_url}{lang_uri}Help/Account" class="ma_store ma_button"></a>
<a href="{function:base_url}{lang_uri}Store" class="ma_faction ma_button"></a>
<a href="{function:base_url}{lang_uri}Store" class="ma_point ma_button"></a>
<a href="{function:base_url}{lang_uri}Store" class="ma_block ma_button"></a>
<a href="{function:base_url}{lang_uri}Store" class="ma_race ma_button"></a>
<div id="popup_name" class="popup_block">
    <h1>{lang:Account_Login_History}</h1>
    <p>
        <?php foreach ($this->account_model->RecupLog_Login($this->session->userdata('account_id')) as $log_login) : ?>
        <?php echo $log_login['dates']; ?> | <?php echo $log_login['ip']; ?> | <img src="{function:base_url}assets/images/General/OS/<?php echo $log_login['os']; ?>.png" /> | <img src="{function:base_url}assets/images/General/NAV/<?php echo $log_login['nav']; ?>.png" /> | <img src="{function:base_url}assets/images/General/Pays/<?php echo $log_login['pays']; ?>.png" /><br />
        <?php endforeach; ?>
    </p>
</div>