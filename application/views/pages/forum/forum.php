<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/
?>
<script type="text/javascript">
$(document).ready(function(){
   $(".crant").click(function()  {
       $('#forum_voile_ajax').fadeIn(1000, function(){
           $("#forum_voile_ajax").css('display','block');
       });
   });
   $(".crantoff").click(function()  {
       $('#forum_voile_ajax').fadeOut(1000, function(){
           $("#forum_voile_ajax").css('display','none');
       });
   });
   $(".formAddTopic").submit(function()  {
       var by = "<?php echo $this->session->userdata('account_id'); ?>";
       var id_news = "<?php echo $this->uri->segment(4); ?>";
       var titre = $('#Titre_Add_Forum').val();
       var text = $('#Text_Add_Forum').val();
   });
});
</script>
<?php
foreach ($this->forum_model->GetForum($this->uri->segment(4)) as $data) {
    $nombreDeMessagesParPage = 25;
    $totalDesMessages = $data['forum_topic'] + 1;
    $nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);
    $page = (isset($_GET['pagination']))?intval($_GET['pagination']):1;
    echo '<p>Page : ';
    for ($i = 1 ; $i <= $nombreDePages ; $i++)
    {
        if ($i == $page)
        {
        echo $i;
        }
        else
        {
        echo '
        <a href="{function:base_url}{lang_uri}/Forum/Forums/'.$this->uri->segment(4).'/'.$i.'">'.$i.'</a>';
        }
    }
    echo '</p>';
    $premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

    echo '<h1>'.stripslashes(htmlspecialchars($data['forum_name'])).'</h1><br /><br />';
    
}
if($this->session->userdata('logged_in')) {
    if ($this->session->userdata('account_rank') >= 1) {
        echo'<span style="cursor: pointer;" class="crant">Create a new Topic\'s</span>';
    }
}
?>
<div id="forum_voile_ajax" style="display: none;">
    <div class="forum_add_topic_ajax">
        <div class="crantoff" style="float: right;">X</div>
        <form class="formAddTopic" method="POST" action="">
            <br />
            <p>
                <label for="titre">Titre : </label> <input type="text" name="titre" placeholder="Titre" id="Titre_Add_Forum" require />
                <br /><br /><br /><br /><br /><br /><br /><br />
                <textarea class="TextCommentaire" id="message" rows="20" placeholder="Commentaire..." name="message" id="Text_Add_Forum" require></textarea>  
                <br /><br /><br />
                <button type="submit" class="btn ButtonFofo btn-xs" style="float: right;margin-right: 50px;">{lang:Add}</button>
            </p>
        </form>
    </div>
</div>
<table>   
<tr>
    <th><img src="./images/annonce.gif" alt="Annonce" /></th>
    <th class="titre"><strong>{lang:Forum_Title}</strong></th>             
    <th class="nombremessages"><strong>{lang:Forum_Reply}</strong></th>
    <th class="nombrevu"><strong>{lang:Forum_View}</strong></th>
    <th class="auteur"><strong>{lang:Forum_Author}</strong></th>                       
    <th class="derniermessage"><strong>{lang:Forum_Last_Reply}</strong></th>
</tr>
<?php
foreach ($this->forum_model->GetTopic($this->uri->segment(4)) as $data) {
    echo'<tr><td><img src="./images/annonce.gif" alt="Annonce" /></td>
        <td id="titre"><strong>Annonce : </strong>
        <strong><a href="{function:base_url}{lang_uri}Forum/Topic/'.$data['topic_id'].'"                 
        title="Topic commencé à
        '.date('H\hi \l\e d M,y',$data['topic_time']).'">
        '.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
        <td class="nombremessages">'.$data['topic_post'].'</td>
        <td class="nombrevu">'.$data['topic_vu'].'</td>
        <td><a href="{function:base_url}{lang_uri}./profil.php?m='.$data['topic_createur'].'
        &amp;action=consulter">
        '.stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';
        $nombreDeMessagesParPage = 15;
        $nbr_post = $data['topic_post'] +1;
        $page = ceil($nbr_post / $nombreDeMessagesParPage);
        echo '<td class="derniermessage">Par
        <a href="{function:base_url}{lang_uri}./profil.php?m='.$data['post_createur'].'
        &amp;action=consulter">
        '.stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
        A <a href="{function:base_url}{lang_uri}Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'].'">'.date('H\hi \l\e d M y',$data['post_time']).'</a></td></tr>';
}
echo ('</table><table>
<tr>
<th><img src=".style/template/default/images/message.gif" alt="Message"/></th>
    <th class="titre"><strong>{lang:Forum_Title}</strong></th>             
    <th class="nombremessages"><strong>{lang:Forum_Reply}</strong></th>
    <th class="nombrevu"><strong>{lang:Forum_View}</strong></th>
    <th class="auteur"><strong>{lang:Forum_Author}</strong></th>                       
    <th class="derniermessage"><strong>{lang:Forum_Last_Reply}</strong></th>
</tr>');
foreach ($this->forum_model->GetAllTopicPag($this->uri->segment(4)) as $data) {
    echo'<tr><td><img src="./images/message.gif" alt="Message" /></td>
    <td class="titre">
    <strong><a href="{function:base_url}{lang_uri}Forum/Topic/'.$data['topic_id'].'"                 
    title="Topic commencé à
    '.date('H\hi \l\e d M,y',$data['topic_time']).'">
    '.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
    <td class="nombremessages">'.$data['topic_post'].'</td>
    <td class="nombrevu">'.$data['topic_vu'].'</td>
    <td><a href="{function:base_url}{lang_uri}./profil.php?m='.$data['topic_createur'].'
    &amp;action=consulter">
    '.stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';
    $nombreDeMessagesParPage = 15;
    $nbr_post = $data['topic_post'] +1;
    $page = ceil($nbr_post / $nombreDeMessagesParPage);
    echo '<td class="derniermessage">Par
    <a href="{function:base_url}{lang_uri}/profil.php?m='.$data['post_createur'].'
    &amp;action=consulter">
    '.stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
    A <a href="{function:base_url}{lang_uri}Forum/Topic/'.$data['topic_id'].'/'.$page.'#p_'.$data['post_id'].'">'.date('H\hi \l\e d M y',$data['post_time']).'</a></td></tr>';
}
echo ('</table>');