<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/
if($this->session->userdata('logged_in')) {
    $id = $this->session->userdata('account_id');
}
else {
    $id = 0;
}
$topic = $this->uri->segment(4);
$data = $this->forum_model->GetTopicFil($topic);
$forum=$data['forum_id']; 
$totalDesMessages = $data['topic_post'] + 1;
$nombreDeMessagesParPage = 15;
$nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);

$page = (isset($_GET['pagination']))?intval($_GET['pagination']):1;
echo 'Page : ';
for ($i = 1 ; $i <= $nombreDePages ; $i++)
{
    if ($i == $page)
    {
    echo $i;
    }
    else
    {
    echo '<a href="{function:base_url}{lang_uri}Forum/Topic/'.$topic.'/'.$i.'">
    ' . $i . '</a> ';
    }
}
echo'<br />';

$premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

if ($this->forum_model->GetAllPost2($topic, $premierMessageAafficher, $nombreDeMessagesParPage)->num_rows() == 0) {
    echo'<p>Il n y a aucun poste</p>';
}
else {
    $nbr_vu = $this->forum_model->GetRead($topic, $id);
    if ($nbr_vu == 0) {
        $this->forum_model->GetAddRead($id, $topic, $forum, $data['topic_last_post']);
    }
    else {
        $this->forum_model->GetUpdateRead($id, $topic, $data['topic_last_post']);
    }

    echo ('<table>
    <tr>
    <th class="vt_auteur"><strong>Auteurs</strong></th>             
    <th class="vt_mess"><strong>Messages</strong></th>       
    </tr>');

    foreach ($this->forum_model->GetAllPost($topic, $premierMessageAafficher, $nombreDeMessagesParPage) as $data) {
        echo'<tr><td><strong>
        <a href="./profil.php?m='.$data['id'].'&amp;action=consulter">
        '.stripslashes(htmlspecialchars($data['account_name'])).'</a></strong></td>';
        if($this->session->userdata('logged_in'))
        {
            if (($id == $data['post_createur']) OR ($this->session->userdata('account_rank') >= 3))
            {
                echo ('<td id=p_'.$data['post_id'].'>Posté ŕ '.date('H\hi \l\e d M y',$data['post_time']).'');
                if ($this->forum_model->GetFirstPost($topic) == $data['post_id']) {
                    echo'<span class="deltopic" id="'.$topic.'" idforum="'.$forum.'">Supprimer ce topic</span>
                    <div id="dialogdeltopic" title="Supprimer ?" style="display: none;">
                    Êtes vous sur de vouloir supprimer le topic ?<br />
                    </div>';
                }
                else {
                    echo'<span class="delpost" id="'.$data['post_id'].'">Supprimer ce post</span>
                    <div id="dialogdelpost" title="Supprimer ?" style="display: none;">
                    Êtes vous sur de vouloir supprimer le post ?<br />
                    </div>';
                }
                echo'
                <a href="./poste.php?p='.$data['post_id'].'&amp;action=edit">
                <img src=".style/template/default/images/editer.gif" alt="Editer"
                title="Editer ce message" /></a></td></tr>';
            }
            else {
                echo'<td> Posté ŕ '.date('H\hi \l\e d M y',$data['post_time']).'</td></tr>';
            }
        }
        else {
            echo'<td> Posté ŕ '.date('H\hi \l\e d M y',$data['post_time']).'</td></tr>';
        }
    
        echo'<tr><td>
        <img src=".style/template/default/images/avatars/'.$data['membre_avatar'].'" alt="" /> <br />Membre inscrit le '.date('d/m/Y',$data['membre_inscrit']).'<br />Messages : '.$data['membre_post'].'<br />Localisation : '.stripslashes(htmlspecialchars($data['membre_localisation'])).'</td>';
        echo'<td>'.nl2br(stripslashes(htmlspecialchars($data['post_texte']))).'<br /><hr />'.nl2br(stripslashes(htmlspecialchars($data['membre_signature']))).'</td></tr>';
    }
    echo ('</table>');

    echo '<p>Page : ';
    for ($i = 1 ; $i <= $nombreDePages ; $i++)
    {
        if ($i == $page)
        {
                        echo $i;
        }
        else
        {
            echo '<a href="topic.php?t='.$topic.'&amp;page='.$i.'">
            ' . $i . '</a> ';
        }
    }
    echo'</p>';

    $this->forum_model->GetUpdateReadOne($topic);

}

if($this->session->userdata('logged_in')) {
    if ($this->session->userdata('account_rank') >= 3) {
        
        $data2 = $this->forum_model->GetSelectLock($topic);
        
        if ($data2['topic_locked'] == 1)
        {
            echo'<span class="unlocktopic" id="'.$topic.'">
                <img src="./images/lock.gif" alt="Dévérouiller" title="vérouiller ce sujet" /></span><div id="dialog2" title="Dévérouillez ?" style="display: none;">
               Êtes vous sur de vouloir le dévérouiller ?<br />
                </div>';
        }
        else
        {
            echo'<span class="locktopic" id="'.$topic.'">
                <img src="./images/lock.gif" alt="Vérouiller" title="vérouiller ce sujet" /></span><div id="dialog" title="Vérouillez ?" style="display: none;">
               Êtes vous sur de vouloir le vérouiller ?<br />
                </div>';
        }

        echo'<p>Déplacer vers :</p>
        <form method="post" action="action.php?action=deplacer&amp;t='.$topic.'">
        <select name="dest">';               
        foreach($this->forum_model->GetUpdateForumTopic($forum) as $data)
        {
             echo'<option value='.$data['forum_id'].' id='.$data['forum_id'].'>'.$data['forum_name'].'</option>';
        }
        echo'
        </select>
        <input type="hidden" name="from" value='.$forum.'>
        <input type="submit" name="submit" value="Envoyer" />
        </form>';
        
    }
    if ($this->session->userdata('account_rank') >= 1) {
        
        if ($data2['topic_locked'] == 0)
        {
            echo ('RÃ©ponse');
            ?>
            <form class="reponserapide" method="post" action="">
                <input type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('<gras>', '</gras>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('<italique>', '</italique>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="soulignÃ©" name="soulignÃ©" value="SoulignÃ©" onClick="javascript:bbcode('<souligne>', '</souligne>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('<lien>', '</lien>');return(false)" class="btn btn-sm ButtonFofo" />
                <input type="button" id="image" name="image" value="Images" onClick="javascript:bbcode('<images>', '</images>');return(false)" class="btn btn-sm ButtonFofo" />
                <br />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(' :D ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/rire.gif" title="lol" alt="lol" onClick="javascript:smilies(' :lol: ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/triste.png" title="triste" alt="triste" onClick="javascript:smilies(' :( ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/huh.png" title="choc" alt="choc" onClick="javascript:smilies(' :o ');return(false)" />
                <br />
                <textarea cols="80" rows="8" id="message" name="message"></textarea>
                <input type="text" value="<?php echo $topic; ?>" name="Idtopic" id="Idtopic" style="display: none;" placeholder="Idtopic">
                <input type="text" value="<?php echo $forum; ?>" name="Idforum" id="Idforum" style="display: none;" placeholder="Idforum">
                <input type="text" value="{function:base_url}" name="href" id="href" style="display: none;" placeholder="Lien">
                <input type="submit" name="submit" value="Envoyer" />
            </form>
        <?php
        }
        
    }    
}