<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

?>
<h1 class="forum_head"><?php echo $this->forum_model->RecupNameCat($this->uri->segment(4)); ?></h1>  
    <?php foreach($this->forum_model->RecupForum($this->uri->segment(4), $lvl) as $messages): ?>
    <div class="forum_bg">
            <div class="icon_open"></div>
            <div class="forum_title"><a href="{function:base_url}{lang_uri}Forum/Forums/<?php echo $messages->forum_id; ?>"><?php echo $messages->forum_name; ?></a></div>
            <div class="forum_desc"><?php echo $messages->forum_desc; ?><br /><a href="#">Archives</a></div>

            <div class="forum_topics"><?php echo $messages->count_topic; ?> {lang:Forum_Topics}<br /><?php echo $messages->count_post; ?> {lang:Forum_Posts}</div>

            <div class="forum_lastpost">
                <?php 
                if ($messages->topic_titre == null) {
                ?>
                <br /><a>{lang:Forum_No_Message}</a>
                <?php
                }
                else {
                ?>
                <a href="{function:base_url}{lang_uri}Forum/Topic/<?php echo $messages->topic_id; ?>#<?php echo $messages->post_id; ?>"><?php echo $messages->topic_titre; ?></a><br />
                {lang:Le} <?php echo date('d/m/Y H:i',$messages->post_time); ?><br />
                {lang:By} <a href="{function:base_url}Profil/<?php echo $messages->id; ?>"><?php echo ucfirst(strtolower($messages->account_name)); ?></a>
                <?php
                } 
                ?>
            </div>
    </div>
    <?php endforeach; ?>