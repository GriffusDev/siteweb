{news_entries}
<script type="text/javascript">
////////////////////////////////////////////////////////
// Add com Action
/////////////////////////////////////////////////////////
$(document).ready(function(){
    $(".ComAdd").click(function(){
        $('.ComAdd').fadeOut(1000, function(){
            $('.ComAdd2').fadeIn(500, function(){
                return false;
            });
        });
    });

    ////////////////////////////////////////////////////////
    // AddCom Form Submit
    /////////////////////////////////////////////////////////

    $(".formAddCom").submit(function()  {
        Notification.Add('general', 'warning', 'Chargement en cours, Merci de patienté...');
        if ($(".TextCommentaire").val() === "") {
            $('form[class=formAddCom] input[type=submit]').attr('disabled','disabled');
            $(".TextCommentaire").css('border','solid 1px #FF0000');
            $('#center').fadeIn(500, function(){
                $('.nt_success').remove();
                $('.nt_warning').remove();
                $('.nt_error').remove();
                $('#center').prepend('<div class="nt_error">Merci de remplire le champs</div>');
                $('.nt_error').fadeOut(5000, function(){
                    $('form[class=formAddCom] input[type=submit]').removeAttr('disabled');
                    $('.TextCommentaire').css('border','solid 1px green');
                });
            });
            return false;
        }
        else {
            $.ajax({
                type: 'POST',
                url: '{function:base_url}{lang_uri}Home/Addcom/{idnews}/'+$(".TextCommentaire").val(),
                data: '',
                success: function(html) {
                    switch(parseInt(html))
                    {
                        case 1:
                        {
                            $('.formAddCom').remove();
                            $('#center').fadeIn(500, function(){
                                $('.nt_success').remove();
                                $('.nt_warning').remove();
                                $('.nt_error').remove();
                                $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">Commentaire posté, rechargement de la page merci de patienter...</div></div>');
                                $('.nt_success').fadeOut(3000, function(){
                                    location.reload();
                                });
                            });
                            return false;
                            break;
                        }
                        default: 
                        {
                            $('.nt_success').remove();
                            $('.nt_warning').remove();
                            $('.nt_error').remove();
                            $('#center').fadeIn(500, function(){
                                $('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
                                $('.nt_error').fadeOut(7500);
                            });
                            return false;
                            break;
                        }
                    }
                }
            });
            return false;
        }
        return false;  
    });
    
});
</script>
<h1>{titre}</h1>
<div class="stafflogo"></div>
<div class="center_contenu">
    <div class="news_picture"><img src="{function:base_url}assets/images/News/{pictures}" /><div class="news_info">Par <strong style="color: orange;"><?php echo strtolower('{account_name}'); ?></strong><br />{date_news}</div></div>
    <div class="news_text_comm">
    <?php echo BBCode('{news}'); ?>
    </div>
    <div class="clear"></div>
</div>
<h1 style="text-align: center;">{lang:Com_Title}</h1>
<div class="sep"></div>
<script>
$(document).ready(function() {
    $(".YesDeleteCom").click(function(){
        Notification.Add('general', 'warning', 'Chargement en cours, Merci de patienté...');
        var id_com = $(this).attr('id');
        $.ajax({
            type: 'POST',
            url: '{function:base_url}{lang_uri}Home/Delcom/'+id_com+'/del',
            data: '',
            success: function(html) {
                switch(parseInt(html))
                {
                    case 1:
                    {
                        $('.formAddCom').remove();
                        $('#center').fadeIn(500, function(){
                            $('.nt_success').remove();
                            $('.nt_warning').remove();
                            $('.nt_error').remove();
                            $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">Commentaire Supprimer, rechargement de la page merci de patienter...</div></div>');
                            $('.nt_success').fadeOut(3000, function(){
                                location.reload();
                            });
                        });
                        return false;
                        break;
                    }
                    default: 
                    {
                        $('.nt_success').remove();
                        $('.nt_warning').remove();
                        $('.nt_error').remove();
                        $('#center').fadeIn(500, function(){
                            $('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
                            $('.nt_error').fadeOut(7500);
                        });
                        return false;
                        break;
                    }
                }
            }
        });
        return false;
    });
    $(".YesHideCom").click(function(){
        Notification.Add('general', 'warning', 'Chargement en cours, Merci de patienté...');
        var id_com = $(this).attr('id');
        $.ajax({
            type: 'POST',
            url: '{function:base_url}{lang_uri}Home/Delcom/'+id_com+'/hide',
            data: '',
            success: function(html) {
                switch(parseInt(html))
                {
                    case 1:
                    {
                        $('.formAddCom').remove();
                        $('#center').fadeIn(500, function(){
                            $('.nt_success').remove();
                            $('.nt_warning').remove();
                            $('.nt_error').remove();
                            $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">Commentaire Cacher, rechargement de la page merci de patienter...</div></div>');
                            $('.nt_success').fadeOut(3000, function(){
                                location.reload();
                            });
                        });
                        return false;
                        break;
                    }
                    default: 
                    {
                        $('.nt_success').remove();
                        $('.nt_warning').remove();
                        $('.nt_error').remove();
                        $('#center').fadeIn(500, function(){
                            $('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
                            $('.nt_error').fadeOut(7500);
                        });
                        return false;
                        break;
                    }
                }
            }
        });
        return false;
    });
    $(".YesHidesCom").click(function(){
        Notification.Add('general', 'warning', 'Chargement en cours, Merci de patienté...');
        var id_com = $(this).attr('id');
        $.ajax({
            type: 'POST',
            url: '{function:base_url}{lang_uri}Home/Delcom/'+id_com+'/hides',
            data: '',
            success: function(html) {
                switch(parseInt(html))
                {
                    case 1:
                    {
                        $('.formAddCom').remove();
                        $('#center').fadeIn(500, function(){
                            $('.nt_success').remove();
                            $('.nt_warning').remove();
                            $('.nt_error').remove();
                            $('#center').prepend('<div class="nt_success"><div class="nt_title_success">Félicitation</div><div class="nt_text_success">Commentaire revenu avec success, rechargement de la page merci de patienter...</div></div>');
                            $('.nt_success').fadeOut(3000, function(){
                                location.reload();
                            });
                        });
                        return false;
                        break;
                    }
                    default: 
                    {
                        $('.nt_success').remove();
                        $('.nt_warning').remove();
                        $('.nt_error').remove();
                        $('#center').fadeIn(500, function(){
                            $('#center').prepend('<div class="nt_error">Une erreur internet est survenu merci de contacter le staff</div>');
                            $('.nt_error').fadeOut(7500);
                        });
                        return false;
                        break;
                    }
                }
            }
        });
        return false;
    });
    $("#DelQuest").click(function(){
        var id_com = $(this).attr('class');
        $('#DelQuest').fadeOut(1000, function(){
            $('.DelRep-'+id_com).fadeIn(500, function(){
                return false;
            });
        });
    });
});
</script>
<?php foreach ($this->news_model->get_liste_comm($this->uri->segment(4))->result() as $data) : ?>
<div class="center_contenu">
    <div class="news_picture"><img src="{function:base_url}assets/images/Avatar/noavatar.jpg" alt="" /></div>
    <div class="news_text_comm">
        <?php
        if ($data->hide == "0") { 
            echo ('Message supprimer'); 
            if ($this->session->userdata('logged_in') == TRUE) { 
                if ($this->session->userdata('account_rank') >= 3) {
                ?>
                <span class="YesHidesCom" id="<?php echo $data->id_com; ?>">Remettre en public</span>
                <?php
                }
            }
        } else { ?>
            <div class="comm_info">
                {lang:By} <?php echo $data->account_name; ?> / <?php echo $data->date; ?> <?php if ($this->session->userdata('logged_in') == TRUE) { if ($this->session->userdata('account_rank') >= 3) { 
                    echo ('<span id="DelQuest" class="'.$data->id_com.'"> 
        <img src="'.base_url().'assets/images/General/error.png" /> 
    </span>
    <span style="display: none;cusror: pointer;" class="DelRep-'.$data->id_com.'">
                            <span class="YesHideCom" id="'.$data->id_com.'">Cacher</span>/');if ($this->session->userdata('account_rank') >= 8) { echo('<span class="YesDeleteCom" id="'.$data->id_com.'">Supprimer</span>');}echo('</span>
                          '); } }?>
            </div>
            <?php echo BBCode($data->commentaire);
        } 
        ?>
    </div>
    <div class="clear"></div>
</div>
<?php endforeach; if ($this->session->userdata('logged_in') == TRUE) { ?>
    <center>
        <a class="ComAdd" style="text-align: center;cursor: pointer;">{lang:Com_Add}</a>
        <div class="ComAdd2" style="display: none;">
            <form class="formAddCom" method="POST" action="">
                <input type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('<gras>', '</gras>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('<italique>', '</italique>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="souligné" name="souligné" value="Souligné" onClick="javascript:bbcode('<souligne>', '</souligne>');return(false)" class="btn ButtonFofo btn-xs" />
                <input type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('<lien>', '</lien>');return(false)" class="btn btn-sm ButtonFofo" />
                <input type="button" id="image" name="image" value="Images" onClick="javascript:bbcode('<images>', '</images>');return(false)" class="btn btn-sm ButtonFofo" />
                <br />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(' :D ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/rire.gif" title="lol" alt="lol" onClick="javascript:smilies(' :lol: ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/triste.png" title="triste" alt="triste" onClick="javascript:smilies(' :( ');return(false)" />
                <img src="http://fr.openclassrooms.com/Templates/images/smilies/huh.png" title="choc" alt="choc" onClick="javascript:smilies(' :o ');return(false)" />
                <br />
                <textarea class="TextCommentaire" id="message" data="3" placeholder="Commentaire..." name="message"></textarea>
                <br />
                <button type="submit" class="btn ButtonFofo btn-xs">{lang:Add}</button>
            </form>
        </div>
    </center>
    <?php
}
else {
    ?>
    <center><a style="text-align: center;">Merci de vous inscrire ou de vous connectez !</a></center>
    <?php
}
?>
{/news_entries}