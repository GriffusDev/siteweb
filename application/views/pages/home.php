<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php foreach ($this->news_model->get_liste()->result_array() as $data) : ?>
<h1><?php echo $data['titre']; ?><a href="{function:base_url}Home/News/<?php echo $data['idnews']; ?>" class="news_comment"><?php echo $this->news_model->count_com($data['idnews']); ?></a></h1>
<div class="stafflogo"></div>
<div class="center_contenu">
    <div class="news_picture"><img src="{function:base_url}assets/images/News/<?php echo $data['pictures']; ?>" /><div class="news_info">Par <strong style="color: orange;"><?php echo ucfirst(strtolower($data['account_name'])); ?></strong><br /><?php echo $data['date_news']; ?></div></div>
    <div class="news_text">
        <?php echo character_limiter(BBCode($data['news']), '878', '&#8230; <a href="{function:base_url}{lang_uri}Home/News/'.$data['idnews'].'">Lire la suite</a>'); ?>
    </div>
    <div class="clear"></div>
</div>
<?php 
endforeach;