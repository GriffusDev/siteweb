<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_model extends CI_Model {
    
    public function __construct(){
        
        parent::__construct();
        
    }
    
    public function RecupGeneral ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        return $DB3->query('SELECT * FROM membres WHERE id = "'.$id.'"')->result_array();
        
    }
    
    public function RecupLog_Login ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        return $DB3->query('SELECT *, DATE_FORMAT(`dates`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'dates\' FROM log_login WHERE account = "'.$id.'"')->result_array();
        
    }
    
}