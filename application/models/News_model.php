<?php

/**
 * -----------------------------------------------
 *
 * This file is part of deathart.
 *
 * Diamond is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Diamond is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Diamond. If not, see <http://www.gnu.org/licenses/>
 *
 * @author Louis BRABANT <louis.brabant@laposte.net>
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * -----------------------------------------------
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News_model extends CI_Model {
    
    public function __construct(){
        
        parent::__construct();
        
    }
    
    public function get_liste() {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        return $DB3->query('SELECT *, DATE_FORMAT(`date_news`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date_news\' FROM news LEFT JOIN membres ON membres.id = news.auteur ORDER BY idnews DESC');
        
    }
    
    public function get_news($id) { 
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        return $DB3->Query('SELECT *, DATE_FORMAT(`date_news`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date_news\' FROM news LEFT JOIN membres ON membres.id = news.auteur WHERE idnews= "'.$id.'" ORDER BY idnews DESC');
    }
    
    public function get_liste_comm ($id) {
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        return $DB3->Query('SELECT *, DATE_FORMAT(`date`,\'Le %d/%m/%Y &agrave; %H:%i:%s\') AS \'date\', account_name FROM news_commentaire LEFT JOIN membres ON membres.id = news_commentaire.poster WHERE id_news="'.$id.'" ORDER BY id_com ASC');
    }
    
    public function add_com($id, $comm, $by) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->Query('INSERT INTO news_commentaire(id_news, poster, date, commentaire) VALUES ("'.$id.'", "'.$by.'", "'.date('Y-m-d H:i:s').'", "'.$comm.'")');
        $DB3->Query('UPDATE news SET nb_com = +1 WHERE idnews = "'.$id.'"');
        $DB3->cache_delete('Home', 'News');
        
    }
    
    public function count_com($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        $resultat = $DB3->Query('SELECT COUNT(*) AS nb_total FROM news_commentaire WHERE id_news = "'.$id.'" AND hide = "1"')->row_array();
        
        return $resultat['nb_total'];
        
    }
    
    public function del_com($id, $type = "") {
        
        $DB3 = $this->load->database('default', TRUE);
        
        if ($type == "hide") {
            $DB3->Query('UPDATE news_commentaire SET hide = "0" WHERE id_com = "'.$id.'"');
            $DB3->cache_delete('Home', 'News');
            echo 1;
        }
        elseif ($type == "del") {
            $DB3->Query('DELETE FROM news_commentaire WHERE id_com = "'.$id.'"');
            $DB3->cache_delete('Home', 'News');
            echo 1;
        }
        elseif ($type == "hides") {
            $DB3->Query('UPDATE news_commentaire SET hide = "1" WHERE id_com = "'.$id.'"');
            $DB3->cache_delete('Home', 'News');
            echo 1;
        }
        else {
            $DB3->cache_delete('Home', 'News');
            echo 0;
        }
        
    }
    
}