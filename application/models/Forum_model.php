<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forum_model extends CI_Model {
    
    public function __construct(){
        
        parent::__construct();
        
    }
    
    public function RecupCat ($id = "") {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        if ($id == "") {
        return $DB1->select('cat_id, cat_nom', false)
                   ->from('forum_categorie')
                   ->order_by("cat_ordre", "asc")
                   ->get()
                   ->result();
        }
        else {
            return $DB1->select('cat_id, cat_nom', false)
                   ->from('forum_categorie')
                   ->where('cat_id', $id)
                   ->get()
                   ->result();
        }
        
    }
    
    public function RecupForum ($id, $lvl) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        return $DB1->Query('SELECT forum_forum.forum_id, forum_name, forum_desc, forum_post, forum_topic, auth_view, count_topic, count_post, forum_topic.topic_id, forum_topic.topic_titre, forum_topic.topic_post, post_id, post_time, post_createur, account_name, 
        membres.id
        FROM forum_forum
        LEFT JOIN forum_post ON forum_post.post_id = forum_forum.forum_last_post_id
        LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
        LEFT JOIN membres ON membres.id = forum_post.post_createur
        WHERE auth_view <= "'.$lvl.'" AND forum_cat_id = "'.$id.'"
        ORDER BY forum_ordre')->result();        
        
    }
    
    public function RecupNameCat ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        $result = $DB1->Query('SELECT cat_nom FROM forum_categorie WHERE cat_id = "'.$id.'"')->row_array();
        
        return $result['cat_nom'];
        
    }
    
    public function GetForum ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT forum_name, forum_topic, auth_view, auth_topic FROM forum_forum WHERE forum_id = '.$id.'')->result_array();
        
    }
    
    public function GetTopic ($forum) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
        Mb.account_name AS membre_pseudo_createur, post_createur, post_time, Ma.account_name AS membre_pseudo_last_posteur, post_id FROM forum_topic 
        LEFT JOIN membres Mb ON Mb.id = forum_topic.topic_createur
        LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
        LEFT JOIN membres Ma ON Ma.id = forum_post.post_createur    
        WHERE topic_genre = "Annonce" AND forum_topic.forum_id = '.$forum.' 
        ORDER BY topic_last_post DESC')->result_array();
        
    }
    
    public function GetAllTopicPag ($forum) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
        Mb.account_name AS membre_pseudo_createur, post_createur, post_time, Ma.account_name AS membre_pseudo_last_posteur, post_id FROM forum_topic 
        LEFT JOIN membres Mb ON Mb.id = forum_topic.topic_createur
        LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
        LEFT JOIN membres Ma ON Ma.id = forum_post.post_createur    
        WHERE topic_genre <> "Annonce" AND forum_topic.forum_id = '.$forum.'
        ORDER BY topic_last_post DESC')->result_array();
        //LIMIT :premier ,:nombre', array('forum' => $forum, 'premier' => $premierMessageAafficher, 'nombre' => $nombreDeMessagesParPage));
        
    }
    
    public function GetTopicFil ($topic) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT topic_titre, topic_post, forum_topic.forum_id, topic_last_post,
        forum_name, auth_view, auth_topic, auth_post 
        FROM forum_topic 
        LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id 
        WHERE topic_id = '.$topic.'')->row_array();
        
    }
    
    public function GetAllPost ($topic, $premier, $nombre) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT post_id , post_createur , post_texte , post_time ,
        id, account_name, membre_inscrit, membre_avatar, membre_localisation, membre_post, membre_signature
        FROM forum_post
        LEFT JOIN membres ON id = forum_post.post_createur
        WHERE topic_id ='.$topic.'
        ORDER BY post_id')->result_array();
        
    }
    
    public function GetAllPost2 ($topic, $premier, $nombre) {  
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT post_id , post_createur , post_texte , post_time ,
        id, account_name, membre_inscrit, membre_avatar, membre_localisation, membre_post, membre_signature
        FROM forum_post
        LEFT JOIN membres ON id = forum_post.post_createur
        WHERE topic_id ='.$topic.'
        ORDER BY post_id');
        
    }
    
    public function GetRead ($topic, $id) {      
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        $resultat = $DB1->Query('SELECT COUNT(*) AS nb_total FROM forum_topic_view WHERE tv_topic_id = '.$topic.' AND tv_id = '.$id.'')->row_array();
       
        return $resultat['nb_total'];
        
    }
    
    public function GetAddRead ($id, $topic, $forum, $last_post) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('INSERT INTO forum_topic_view (tv_id, tv_topic_id, tv_forum_id, tv_post_id) VALUES ('.$id.', '.$topic.', '.$forum.', '.$last_post.')');
        
    }
    
    public function GetUpdateRead ($id, $topic, $last_post) { 
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('UPDATE forum_topic_view SET tv_post_id = '.$last_post.' WHERE tv_topic_id = '.$topic.' AND tv_id = '.$id.'');  
        
    }
    
    public function GetUpdateReadOne ($topic) { 
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('UPDATE forum_topic SET topic_vu = topic_vu + 1 WHERE topic_id = '.$topic.'');
        
    }
    
    public function GetFirstPost ($topic) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        $query = $DB1->Query('SELECT topic_first_post, topic_last_post FROM forum_topic WHERE topic_id = '.$topic.'')->row_array();
        
        return $query['topic_first_post'];
                
    }
    
    public function GetSelectLock ($topic) { 
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT topic_locked FROM forum_topic WHERE topic_id = '.$topic.'')->row_array();
        
    }
    
    public function GetUpdateForumTopic ($forum) { 
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        return $DB1->Query('SELECT forum_id, forum_name FROM forum_forum WHERE forum_id <> '.$forum.'')->result_array();
        
    }
    
}