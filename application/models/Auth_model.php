<?php

/**
 * -----------------------------------------------
 *
 * This file is part of deathart.
 *
 * Diamond is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Diamond is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Diamond. If not, see <http://www.gnu.org/licenses/>
 *
 * @author Louis BRABANT <louis.brabant@laposte.net>
 * @license http://www.gnu.org/licenses/gpl.html GPLv3
 * 
 * -----------------------------------------------
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth_model extends CI_Model {
    
    public function __construct(){
        
        parent::__construct();
        
    }
    
    public function VerifPseudo ($pseudo) {
        
        $DB3 = $this->load->database('auth', TRUE);
        $DB3->cache_on();
        $pseudoexiste = $DB3->query('SELECT * FROM account WHERE username="'.strtoupper($pseudo).'"');
        
        if($pseudoexiste->num_rows() > 0) {
            return 1;
        }
        else {
            return 0;
        }
        
    }
    
    public function VerifPassword ($pseudo, $password) {
        
        $PasswordCrypt = strtoupper(sha1(strtoupper("".$pseudo.":".$password."")));
        $DB3 = $this->load->database('auth', TRUE);
        $DB3->cache_on();
        $recup = $DB3->query('SELECT id, sha_pass_hash FROM account WHERE username="'.strtoupper($pseudo).'"')->row();

        if ($PasswordCrypt == $recup->sha_pass_hash) {
            //On vérifie le rank et el gmlevel pour saovir si il est banni si les deux son en -1 alors il est banni
            if ($this->RecupRank($recup->id) !== -1) {
                if ($this->RecupGMlevel($recup->id) !== -1) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
        
    }
    
    public function RecupID ($pseudo) {
        
        $DB3 = $this->load->database('auth', TRUE);
        $DB3->cache_on();
        $recup = $DB3->query('SELECT id FROM account WHERE username="'.strtoupper($pseudo).'"')->row();
        return $recup->id;
        
    }
    
    public function RecupRank ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        $recup = $DB3->query("SELECT membre_rank FROM membres WHERE id='".$id."'")->row();
        
        return $recup->membre_rank;
        
    }
    
    public function RecupGMlevel ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        $recup = $DB3->query("SELECT membre_gmlevel FROM membres WHERE id='".$id."'")->row();
        
        return $recup->membre_gmlevel;
        
    }
    
    public function RecupPerso ($id) {
        
        $DB3 = $this->load->database('default', TRUE);
        $DB3->cache_on();
        
        $recup = $DB3->query("SELECT perso FROM membres WHERE id='".$id."'")->row();
        
        return $recup->perso;
        
    }
    
    public function avatar ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        $DB1->cache_on();
        
        $recup_avatar = $DB1->query("SELECT membre_avatar FROM membres WHERE id='".$id."'")->row();
        
        return $recup_avatar->membre_avatar;
        
    }
    
    public function AddLog ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        
        $user_agent = getenv("HTTP_USER_AGENT");
        
        if ((strpos($user_agent, "Nav") !== FALSE) || (strpos($user_agent, "Gold") !== FALSE) ||
        (strpos($user_agent, "X11") !== FALSE) || (strpos($user_agent, "Mozilla") !== FALSE) ||
        (strpos($user_agent, "Netscape") !== FALSE) AND (!strpos($user_agent, "MSIE") !== FALSE) AND (!strpos($user_agent, "Konqueror") !== FALSE)
        AND (!strpos($user_agent, "Firefox") !== FALSE) AND (!strpos($user_agent, "Safari") !== FALSE))
                $browser = "Internet Explorer";
        elseif (strpos($user_agent, "Opera") !== FALSE)
                $browser = "Opera";
        elseif (strpos($user_agent, "MSIE") !== FALSE)
                $browser = "MSIE";
        elseif (strpos($user_agent, "Lynx") !== FALSE)
                $browser = "Lynx";
        elseif (strpos($user_agent, "WebTV") !== FALSE)
                $browser = "WebTV";
        elseif (strpos($user_agent, "Konqueror") !== FALSE)
                $browser = "Konqueror";
        elseif (strpos($user_agent, "Safari") !== FALSE)
                $browser = "Safari";
        elseif (strpos($user_agent, "Firefox") !== FALSE)
                $browser = "Firefox";
        elseif ((stripos($user_agent, "bot") !== FALSE) || (strpos($user_agent, "Google") !== FALSE) ||
        (strpos($user_agent, "Slurp") !== FALSE) || (strpos($user_agent, "Scooter") !== FALSE) ||
        (stripos($user_agent, "Spider") !== FALSE) || (stripos($user_agent, "Infoseek") !== FALSE))
                $browser = "Bot";
        else
                $browser = "Autre";
        
        if (strpos($user_agent, "Win") !== FALSE)
            $os = "Windows";
        elseif ((strpos($user_agent, "Mac") !== FALSE) || (strpos($user_agent, "PPC") !== FALSE))
            $os = "Mac";
        elseif (strpos($user_agent, "Linux") !== FALSE)
            $os = "Linux";
        elseif (strpos($user_agent, "FreeBSD") !== FALSE)
            $os = "FreeBSD";
        elseif (strpos($user_agent, "SunOS") !== FALSE)
            $os = "SunOS";
        elseif (strpos($user_agent, "IRIX") !== FALSE)
            $os = "IRIX";
        elseif (strpos($user_agent, "BeOS") !== FALSE)
            $os = "BeOS";
        elseif (strpos($user_agent, "OS/2") !== FALSE)
            $os = "OS/2";
        elseif (strpos($user_agent, "AIX") !== FALSE)
            $os = "AIX";
        else
            $os = "Autre";
        
        $language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        $language = $language{0}.$language{1};
        
        $DB1->Query('INSERT INTO log_login (account, dates, ip, os, nav, pays) VALUES ("'.$id.'", NOW(), "'.$_SERVER["REMOTE_ADDR"].'", "'.$os.'", "'.$browser.'", "'.$language.'")');
        
    }
    
    public function Logout ($id) {
        
        $DB1 = $this->load->database('default', TRUE);
        
        $DB1->cache_delete('Account', 'index');
        
    }
    
}