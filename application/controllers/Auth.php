<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->layout->set_title(lang('Title_Page_Auth'));
        
        //Bootstrap
        $this->layout->add_includes('css', 'assets/css/main.css');
        //Style
        $this->layout->add_includes('css', 'assets/css/login.css');
        //font-awesome.css
        $this->layout->add_includes('css', 'assets/css/font-awesome.css');
              
        //Jquery
        $this->layout->add_includes('js', 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js', NULL, FALSE);
        $this->layout->add_includes('js', 'assets/js/jquerui.ui.js', NULL, TRUE);
        //Bootstrap
        $this->layout->add_includes('js', 'assets/js/bootstrap.js');
        $this->layout->add_includes('js', 'assets/js/transition.js');
        //Theme scripts
        $this->layout->add_includes('js', 'assets/js/auth.js');
        
        $this->lang->load('auth');
        
    }
	
    public function index () {
        
        if ($this->session->userdata('logged_in')) {
            
            redirect('Home', 'refresh');
            
        }
        
        $data = array();
        
        $this->layout->view('pages/auth/home', $data, 'auth');
        
    }
    
    //On vérifie si le pseudo existe
    public function Pseudo() {
        
        $this->load->model('auth_model');
        
        echo $this->auth_model->VerifPseudo($_POST['pseudo']);
        
    }
    
    //On vérifie si le mot de passe existe bien
    public function Login() {
        
        $pseudo = $_POST['pseudo'];
        $password = $_POST['mdp'];
        
        $this->load->model('auth_model');
        
        if ($this->auth_model->VerifPassword($pseudo, $password) === 1) {
            
            $id = $this->auth_model->RecupID($pseudo);
            $this->auth_model->AddLog($id);
            $newdata = array(
                'account_name'  => ucfirst($pseudo),
                'account_id' => $id,
                'account_gmlevel' => $this->auth_model->RecupGMLevel($id),
                'account_rank' => $this->auth_model->RecupRank($id),
                'account_perso' => $this->auth_model->RecupPerso($id),
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            echo $this->auth_model->VerifPassword($pseudo, $password);
        } else {
            echo 0;
        }
        
    }
    
    public function Logout() {
        
        $this->load->model('auth_model');
        
        $this->auth_model->Logout($this->session->userdata('account_id'));
        
        $this->session->sess_destroy();
                
        echo 1;
        
    }
    
}