<?php

/**
* -----------------------------------------------
*
* This file is part of Shindorei.
*
* Diamond is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Diamond is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Diamond. If not, see <http://www.gnu.org/licenses/>
*
* @author Louis BRABANT <louis.brabant@laposte.net>
* @link http://shindorei-serveur.com/
* @link https://bitbucket.org/GriffusDev/siteweb/
* @license http://www.gnu.org/licenses/gpl.html GPLv3
* 
* -----------------------------------------------
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Bug extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->layout->set_title(lang('Title_Page_Bug'));
        
        //Style css
        $this->layout->add_includes('css', 'assets/css/style.css');
                 
        //Jquery
        $this->layout->add_includes('js', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', NULL, FALSE);
        //Bootstrap
        $this->layout->add_includes('js', 'assets/js/jquery_ui_custom.js');
        $this->layout->add_includes('js', 'assets/js/All.js');
        
    }
    
    public function index() {
        
        redirect('Bug/Home');
        
    }
	
    public function Home() {
        
        $data = array();
        
        $this->layout->view('pages/bug/home', $data);
        
    }
    
}