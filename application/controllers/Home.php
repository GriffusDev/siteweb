<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        
        $this->layout->set_title(lang('Title_Page_Home'));
        
        //Style css
        $this->layout->add_includes('css', 'assets/css/style.css');
                 
        //Jquery
        $this->layout->add_includes('js', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', NULL, FALSE);
        //Bootstrap
        $this->layout->add_includes('js', 'assets/js/jquery_ui_custom.js');
        $this->layout->add_includes('js', 'assets/js/All.js');
        
        $this->load->helper('text');
        
    }
	
    public function index() {
        
        $this->load->model('news_model');
        
        $this->load->helper('BBCode_helper');
        
        $data = array();
        $this->layout->view('pages/home', $data);
        
    }
    
    public function News ($id) {
        
        $this->load->model('news_model');
        $data = array(
            'news_entries' => $this->news_model->get_news($id)->result_array()
        );
        
        $this->load->helper('BBCode_helper');
        
        $this->layout->view('pages/news', $data);
        
    }
    
    public function AddCom ($id, $comm) {
        
        $this->load->model('news_model');
        
        $this->news_model->add_com($id, $comm, $this->session->userdata('account_id'));
        
        echo 1;
        
    }
    
    public function DelCom($id, $type = "") {
        
        $this->load->model('news_model');
        
        if ($type == "hide") {
            
            $this->news_model->del_com($id, "hide");
            
        }
        elseif ($type == "del") {
            
            $this->news_model->del_com($id, "del");
            
        }
        elseif ($type == "hides") {
            
            $this->news_model->del_com($id, "hides");
            
        }
        else {
            echo 0;
        }
        
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */