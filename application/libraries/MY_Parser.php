<?php

//Condition d'utilisation :
//Aficher une variable "{lavariable}
//Afficher une langue "{lang:laphrase}"
//Afficher une condition :
//"{if test == ""}
//condition oui
//{else}
//condition non
//{endif}
//Fonction base_url()
//{function:base_url}
//La suite : http://ellislab.com/codeigniter%20/user-guide/libraries/parser.html

class MY_Parser extends CI_Parser {

    public $l_delim = '{';
    public $l_delimIF = '{';
    private $ci;
    
    const LANG_REPLACE_REGEXP = '!\{lang:\s*(?<key>[^\}]+)\}!';
    
    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
    }

    public function parse($template, $data, $return = FALSE) {
        
        $template = $this->ci->load->view($template, $data, TRUE);
        $template = $this->replace_lang_keys($template);
        $template = $this->parse_function($template);
        $template = $this->parse_session($template);
        //$template = $this->parse_statut($template);

        return $this->_parse($template, $data, $return);
    }

    protected function replace_lang_keys($template) {
        return preg_replace_callback(self::LANG_REPLACE_REGEXP, array($this, 'replace_lang_key'), $template);
    }

    protected function replace_lang_key($key) {
        return $this->ci->lang->line($key[1]);
    }
    
    protected function parse_function($template) {
        $template = str_replace('{function:base_url}', base_url(), $template);
        $template = str_replace('{lang_uri}', $this->ci->uri->segment(1).'/', $template);
        return $template;
    }
    
    public function get_statut($host, $port) {
        $fp = @fsockopen($host, $port, $errno, $errstr, 1);

        if($fp >= 1) 
            return'up.png';
        else
            return'down.png';
    }
    
    protected function parse_statut($template) {
        $template = str_replace('{statut:logon}', $this->get_statut('37.187.26.230', '3724'), $template);
        $template = str_replace('{statut:realm}', $this->get_statut('37.187.26.230', '8086'), $template);
        $template = str_replace('{statut:ts}', $this->get_statut('37.187.26.230', '10011'), $template);
        return $template;
    }
    
    protected function parse_session($template) {
        $template = str_replace('{session:login}', $this->ci->session->userdata('logged_in'), $template);
        $template = str_replace('{session:id}', $this->ci->session->userdata('account_id'), $template);
        $template = str_replace('{session:pseudo}', $this->ci->session->userdata('account_name'), $template);
        $template = str_replace('{session:rank}', $this->ci->session->userdata('account_rank'), $template);
        $template = str_replace('{session:gmlevel}', $this->ci->session->userdata('account_gmlevel'), $template);
        return $template;
    }  
    
}